#include<stdio.h>
#include"json.h"

void jsonDump(json_value * jVal){
  //printf("jVal is %0llx\n",jVal);
  //printf("jVal->type is %d\n",jVal->type);
  //printf("\n");
  setvbuf( stdout, NULL, _IONBF, BUFSIZ );

  switch(jVal->type){
  case json_none:
    printf("NONE");
    break;
  case json_object:
    printf("object:[");
    if(jVal->u.object.length>0){
      int i;
      printf("<name:\"%s\",value:",jVal->u.object.values[0].name);
      jsonDump(jVal->u.object.values[0].value);
      printf(">");
      for(i=1;i<jVal->u.object.length;i++){
	printf(", ");
	printf("<name:\"%s\",value:",jVal->u.object.values[i].name);
	jsonDump(jVal->u.object.values[i].value);
	printf(">");
      }
    }
    printf("]");
    break;
  case json_array:
    printf("array:[");
    if(jVal->u.array.length > 0){
      jsonDump(jVal->u.array.values[0]);
      int i;
      for(i=1;i<jVal->u.array.length;i++){
	printf(", ");
	jsonDump(jVal->u.array.values[i]);
      }
    }
    printf("]");
    break;
  case json_integer:
    printf("integer:%lld",jVal->u.integer);
    break;
  case json_double:
    printf("double:%f",jVal->u.dbl);
    break;
  case json_string:
    printf("string:\"%s\"",jVal->u.string.ptr);
    break;
  case json_boolean:
    printf("boolean:%s",jVal->u.boolean ? "TRUE" : "FALSE");
    break;
  case json_null:
    printf("NULL");
    break;
  default:
    break;
  }
}

int makeIDChangeTableMem(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink);
int makeIDChangeTableMemArray(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink);
int makeIDChangeTableAtom(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink);
int makeIDChangeTableAtomArray(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink);
int makeIDChangeTableLink(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink);
int makeIDChangeTableLinkArray(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink);

int makeIDChangeTableMem(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink){
  int oldID = jVal->u.object.values[0].value->u.integer;

  IDBijection[oldID] = newID;
  IDBijection[newID] = oldID;
  IDInjectionForHyperLink[oldID] = oldID;

  newID++;

  newID = makeIDChangeTableMemArray(jVal->u.object.values[3].value,newID,IDBijection,IDInjectionForHyperLink);
  newID = makeIDChangeTableAtomArray(jVal->u.object.values[2].value,newID,IDBijection,IDInjectionForHyperLink);

  return newID;
}

int makeIDChangeTableMemArray(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink){
  int i;

  for(i=0;i<jVal->u.array.length;i++){
    newID = makeIDChangeTableMem(jVal->u.array.values[i],newID,IDBijection,IDInjectionForHyperLink);
  }

  return newID;
}

int makeIDChangeTableAtomArray(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink){
  int i;

  for(i=0;i<jVal->u.array.length;i++){
    newID = makeIDChangeTableAtom(jVal->u.array.values[i],newID,IDBijection,IDInjectionForHyperLink);
  }

  return newID;
}

int makeIDChangeTableAtom(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink){
  int oldID = jVal->u.object.values[0].value->u.integer;

  IDBijection[oldID] = newID;
  IDBijection[newID] = oldID;
  IDInjectionForHyperLink[oldID] = oldID;

  newID++;

  newID = makeIDChangeTableLinkArray(jVal->u.object.values[2].value,newID,IDBijection,IDInjectionForHyperLink);

  return newID;
}

int makeIDChangeTableLinkArray(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink){
  int i;

  for(i=0;i<jVal->u.array.length;i++){
    newID = makeIDChangeTableLink(jVal->u.array.values[i],newID,IDBijection,IDInjectionForHyperLink);
  }

  return newID;
}

int prevIndex(int *array,int tmpIndex){
  int i;

  for(i=tmpIndex-2;i>=0;i-=2){
    if((array[i]!=0 && array[i-1]!=-1)){
      return i;
    }
  }

  //fprintf(stderr,"BAD INDEX CASE:\nFILE...%s\nfunc...%s\nLINE...%d\n",__FILE__,__func__,__LINE__);
  //exit( EXIT_FAILURE );
  return 0;
}

int makeIDChangeTableLink(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink){
  int oldID;

  switch(jVal->u.object.values[0].value->u.integer){
  case 128://integer
    break;
  case 129://double
    break;
  case 131://string
    break;
  case 138://hyper link
    oldID = jVal->u.object.values[1].value->u.integer;
    newID++;

    //*
    if(IDBijection[oldID]==0){
      IDBijection[oldID] = oldID;
      IDBijection[oldID-1] = -1;
    }
    //*/
    if(jVal->parent->parent->u.object.values[0].value->u.integer == prevIndex(IDBijection,oldID)){
      IDBijection[oldID] = newID;
    }
    IDBijection[newID] = oldID;
    IDInjectionForHyperLink[oldID] = oldID;
    
    newID++;

    break;
  default:
    if(jVal->u.object.values[0].value->u.integer < 128){//normal link

    }else{//bad case
      fprintf(stderr,"BAD LINK CASE:\nFILE...%s\nfunc...%s\nLINE...%d\n",__FILE__,__func__,__LINE__);
      exit( EXIT_FAILURE );
    }
    break;
  }

  return newID;
}

void changeIDMem(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink);
void changeIDMemArray(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink);
void changeIDAtom(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink);
void changeIDAtomArray(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink);
void changeIDLink(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink);
void changeIDLinkArray(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink);
void changeIDProxyLinkArray(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink);

void changeIDMem(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink){
  int oldID = jVal->u.object.values[0].value->u.integer;

  jVal->u.object.values[0].value->u.integer = IDBijection[oldID];

  changeIDMemArray(jVal->u.object.values[3].value,newID,IDBijection,IDInjectionForHyperLink);
  changeIDAtomArray(jVal->u.object.values[2].value,newID,IDBijection,IDInjectionForHyperLink);

  return;
}

void changeIDMemArray(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink){
  int i;

  for(i=0;i<jVal->u.array.length;i++){
    changeIDMem(jVal->u.array.values[i],newID,IDBijection,IDInjectionForHyperLink);
  }

  return;
}

void changeIDAtomArray(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink){
  int i;

  for(i=0;i<jVal->u.array.length;i++){
    changeIDAtom(jVal->u.array.values[i],newID,IDBijection,IDInjectionForHyperLink);
  }

  return;
}

void changeIDAtom(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink){
  int oldID = jVal->u.object.values[0].value->u.integer;

  jVal->u.object.values[0].value->u.integer = IDBijection[oldID];

  if((jVal->u.object.values[1].value->u.string.ptr)[0]=='$'){//proxy atoms' link
    changeIDProxyLinkArray(jVal->u.object.values[2].value,newID,IDBijection,IDInjectionForHyperLink);
  }else{
    changeIDLinkArray(jVal->u.object.values[2].value,newID,IDBijection,IDInjectionForHyperLink);
  }

  return;
}

void changeIDLinkArray(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink){
  int i;

  for(i=0;i<jVal->u.array.length;i++){
    changeIDLink(jVal->u.array.values[i],newID,IDBijection,IDInjectionForHyperLink);
  }

  return;
}

void changeIDProxyLinkArray(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink){
  int i;

  for(i=0;i<jVal->u.array.length-1;i++){
    changeIDLink(jVal->u.array.values[i],newID,IDBijection,IDInjectionForHyperLink);
  }

  return;
}

void changeIDLink(json_value *jVal,int newID,int *IDBijection,int *IDInjectionForHyperLink){
  int oldID;

  switch(jVal->u.object.values[0].value->u.integer){
  case 128://integer
    break;
  case 129://double
    break;
  case 131://string
    break;
  case 138://hyper link
    oldID = jVal->u.object.values[1].value->u.integer;

    jVal->u.object.values[1].value->u.integer = IDBijection[oldID];

    break;
  default:
    if(jVal->u.object.values[0].value->u.integer < 128){//normal link
      oldID = jVal->u.object.values[1].value->u.integer;

      jVal->u.object.values[1].value->u.integer = IDBijection[oldID];
    }else{//bad case
      fprintf(stderr,"BAD LINK CASE:\nFILE...%s\nfunc...%s\nLINE...%d\n",__FILE__,__func__,__LINE__);
      exit( EXIT_FAILURE );
    }
    break;
  }

  return;
}

#define SWAP(X,Y,TMP){				\
    TMP = X;					\
    X = Y;					\
    Y = TMP;					\
  }						\

void memReverseOrder(json_value *jVal);
void memReverseOrderInner(json_value *jVal);

void memReverseOrder(json_value *jVal){

  memReverseOrderInner(jVal->u.object.values[3].value);

  return;
}

void memReverseOrderInner(json_value *jVal){
  int i,j;
  json_value *tmp;

  i = 0;
  j = jVal->u.array.length - 1;

  while(i<j){
    SWAP(jVal->u.array.values[i],jVal->u.array.values[j],tmp);
    i++;
    j--;
  }

  for(i=0;i<jVal->u.array.length;i++){
    memReverseOrder(jVal->u.array.values[i]);
  }

  return;
}

void arrayInitialize(int *array,int size){
  int i;

  for(i=0;i<size;i++){
    array[i] = 0;
  }

  return;
}



json_value * IDChanger(json_value * jVal,int maxID){
  int IDBijection[maxID*2+1];
  int IDInjectionForHyperLink[maxID+1];

  arrayInitialize(IDBijection,maxID*2+1);
  arrayInitialize(IDInjectionForHyperLink,maxID+1);

  makeIDChangeTableMem(jVal,maxID,IDBijection,IDInjectionForHyperLink);

  changeIDMem(jVal,maxID,IDBijection,IDInjectionForHyperLink);

  memReverseOrder(jVal);

  return jVal;
}
