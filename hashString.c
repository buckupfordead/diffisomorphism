#include"convertedGraph.h"
#include"trie.h"
#include"adjacent.h"
#include <string.h>

Hash callHashValue(InheritedVertex *iVertex,int index,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID,Stack *fixCreditIndexStack);

HashString *makeHashString(){
  HashString *ret = (HashString *)malloc(sizeof(HashString));

  ret->creditIndex = 0;
  ret->body = makeDynamicArray();

  return ret;
}

void freeHashString(HashString *hashString){
  freeDynamicArrayAndValues(hashString->body,free);
  free(hashString);

  return;
}

Hash stringHashValue(char *string){
  Hash ret = OFFSET_BASIS;
  int i;

  for(i=0;string[i] != '\0';i++){
    ret ^= (Hash)string[i];
    ret *= FNV_PRIME;
  }

  return ret;
}

Hash doubleHashValue(double dbl){
  union {
    double d;
    Hash h;
  } uni;

  uni.d = dbl;

  return uni.h;
}

Hash integerHashValue(int i){
  union {
    int i;
    Hash h;
  } uni;

  uni.i = i;

  return uni.h;
}

Hash initialHashValue(ConvertedGraphVertex *cVertex){
  Hash ret = OFFSET_BASIS;

  switch(cVertex->type){
    case convertedAtom:
      ret *= FNV_PRIME;
      ret ^= cVertex->type;
      ret *= FNV_PRIME;
      ret ^= stringHashValue(cVertex->name);
      ret *= FNV_PRIME;
      ret ^= numStack(cVertex->links);
      ret *= FNV_PRIME;

      return ret;
      break;
    case convertedHyperLink:
      ret *= FNV_PRIME;
      ret ^= cVertex->type;
      ret *= FNV_PRIME;
      ret ^= stringHashValue(cVertex->name);
      ret *= FNV_PRIME;

      return ret;
      break;
    default:
      CHECKER("unexpected type\n");
      exit(EXIT_FAILURE);
      break;
  }
}

Hash linkHashValue(LMNtalLink *link,int index,ConvertedGraph *cGraph,int gapOfGlobalRootMemID,Stack *fixCreditIndexStack){
  Hash ret;

  switch(link->attr){
    printf(" --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
    printf("%d %d\n", link->attr, link->data.ID);
    case INTEGER_ATTR:
      ret = OFFSET_BASIS;
      ret *= FNV_PRIME;
      ret ^= link->attr;
      ret *= FNV_PRIME;
      ret ^= link->data.ID;
      return ret;
      break;
    case HYPER_LINK_ATTR:
      ret = OFFSET_BASIS;
      ret *= FNV_PRIME;
      ret ^= link->attr;
      ret *= FNV_PRIME;
      ret ^= callHashValue(((ConvertedGraphVertex *)readDynamicArray(cGraph->hyperLinks,link->data.ID))->correspondingVertexInTrie,index,cGraph,gapOfGlobalRootMemID,fixCreditIndexStack);
      return ret;
      break;
    case GLOBAL_ROOT_MEM_ATTR:
      ret = OFFSET_BASIS;
      ret *= FNV_PRIME;
      ret ^= link->attr;
      ret *= FNV_PRIME;
      ret ^= link->data.ID;
      return ret;
      break;
    case DOUBLE_ATTR://LMNtalLink非対応
    case STRING_ATTR://LMNtalLink非対応
    default:
      if(link->attr < 128){
        ret = OFFSET_BASIS;
        ret *= FNV_PRIME;
        ret ^= link->attr;
        ret *= FNV_PRIME;
        ret ^= callHashValue(((ConvertedGraphVertex *)readDynamicArray(cGraph->atoms,link->data.ID))->correspondingVertexInTrie,index,cGraph,gapOfGlobalRootMemID,fixCreditIndexStack);
        return ret;
      }else{
        CHECKER("unexpected type\n");
        exit(EXIT_FAILURE);
      }
      break;
  }
}

Hash adjacentHashValue(ConvertedGraphVertex *cVertex,int index,ConvertedGraph *cGraph,int gapOfGlobalRootMemID,Stack *fixCreditIndexStack){
  Hash ret;
  Hash sum,mul;
  Hash tmp;
  int i;

  switch(cVertex->type){
    case convertedAtom:
      ret = OFFSET_BASIS;
      for(i=0;i<numStack(cVertex->links);i++){
        ret *= FNV_PRIME;
        ret ^= linkHashValue((LMNtalLink *)readStack(cVertex->links,i),index-1,cGraph,gapOfGlobalRootMemID,fixCreditIndexStack);
      }

      return ret;
      break;
    case convertedHyperLink:
      sum = ADD_INIT;
      mul = MUL_INIT;
      for(i=0;i<numStack(cVertex->links);i++){
        tmp = linkHashValue((LMNtalLink *)readStack(cVertex->links,i),index-1,cGraph,gapOfGlobalRootMemID,fixCreditIndexStack);
        sum += tmp;
        mul *= (tmp*2+1);
      }
      ret = sum ^ mul;

      return ret;
      break;
    default:
      CHECKER("unexpected type\n");
      exit(EXIT_FAILURE);
      break;
  }
}

void pushInheritedVertexIntoFixCreditIndexStackWithoutOverlap(Stack *fixCreditIndexStack,InheritedVertex *iVertex){
  if(!iVertex->isPushedIntoFixCreditIndex){
    pushStack(fixCreditIndexStack,iVertex);
    iVertex->isPushedIntoFixCreditIndex = TRUE;
  }

  return;
}

InheritedVertex *popInheritedVertexFromFixCreditIndexStackWithoutOverlap(Stack *fixCreditIndexStack){
  InheritedVertex *iVertex = popStack(fixCreditIndexStack);
  iVertex->isPushedIntoFixCreditIndex = FALSE;

  return iVertex;
}

void fixCreditIndex(Stack *fixCreditIndexStack,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID){
  while(!isEmptyStack(fixCreditIndexStack)){
    InheritedVertex *iVertex = popInheritedVertexFromFixCreditIndexStackWithoutOverlap(fixCreditIndexStack);
    TrieBody *ownerNode = iVertex->ownerNode;
    HashString *hashString = iVertex->hashString;

    hashString->creditIndex = ownerNode->depth;
  }

  return;
}

Hash callHashValue(InheritedVertex *iVertex,int index,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID,Stack *fixCreditIndexStack){
  HashString *hashString = iVertex->hashString;

  if(index < 0){
    return 0;
  }else if(index < hashString->creditIndex){
    return ((KeyContainer *)readDynamicArray(hashString->body,index))->u.ui32;
  }else if(index == 0){
    Hash tmp = initialHashValue(correspondingVertexInConvertedGraph(iVertex,cAfterGraph,gapOfGlobalRootMemID));
    KeyContainer *old = writeDynamicArray(hashString->body,index,allocKey(makeUInt32Key(tmp)));
    if(old != NULL){
      free(old);
    }
    hashString->creditIndex = 1;
    pushInheritedVertexIntoFixCreditIndexStackWithoutOverlap(fixCreditIndexStack,iVertex);
    return tmp;
  }else{
    Hash prevMyHash = callHashValue(iVertex,index - 1,cAfterGraph,gapOfGlobalRootMemID,fixCreditIndexStack);
    Hash adjacentHash = adjacentHashValue(correspondingVertexInConvertedGraph(iVertex,cAfterGraph,gapOfGlobalRootMemID),index,cAfterGraph,gapOfGlobalRootMemID,fixCreditIndexStack);
    Hash newMyHash = (FNV_PRIME * prevMyHash) ^ adjacentHash;
    KeyContainer *old = writeDynamicArray(hashString->body,index,allocKey(makeUInt32Key(newMyHash)));
    if(old != NULL){
      free(old);
    }
    hashString->creditIndex = index + 1;
    pushInheritedVertexIntoFixCreditIndexStackWithoutOverlap(fixCreditIndexStack,iVertex);
    return newMyHash;
  }
}



/* ---ここまでmiyahara作成--- */

/* ---ここからsakazume作成--- */
Hash fnvHash(char *str) {
  Hash hval;
  int i;
  /*
   * FNV-1a hash each octet in the buffer
   */
  hval = OFFSET_BASIS;
  for (i=strlen(str)-1;i>=0;i--) {
    /* xor the bottom with the current octet */
    hval ^= (unsigned int)str[i] & 0x000000FF;
    /* multiply by the FNV magic prime mod 2^32 or 2^64 */
    hval *= FNV_PRIME;
    // printf("i:%d (%02X) hash:%08X\n",i,(unsigned int)str[i]&0x000000FF,hval);
  }

  return hval;
}

Hash labelHashValue(CanonicalLabel label){
  union {
    unsigned int i[2];
    char s[8];
  } hashelem;

  hashelem.i[0] = label.first;
  hashelem.i[1] = label.second;
 
  return fnvHash(hashelem.s);
}

Hash adjacentListHashValue(List *adjacent){
  ListBody *sentinel = adjacent->sentinel;
  ListBody *iterator;
  Hash val,sum,mul,tmp;
  sum = mul = 0;

  for(iterator=sentinel->next;iterator!=sentinel;){
    ListBody *iteratorNext = iterator->next;

    CanonicalLabel *label = iterator->value;
    tmp = labelHashValue(*label);
    sum += tmp;
    mul *= (2*tmp+1);

    iterator = iteratorNext;
  }

  val = sum ^ mul;
  return val;
}

void calcAdjacentInfoHashValue(AdjacentInfo *adjacentInfo){
  Hash h_myself,h_adjacent;
  Hash sum,mul;
  h_myself = labelHashValue(adjacentInfo->myself);
  h_adjacent = adjacentListHashValue(adjacentInfo->adjacent);
  sum = h_myself + h_adjacent;
  mul = (h_myself*2+1) * (h_adjacent*2+1);
  adjacentInfo->hash_myself = h_myself;
  adjacentInfo->hash_info = sum ^ mul;
  return;
}

Hash inverse(Hash hash){
  Hash ret;
  INVERSE(hash,ret);
  return ret;
}

