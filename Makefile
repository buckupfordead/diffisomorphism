# CC = clang
CC = gcc
SRCS = $(wildcard *.c)
# SRCS = main.c
OBJS = $(SRCS:%.c=%.o)
DEPS = $(SRCS:%.c=%.d)
PROG = a.out

$(PROG):$(OBJS)
	$(CC) -o $@ $(OBJS) -lm

-include $(DEPS)

%.o: %.c
	$(CC) -c -O3 -Wall -MMD -MP $<

.PHONY:gdb
gdb:
	$(CC) -g -c -MMD -MP $(SRCS)
	$(CC) -g -o $(PROG) $(OBJS)

.PHONY:clean
clean:
	rm -f $(PROG) $(OBJS) $(DEPS)

