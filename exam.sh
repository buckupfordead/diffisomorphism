for size in {03..12}
do
	for type in {1..3}
	do
		for count in {01..10}
		do
			input="$size-$type.txt"
			output="phi_$size-$type-$count.txt"
			echo "./a.out < input/phi/$input > result/phi/$output"
			./a.out < input/phi/$input > result/phi/$output
		done
	done
done

for size in {03..12}
do
	for type in {1..2}
	do
		for count in {01..10}
		do
			input="$size-$type.txt"
			output="bubble_$size-$type-$count.txt"
			echo "./a.out < input/bubble/$input > result/bubble/$output"
			./a.out < input/bubble/$input > result/bubble/$output
		done
	done
done