################### NOT DIFF MODE ###################
STATES:70
TRANSITION:220
sorted     only in trie:       0...  0.000000%
sorted not only in trie:     220...100.000000%
    sum of canonical labeling time:   0.466463[sec]
average of canonical labeling time:   0.002120[sec]
    sum of    triePropagation time:   0.020373[sec]...  4.367571%
average of    triePropagation time:   0.000093[sec]
    sum of          listMcKay time:   0.446090[sec]... 95.632429%
average of          listMcKay time:   0.002028[sec]

experiment time:1.484903[sec] = 0:00:01

