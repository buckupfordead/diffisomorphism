#include<stdio.h>
#include<string.h>
#include"convertedGraph.h"
#include"json.h"

ConvertedGraph *makeEmptyConvertedGraph(){
  ConvertedGraph *ret = (ConvertedGraph *)malloc(sizeof(ConvertedGraph));
  ret->atoms = makeDynamicArray();
  ret->hyperLinks = makeDynamicArray();
  return ret;
}

LMNtalLink *makeLink(int attr,LMNtalData data){
  LMNtalLink *ret = (LMNtalLink *)malloc(sizeof(LMNtalLink));
  ret->attr = attr;
  ret->data = data;
  return ret;
}

void freeLink(LMNtalLink *link){
  free(link);
  return;
}

LMNtalLink *copyLink(LMNtalLink *link){
  return makeLink(link->attr,link->data);
}

Bool isEqualLinks(LMNtalLink *linkA,LMNtalLink *linkB){
  if(linkA->attr != linkB->attr){
    return FALSE;
  }

  switch(linkA->attr){
    case INTEGER_ATTR:
      return linkA->data.integer == linkB->data.integer;
      break;
    case DOUBLE_ATTR:
      return linkA->data.dbl == linkB->data.dbl;
      break;
    case STRING_ATTR:
      if(strcmp(linkA->data.string,linkB->data.string) == 0){
        return TRUE;
      }else{
        return FALSE;
      }
      break;
    case HYPER_LINK_ATTR:
      return linkA->data.ID == linkB->data.ID;
      break;
    case GLOBAL_ROOT_MEM_ATTR:
      return linkA->data.ID == linkB->data.ID;
      break;
    default:
      if(linkA->attr < 128){
        return linkA->data.ID == linkB->data.ID;
      }else{
        CHECKER("unexpected attr\n");
        exit(EXIT_FAILURE);
        return FALSE;
      }
      break;
  }

}

Bool isHyperLink(LMNtalLink *link){
  return link->attr == HYPER_LINK_ATTR;
}

int LMNtalID(json_value *jVal){
  return jVal->u.object.values[0].value->u.integer;
}

void LMNtalNameCopy(json_value *jVal,char *dstString){
  strcpy(dstString,jVal->u.object.values[1].value->u.string.ptr);
  return;
}

ConvertedGraphVertex *makeConvertedAtom(int ID,char *name){
  ConvertedGraphVertex *cVertex = (ConvertedGraphVertex *)malloc(sizeof(ConvertedGraphVertex));

  cVertex->type = convertedAtom;
  cVertex->links = makeStack();
  cVertex->ID = ID;
  strcpy(cVertex->name,name);
  cVertex->isPushedIntoDiffInfoStack = FALSE;
  cVertex->isVisitedInBFS = FALSE;
  cVertex->correspondingVertexInTrie = NULL;

  return cVertex;
}

ConvertedGraphVertex *makeConvertedHyperLink(int ID,char *hyperLinkAttr){
  ConvertedGraphVertex *cHyperlink = (ConvertedGraphVertex*)malloc(sizeof(ConvertedGraphVertex));

  cHyperlink->type = convertedHyperLink;
  cHyperlink->links = makeStack();
  cHyperlink->ID = ID;
  strcpy(cHyperlink->name,hyperLinkAttr);
  cHyperlink->isPushedIntoDiffInfoStack = FALSE;
  cHyperlink->isVisitedInBFS = FALSE;
  cHyperlink->correspondingVertexInTrie = NULL;

  return cHyperlink;
}

ConvertedGraph *convertGraphLink(json_value *jVal,ConvertedGraph *cGraph,ConvertedGraphVertex *cVertex,int linkNumber){
  int attr = jVal->u.object.values[0].value->u.integer;
  LMNtalData data;

  switch(attr){
    case INTEGER_ATTR:
      data.integer = jVal->u.object.values[1].value->u.integer;
      break;
    case DOUBLE_ATTR:
      data.dbl = jVal->u.object.values[1].value->u.dbl;
      break;
    case STRING_ATTR:
      strcpy(data.string,jVal->u.object.values[1].value->u.string.ptr);
      break;
    case HYPER_LINK_ATTR:
      data.ID = jVal->u.object.values[1].value->u.integer;
      break;
    case GLOBAL_ROOT_MEM_ATTR:
      data.ID = jVal->u.object.values[1].value->u.integer;
      break;
    default:
      if(attr < 128){
        data.ID = jVal->u.object.values[1].value->u.integer;
      }else{
        CHECKER("unexpected attr\n");
        exit(EXIT_FAILURE);
      }
      break;
  }

  LMNtalLink *link = makeLink(attr,data);
  pushStack(cVertex->links,link);

  if(link->attr == HYPER_LINK_ATTR){
    if(readDynamicArray(cGraph->hyperLinks,link->data.ID) == NULL){
      writeDynamicArray(cGraph->hyperLinks,link->data.ID,makeConvertedHyperLink(link->data.ID,""));
    }

    LMNtalData data;
    data.integer = cVertex->ID;
    LMNtalLink *linkFromHyperLink = makeLink(linkNumber,data);
    pushStack(((ConvertedGraphVertex *)readDynamicArray(cGraph->hyperLinks,link->data.ID))->links,copyLink(linkFromHyperLink));
  }

  return cGraph;
}

ConvertedGraph *convertGraphLinksArray(json_value *jVal,ConvertedGraph *cGraph,ConvertedGraphVertex *cVertex){
  int i;

  for(i=0;i<jVal->u.array.length;i++){
    convertGraphLink(jVal->u.array.values[i],cGraph,cVertex,i);
  }

  return cGraph;
}

ConvertedGraph *convertGraphAtom(json_value *jVal,ConvertedGraph *cGraph,LMNtalLink *linkToParentMem){
  char tmpName1[NAME_LENGTH];
  strcpy(tmpName1,"ATOM_");
  char tmpName2[NAME_LENGTH];
  LMNtalNameCopy(jVal,tmpName2);
  strcat(tmpName1,tmpName2);

  ConvertedGraphVertex *cVertex = makeConvertedAtom(LMNtalID(jVal),tmpName1);

  writeDynamicArray(cGraph->atoms,LMNtalID(jVal),cVertex);

  convertGraphLinksArray(jVal->u.object.values[2].value,cGraph,cVertex);
  pushStack(cVertex->links,copyLink(linkToParentMem));
  if(linkToParentMem->attr != GLOBAL_ROOT_MEM_ATTR){
    LMNtalData data;
    data.integer = LMNtalID(jVal);
    pushStack(((ConvertedGraphVertex *)readDynamicArray(cGraph->hyperLinks,linkToParentMem->data.ID))->links,makeLink(numStack(cVertex->links)-1,data));
  }

  return cGraph;
}

ConvertedGraph *convertGraphAtomsArray(json_value *jVal,ConvertedGraph *cGraph,LMNtalLink *linkToParentMem){
  int i;

  for(i=0;i<jVal->u.array.length;i++){
    convertGraphAtom(jVal->u.array.values[i],cGraph,linkToParentMem);
  }

  return cGraph;
}

ConvertedGraph *convertGraphMemsArray(json_value *jVal,ConvertedGraph *cGraph,LMNtalLink *linkToParentMem);

ConvertedGraph *convertGraphMem(json_value *jVal,ConvertedGraph *cGraph,LMNtalLink *linkToParentMem){
  char tmpName1[NAME_LENGTH];
  strcpy(tmpName1,"MEM_");
  char tmpName2[NAME_LENGTH];
  LMNtalNameCopy(jVal,tmpName2);
  strcat(tmpName1,tmpName2);

  ConvertedGraphVertex *cVertex = makeConvertedAtom(LMNtalID(jVal),tmpName1);
  ConvertedGraphVertex *cHyperlink = makeConvertedHyperLink(LMNtalID(jVal),"");

  writeDynamicArray(cGraph->atoms,LMNtalID(jVal),cVertex);
  writeDynamicArray(cGraph->hyperLinks,LMNtalID(jVal),cHyperlink);

  LMNtalData data;
  data.integer = LMNtalID(jVal);
  LMNtalLink *linkToThisMem = makeLink(HYPER_LINK_ATTR,data);

  pushStack(cVertex->links,copyLink(linkToThisMem));
  pushStack(cVertex->links,copyLink(linkToParentMem));
  data.integer = LMNtalID(jVal);
  pushStack(cHyperlink->links,makeLink(0,data));
  if(linkToParentMem->attr != GLOBAL_ROOT_MEM_ATTR){
    data.integer = LMNtalID(jVal);
    pushStack(((ConvertedGraphVertex *)readDynamicArray(cGraph->hyperLinks,linkToParentMem->data.ID))->links,makeLink(1,data));
  }

  convertGraphAtomsArray(jVal->u.object.values[2].value,cGraph,linkToThisMem);
  convertGraphMemsArray(jVal->u.object.values[3].value,cGraph,linkToThisMem);

  return cGraph;
}

ConvertedGraph *convertGraphMemsArray(json_value *jVal,ConvertedGraph *cGraph,LMNtalLink *linkToParentMem){
  int i;

  for(i=0;i<jVal->u.array.length;i++){
    convertGraphMem(jVal->u.array.values[i],cGraph,linkToParentMem);
  }

  return cGraph;
}

ConvertedGraph *convertGraph(json_value * jVal){
  ConvertedGraph *ret = (ConvertedGraph *)malloc(sizeof(ConvertedGraph));
  ret->atoms = makeDynamicArray();
  ret->hyperLinks = makeDynamicArray();

  LMNtalData data;
  data.integer = 0;
  LMNtalLink *gRootMemLink = makeLink(GLOBAL_ROOT_MEM_ATTR,data);

  /*$*/
  // int i;
  // printf("%lu  ",__LINE__+2);
  // for(i=0;i<=3;i++){
  //   printf("%d:%s  ", i, jVal->u.object.values[i]);
  // }
  //   printf("\n");
  /*$*/
  convertGraphAtomsArray(jVal->u.object.values[2].value,ret,gRootMemLink);
  convertGraphMemsArray(jVal->u.object.values[3].value,ret,gRootMemLink);

  freeLink(gRootMemLink);

  return ret;
}

void pushConvertedVertexIntoDiffInfoStackWithoutOverlap(Stack *stack,ConvertedGraphVertex *cVertex){
  if(cVertex != NULL){
    if(!cVertex->isPushedIntoDiffInfoStack){
      pushStack(stack,cVertex);
      cVertex->isPushedIntoDiffInfoStack = TRUE;
    }
  }
  return;
}

//[%] Stackを追加すべき処理
ConvertedGraphVertex *popConvertedVertexFromDiffInfoStackWithoutOverlap(Stack *stack){
  ConvertedGraphVertex *ret = popStack(stack);
  ret->isPushedIntoDiffInfoStack = FALSE;

  return ret;
}

void LMNtalLinkDump(LMNtalLink *link){
  /* link-attrが255の時は(多分)終端記号なので出力しない */
  /* ↑グローバルルート膜に所属してることを示しているのでそのまま表示 */
  // if(link->attr!=255){
    fprintf(stdout,"<%d,%d>",link->attr,link->data.ID);    
  // }
  return;
}

void convertedGraphVertexDump(ConvertedGraphVertex *cVertex){
  int i;

  if(cVertex->type == convertedAtom){
    fprintf(stdout,"type:ATOM\n");
  }else if(cVertex->type == convertedHyperLink){
    fprintf(stdout,"type:HYPERLINK\n");
  }

  fprintf(stdout,"ID:%d\n",cVertex->ID);
  fprintf(stdout,"name:%s\n",cVertex->name);

  fprintf(stdout,"links:");
  for(i=0;i<numStack(cVertex->links);i++){
    if(i!=0){
      fprintf(stdout,",");
    }
    LMNtalLinkDump((LMNtalLink *)readStack(cVertex->links,i));
  }
  fprintf(stdout,"\n");
}

void convertedGraphVertexDumpCaster(void *cVertex){
  convertedGraphVertexDump((ConvertedGraphVertex *)cVertex);
  return;
}

void convertedGraphDump(ConvertedGraph *cGraph){
  fprintf(stdout,"CONVERTED ATOMS:\n");
  dynamicArrayDump(cGraph->atoms,convertedGraphVertexDumpCaster);
  fprintf(stdout,"CONVERTED HYPERLINKS:\n");
  dynamicArrayDump(cGraph->hyperLinks,convertedGraphVertexDumpCaster);
  return;
}

void freeLinkCaster(void *link){
  freeLink((LMNtalLink *)link);
  return;
}

void freeConvertedGraphVertex(ConvertedGraphVertex *cVertex){
  freeStackAndValues(cVertex->links,freeLinkCaster);
  free(cVertex);
  return;
}

void freeConvertedGraphVertexCaster(void *cVertex){
  freeConvertedGraphVertex((ConvertedGraphVertex *)cVertex);
  return;
}

void freeConvertedGraph(ConvertedGraph *cGraph){
  freeDynamicArrayAndValues(cGraph->atoms,freeConvertedGraphVertexCaster);
  freeDynamicArrayAndValues(cGraph->hyperLinks,freeConvertedGraphVertexCaster);
  return;
}

ConvertedGraphVertex *correspondingVertexInConvertedGraph(InheritedVertex *iVertex,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID){
  int afterID = iVertex->beforeID + gapOfGlobalRootMemID;

  switch(iVertex->type){
    case convertedAtom:
      return (ConvertedGraphVertex *)readDynamicArray(cAfterGraph->atoms,afterID);
      break;
    case convertedHyperLink:
      return (ConvertedGraphVertex *)readDynamicArray(cAfterGraph->hyperLinks,afterID);
      break;
    default:
      CHECKER("unexpected vertex type\n");
      exit(EXIT_FAILURE);
      break;
  }
}

ConvertedGraphVertex *getConvertedVertexFromGraphAndIDAndType(ConvertedGraph *cGraph,int ID,ConvertedGraphVertexType type){
  switch(type){
    case convertedAtom:
      return (ConvertedGraphVertex *)readDynamicArray(cGraph->atoms,ID);
      break;
    case convertedHyperLink:
      return (ConvertedGraphVertex *)readDynamicArray(cGraph->hyperLinks,ID);
      break;
    default:
      CHECKER("unexpected vertex type\n");
      exit(EXIT_FAILURE);
      break;
  }
}

void getNextDistanceConvertedVertices(Stack *BFSStack,Stack *initializeConvertedVerticesStack,ConvertedGraph *cAfterGraph){
  Stack *nextBFSStack = makeStack();

  while(!isEmptyStack(BFSStack)){
    ConvertedGraphVertex *cVertex = popStack(BFSStack);

    int i;
    for(i=0;i<numStack(cVertex->links);i++){
      LMNtalLink *link = readStack(cVertex->links,i);
      ConvertedGraphVertex *adjacentVertex;

      switch(link->attr){
        case INTEGER_ATTR:
          break;
        case DOUBLE_ATTR:
          break;
        case STRING_ATTR:
          break;
        case HYPER_LINK_ATTR:
          adjacentVertex = (ConvertedGraphVertex *)readDynamicArray(cAfterGraph->hyperLinks,link->data.ID);
          if(!adjacentVertex->isVisitedInBFS){
            pushStack(nextBFSStack,adjacentVertex);
            adjacentVertex->isVisitedInBFS = TRUE;
          }
          break;
        case GLOBAL_ROOT_MEM_ATTR:
          break;
        default:
          if(link->attr < 128){
            adjacentVertex = (ConvertedGraphVertex *)readDynamicArray(cAfterGraph->atoms,link->data.ID);
            if(!adjacentVertex->isVisitedInBFS){
              pushStack(nextBFSStack,adjacentVertex);
              adjacentVertex->isVisitedInBFS = TRUE;
            }
          }else{
            CHECKER("unexpected vertex type\n");
            exit(EXIT_FAILURE);
          }
          break;
      }
    }

    pushStack(initializeConvertedVerticesStack,cVertex);
  }

  swapStack(nextBFSStack,BFSStack);
  freeStack(nextBFSStack);

  return;
}


