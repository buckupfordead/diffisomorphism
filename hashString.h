#include"convertedGraph.h"
#include"trie.h"
#include"adjacent.h"

Hash fnvHash(char *str);

Hash labelHashValue(CanonicalLabel label);

Hash adjacentListHashValue(List *adjacent);

void calcAdjacentInfoHashValue(AdjacentInfo *adjacentInfo);

Hash inverse(Hash hash);