#ifndef _DIFF_H
#define _DIFF_H

#include"json.h"
#include"util.h"
#include"collection.h"
// #include"trie.h"

typedef struct _DiffInfo{
  Stack *deletedVertices;
  Stack *addedVertices;
  Stack *relinkedVertices;
} DiffInfo;

#include"convertedGraph.h"

enum {
  BEFORE,
  AFTER
} ;

typedef struct _GraphInfo{
  json_char jsonString[BUFF_LENGTH];
  json_value * jsonValue;
  int stateID;
  ConvertedGraph *convertedGraph;
  int globalRootMemID;
} GraphInfo;

typedef struct _Diff{
  GraphInfo *parentGraphInfo;
  DynamicArray *childrenGraphInfo;
} Diff;

Diff *makeDiff();
void freeDiff();
Bool succRead(int *succPtr);
//int diffRead(json_char * beforeJson,json_char * afterJson);
int scanState();
void scanSuccessorGraphInfos(Diff *diff,int succ,Bool fullScan);
GraphInfo *makeEmptyGrpahInfo();
DiffInfo *compareGraphInfo(GraphInfo *beforeGraphInfo,GraphInfo *afterGraphInfo,int gapOfGlobalRootMemID);
void freeGraphInfo(GraphInfo *graphInfo);
void freeGraphInfoCaster(void *graphInfo);
void freeDiffInfo(DiffInfo *diffInfo);
void diffInfoDump(DiffInfo *diffInfo);
void freeDiff(Diff *diff);
void diffDump(Diff *diff);
void freeDiffCaster(void *d);
void diffDumpCaster(void *d);

/*$*/
GraphInfo * scanGraphInfoForSlim(char *buff);
void scanGraphInfoAndStateIDForSlim(Diff *diff, char *buff, int stateID);
/*$*/

#endif
