#include <stdio.h>
#include "util.h"
#include <string.h>
#include <stdlib.h>

void richGets(char *buff){

  fgets(buff,BUFF_LENGTH,stdin);

  //文字数オーバーなら異常終了
  if(strlen(buff) == BUFF_LENGTH - 1){
    fprintf(stderr,"string length is over\n");
    exit( EXIT_FAILURE );
  }

  buff[strlen(buff) - 1] = '\0';
  //printf("buff:%s\n",buff);

  return;
}


