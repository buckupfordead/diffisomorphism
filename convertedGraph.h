#ifndef _CONVERTED_GRAPH_H
#define _CONVERTED_GRAPH_H

#include<stdint.h>
#include"util.h"
#include"collection.h"
#include"json.h"
#include"hash.h"

typedef struct _ConvertedGraph{
  DynamicArray *atoms;
  DynamicArray *hyperLinks;
}ConvertedGraph;



#define NAME_LENGTH 256

typedef enum {
  convertedNone,
  convertedAtom,
  convertedHyperLink,
  convertedNull
} ConvertedGraphVertexType;

#include"trie.h"

typedef union {
  int integer;
  double dbl;
  char string[NAME_LENGTH];
  int ID;
}LMNtalData;

//to do :dataのdoubleとstringへの対応。
typedef struct _LMNtalLink{
  int attr;
  LMNtalData data;
} LMNtalLink;

#define INTEGER_ATTR 128
#define DOUBLE_ATTR 129
#define STRING_ATTR 131
#define HYPER_LINK_ATTR 138
#define GLOBAL_ROOT_MEM_ATTR 255

typedef struct _ConvertedGraphVertex{
  ConvertedGraphVertexType type;
  Stack *links;
  int ID;
  char name[NAME_LENGTH];
  Bool isPushedIntoDiffInfoStack;
  Bool isVisitedInBFS;
  struct _InheritedVertex *correspondingVertexInTrie;
} ConvertedGraphVertex;

ConvertedGraph *makeEmptyConvertedGraph();
ConvertedGraph *convertGraph(json_value *jVal);
void pushConvertedVertexIntoDiffInfoStackWithoutOverlap(Stack *stack,ConvertedGraphVertex *cVertex);
ConvertedGraphVertex *popConvertedVertexFromDiffInfoStackWithoutOverlap(Stack *stack);
void convertedGraphVertexDump(ConvertedGraphVertex *cVertex);
void convertedGraphVertexDumpCaster(void *cVertex);
void convertedGraphDump(ConvertedGraph *cGraph);
void freeConvertedGraph(ConvertedGraph *cGraph);
int LMNtalID(json_value *jVal);
Bool isEqualLinks(LMNtalLink *linkA,LMNtalLink *linkB);
Bool isHyperLink(LMNtalLink *link);
ConvertedGraphVertex *correspondingVertexInConvertedGraph(struct _InheritedVertex *iVertex,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID);
ConvertedGraphVertex *getConvertedVertexFromGraphAndIDAndType(ConvertedGraph *cGraph,int ID,ConvertedGraphVertexType type);
void getNextDistanceConvertedVertices(Stack *BFSStack,Stack *initializeConvertedVerticesStack,ConvertedGraph *cAfterGraph);





#endif
