for type in {1..3}
do
	for tg in "cano" "adja"
	do
		txt="grep/phi_${tg}_${type}.txt"
		echo "phi_03-${type}" > ${txt}
			grep ${tg} phi/phi_03-${type}-??.txt >> ${txt}
		for size in {04..12}
		do
			echo "" >> ${txt}
			echo "phi_${size}-${type}" >> ${txt}
				grep ${tg} phi/phi_${size}-${type}-??.txt >> ${txt}
		done
		# echo "done" >> ${txt}
	done
done

for type in {1..2}
do
	for tg in "cano" "adja"
	do
		txt="grep/bubble_${tg}_${type}.txt"
		echo "bubble_03-${type}" > ${txt}
			grep ${tg} bubble/bubble_03-${type}-??.txt >> ${txt}
		for size in {04..12}
		do
			echo "" >> ${txt}
			echo "bubble_${size}-${type}" >> ${txt}
				grep ${tg} bubble/bubble_${size}-${type}-??.txt >> ${txt}
		done
		# echo "done" >> ${txt}
	done
done
