#include<sys/time.h>
#include<stdio.h>

double startTime;
double endTime;

double beforeTime;
double afterTime;

int countOfStates = -1;
int countOfTransition = 0;
int countOfSortedInTrie = 0;
int countOfNotSortedInTrie = 0;

double triePropagateTime = 0.0;
double listMcKayTime = 0.0;
double addSerialTime = 0.0;
double rewriteAdjacentTime = 0.0;

double get_dtime(void){
  struct timeval tv;
  gettimeofday(&tv,NULL);
  return ((double)(tv.tv_sec) + (double)(tv.tv_usec) * 0.001 * 0.001);
}

