#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"json.h"
#include"jsonValueDump.h"
#include"diff.h"
#include"util.h"
#include"convertedGraph.h"
#include"collection.h"


#define FALSE (0)
#define TRUE (!FALSE)

Diff *makeDiff(){
  Diff *diff = (Diff *)malloc(sizeof(Diff));
  diff->parentGraphInfo = NULL;
  diff->childrenGraphInfo = makeDynamicArray();
  return diff;
}

Bool succRead(int *succPtr){
  char buff[BUFF_LENGTH];

  richGets(buff);

  if(!strcmp("Succ number Information",buff)){
    richGets(buff);
    scanf(" %d\n",succPtr);
    return TRUE;
  }else{
    return FALSE;
  }
}

/*
int diffRead(json_char * beforeJson,json_char * afterJson){
  char buff[BUFF_LENGTH];

  richGets(buff);

  if(!strcmp("Difference Information",buff)){
    richGets(buff);
    richGets(buff);
    richGets(beforeJson);
    richGets(buff);
    richGets(afterJson);
    return TRUE;
  }else{
    return FALSE;
  }
}
//*/
GraphInfo * scanGraphInfo(){
  char buff[BUFF_LENGTH];
  GraphInfo *ret = (GraphInfo *)malloc(sizeof(GraphInfo));

  richGets(buff);
  // printf("%s\n",buff);
  richGets(buff);
  // printf("%s\n",buff);

  richGets(ret->jsonString);
  // printf("%s\n", ret->jsonString);
  ret->jsonValue = json_parse(ret->jsonString,BUFF_LENGTH);
  ret->stateID = -1;
  /* jsonValueの全容はわからないがグラフをコンバートしている */
  ret->convertedGraph = convertGraph(ret->jsonValue);
  ret->globalRootMemID = LMNtalID(ret->jsonValue);

  return ret;
}

int scanState(){
  char buff[BUFF_LENGTH];
  int ID;

  richGets(buff);
  richGets(buff);
  scanf(" %d\n",&ID);

  return ID;
}

void scanSuccessorGraphInfos(Diff *diff,int succ,Bool fullScan){

  int i;

  diff->parentGraphInfo = scanGraphInfo();

  for(i=0;i<succ;i++){
    GraphInfo *graphInfo = scanGraphInfo();
    writeDynamicArray(diff->childrenGraphInfo,i,graphInfo);

    if(!fullScan){
      return;
    }
  }

  diff->parentGraphInfo->stateID = scanState();

  for(i=0;i<succ;i++){
    GraphInfo *graphInfo = readDynamicArray(diff->childrenGraphInfo,i);
    graphInfo->stateID = scanState();
  }
  return;
}

GraphInfo * scanGraphInfoForSlim(char *buff){
  GraphInfo *ret = (GraphInfo *)malloc(sizeof(GraphInfo));

  strcpy(ret->jsonString,buff);
  // printf("%s\n", ret->jsonString);
  ret->jsonValue = json_parse(ret->jsonString,BUFF_LENGTH);
  ret->stateID = -1;
  ret->convertedGraph = convertGraph(ret->jsonValue);
  ret->globalRootMemID = LMNtalID(ret->jsonValue);

  return ret;
}

void scanGraphInfoAndStateIDForSlim(Diff *diff, char *buff, int stateID){
  diff->parentGraphInfo = scanGraphInfoForSlim(buff);
  diff->parentGraphInfo->stateID = stateID;
}

void checkRelink(ConvertedGraphVertex *beforeCAtom,ConvertedGraphVertex *afterCAtom,DynamicArray *afterConvertedHyperLinks,Stack *relinkedVertices){
  if(beforeCAtom != NULL && afterCAtom != NULL){
    int i;
    assert(numStack(beforeCAtom->links)==numStack(afterCAtom->links));
    for(i=0;i<numStack(beforeCAtom->links);i++){
      LMNtalLink *beforeLink = readStack(beforeCAtom->links,i);
      LMNtalLink *afterLink = readStack(afterCAtom->links,i);

      if(!isEqualLinks(beforeLink,afterLink)){
        pushConvertedVertexIntoDiffInfoStackWithoutOverlap(relinkedVertices,afterCAtom);
        if(isHyperLink(beforeLink)){
          pushConvertedVertexIntoDiffInfoStackWithoutOverlap(relinkedVertices,(ConvertedGraphVertex *)readDynamicArray(afterConvertedHyperLinks,beforeLink->data.ID));
        }
        if(isHyperLink(afterLink)){
          pushConvertedVertexIntoDiffInfoStackWithoutOverlap(relinkedVertices,(ConvertedGraphVertex *)readDynamicArray(afterConvertedHyperLinks,afterLink->data.ID));
        }
      }
    }
  }else if(beforeCAtom != NULL && afterCAtom == NULL){
    int i;
    for(i=0;i<numStack(beforeCAtom->links);i++){
      LMNtalLink *beforeLink = readStack(beforeCAtom->links,i);

      if(isHyperLink(beforeLink)){
        pushConvertedVertexIntoDiffInfoStackWithoutOverlap(relinkedVertices,(ConvertedGraphVertex *)readDynamicArray(afterConvertedHyperLinks,beforeLink->data.ID));
      }
    }
  }else if(beforeCAtom == NULL && afterCAtom != NULL){
    int i;
    for(i=0;i<numStack(afterCAtom->links);i++){
      LMNtalLink *afterLink = readStack(afterCAtom->links,i);

      if(isHyperLink(afterLink)){
        pushConvertedVertexIntoDiffInfoStackWithoutOverlap(relinkedVertices,(ConvertedGraphVertex *)readDynamicArray(afterConvertedHyperLinks,afterLink->data.ID));
      }
    }
  }else{
  }
  return;
}

GraphInfo *makeEmptyGrpahInfo(){
  GraphInfo *ret = (GraphInfo *)malloc(sizeof(GraphInfo));

  ret->jsonString[0] = '\0';
  ret->jsonValue = NULL;
  ret->stateID = -1;
  ret->convertedGraph = makeEmptyConvertedGraph();
  ret->globalRootMemID = 0;

  return ret;
}

DiffInfo *compareGraphInfo(GraphInfo *beforeGraphInfo,GraphInfo *afterGraphInfo,int gapOfGlobalRootMemID){
  DynamicArray *beforeConvertedAtoms = beforeGraphInfo->convertedGraph->atoms;
  DynamicArray *beforeConvertedHyperLinks = beforeGraphInfo->convertedGraph->hyperLinks;
  DynamicArray *afterConvertedAtoms = afterGraphInfo->convertedGraph->atoms;
  DynamicArray *afterConvertedHyperLinks = afterGraphInfo->convertedGraph->hyperLinks;

  /*$*/
  // emptygGraphInfo->stateIDは-1
  // これを利用して、通常のstateどうしの比較の時のみ書き換え前後のGraphInfoを表示
  if( beforeGraphInfo->stateID > -1 && afterGraphInfo->stateID > -1){
    printf(" --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
    printf(" beforeGraphInfo->stateID : %d, afterGraphInfo->stateID : %d\n", beforeGraphInfo->stateID, afterGraphInfo->stateID);
    fprintf(stdout,"\n --- before ---\n");
    dynamicArrayDump(beforeConvertedAtoms, convertedGraphVertexDumpCaster);
    fprintf(stdout,"\n --- before end ---\n");
    fprintf(stdout,"\n --- after ---\n");
    dynamicArrayDump(afterConvertedAtoms, convertedGraphVertexDumpCaster);
    fprintf(stdout,"\n --- after end ---\n\n");
  }
  /*$*/

  DiffInfo *diffInfo = (DiffInfo *)malloc(sizeof(DiffInfo));
  diffInfo->deletedVertices = makeStack();
  diffInfo->addedVertices = makeStack();
  diffInfo->relinkedVertices = makeStack();

  int begin = MIN(0,-gapOfGlobalRootMemID);
  int end = MAX(MAX(beforeConvertedAtoms->cap,afterConvertedHyperLinks->cap-gapOfGlobalRootMemID),MAX(afterConvertedAtoms->cap,afterConvertedHyperLinks->cap-gapOfGlobalRootMemID));

  int i;

  for(i=begin;i<end;i++){
    ConvertedGraphVertex *beforeCHyperLink= readDynamicArray(beforeConvertedHyperLinks,i);
    ConvertedGraphVertex *afterCHyperLink = readDynamicArray(afterConvertedHyperLinks,i);

    if(beforeCHyperLink != NULL && afterCHyperLink == NULL){
      pushConvertedVertexIntoDiffInfoStackWithoutOverlap(diffInfo->deletedVertices,beforeCHyperLink);
    }else if(beforeCHyperLink == NULL && afterCHyperLink != NULL){
      pushConvertedVertexIntoDiffInfoStackWithoutOverlap(diffInfo->addedVertices,afterCHyperLink);
    }
  }

  for(i=begin;i<end;i++){
    ConvertedGraphVertex *beforeCAtom = readDynamicArray(beforeConvertedAtoms,i);
    ConvertedGraphVertex *afterCAtom = readDynamicArray(afterConvertedAtoms,i);

    if(beforeCAtom != NULL && afterCAtom == NULL){
      pushConvertedVertexIntoDiffInfoStackWithoutOverlap(diffInfo->deletedVertices,beforeCAtom);
      checkRelink(beforeCAtom,afterCAtom,afterConvertedHyperLinks,diffInfo->relinkedVertices);
    }else if(beforeCAtom == NULL && afterCAtom != NULL){
      pushConvertedVertexIntoDiffInfoStackWithoutOverlap(diffInfo->addedVertices,afterCAtom);
      checkRelink(beforeCAtom,afterCAtom,afterConvertedHyperLinks,diffInfo->relinkedVertices);
    }else if(beforeCAtom != NULL && afterCAtom != NULL){
      checkRelink(beforeCAtom,afterCAtom,afterConvertedHyperLinks,diffInfo->relinkedVertices);
    }
  }

  return diffInfo;
}

void freeGraphInfo(GraphInfo *graphInfo){
  json_value_free(graphInfo->jsonValue);
  freeConvertedGraph(graphInfo->convertedGraph);
  free(graphInfo);
  return;
}

void freeGraphInfoCaster(void *graphInfo){
  freeGraphInfo((GraphInfo *)graphInfo);
  return;
}

void freeDiffInfo(DiffInfo *diffInfo){
  freeStack(diffInfo->deletedVertices);
  freeStack(diffInfo->addedVertices);
  freeStack(diffInfo->relinkedVertices);
  free(diffInfo);
  return;
}

void diffInfoDump(DiffInfo *diffInfo){
  fprintf(stdout,"\n\n --- diff ---\n\n");
  fprintf(stdout,"deletedVertices:\n\n");
  stackDump(diffInfo->deletedVertices,convertedGraphVertexDumpCaster);
  fprintf(stdout,"addedVertices:\n\n");
  stackDump(diffInfo->addedVertices,convertedGraphVertexDumpCaster);
  fprintf(stdout,"relinkedVertices:\n\n");
  stackDump(diffInfo->relinkedVertices,convertedGraphVertexDumpCaster);
  fprintf(stdout," --- diff end ---\n\n");
  return;
}

void freeDiff(Diff *diff){
  freeGraphInfo(diff->parentGraphInfo);
  freeDynamicArrayAndValues(diff->childrenGraphInfo,freeGraphInfoCaster);
  free(diff);
  return;
}

void graphInfoDump(GraphInfo *graphInfo){
  fprintf(stdout,"\n");
  fprintf(stdout,"JSON string:\n");
  fprintf(stdout,"%s\n",graphInfo->jsonString);
  fprintf(stdout,"stateID:%d\n",graphInfo->stateID);
  fprintf(stdout,"JSON value:\n");
  jsonDump(graphInfo->jsonValue);
  fprintf(stdout,"\n");
  return;
}

void graphInfoDumpCaster(void *graphInfo){
  graphInfoDump((GraphInfo *)graphInfo);
  return;
}

void diffDump(Diff *diff){
  fprintf(stdout,"PARENT:\n");
  graphInfoDump(diff->parentGraphInfo);
  fprintf(stdout,"CHILDREN:\n");
  dynamicArrayDump(diff->childrenGraphInfo,graphInfoDumpCaster);
  return;
}

void freeDiffCaster(void *d){
  freeDiff((Diff *)d);
  return;
}

void diffDumpCaster(void *d){
  diffDump((Diff *)d);
  return;
}



