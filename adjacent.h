#ifndef _adjacent_H
#define _adjacent_H

#include"collection.h"
#include"hash.h"
#include"diff.h"
#include"convertedGraph.h"
#include"util.h"

/*
  差分情報生成時に使用する各情報
  adjacentInfo:差分表現のための特定の頂点のラベルと隣接情報ラベルの集合を表す構造体
  参考：CanonicalLabelの定義
  typedef struct _CanonicalLabel{
    Hash first;
    int second;
  } CanonicalLabel;
*/

typedef struct _AdjacentInfo{
  CanonicalLabel myself;      //自分自身のCanonicalLabel
  List *adjacent;             //隣接情報のCanonicalLabelを格納
  Hash hash_myself;           //自分自身のhash(indexに使用)
  Hash hash_info;             //AdjacentInfo全体のhash
} AdjacentInfo;

// adjacentInfoOfGraph:Graph内のadjacentInfoを全て集めた構造体
typedef struct _AdjacentInfoOfGraph{
  //hash値の下2バイトで割り当てるStackを格納するDynamicArray
  //頂点ごとのadjacentInfoはhash値を基にHashTable方式で
  //adjacents->body[hash&0x0000FFFF]にあるStackに格納する
  DynamicArray *adjacents;
  Hash sum;
  Hash mul;
} AdjacentInfoOfGraph;

// 書換え前後それぞれの正規ラベルを格納するための構造体
typedef struct _Labels{
  int numOfAtoms;                 // Atomの個数
  int numOfHyperLinks;            // HyperLinkの個数
  CanonicalLabel *atomLabels;
  CanonicalLabel *hyperLinkLabels;
} Labels;

// 頂点ごとのラベルの変化を表す列挙型
typedef enum Status{
  NONE,     //変更なし
  ADDED,    //Stackに追加済
  UPDATED   //ラベル更新済
}Status;

// adjacentInfo及びadjacentInfoOfGraphを扱う為の関数
AdjacentInfo *makeAdjacentInfo();
void freeadjacentInfo(AdjacentInfo *adjacentInfo);
void dumpAdjacentInfo(AdjacentInfo *adjacentInfo);
AdjacentInfoOfGraph *makeAdjacentInfoOfGraph();
void freeAdjacentInfoOfGraph(AdjacentInfoOfGraph *adjacentInfoOfGraph);
void printAdjacentGraphHash(AdjacentInfoOfGraph *adjacentInfoOfGraph);
void dumpAdjacentInfoOfGraph(AdjacentInfoOfGraph *adjacentInfoOfGraph);

// CanonicalLabelの配列を簡単に扱うための関数
CanonicalLabel *initializeCanonicalLabels(int numOfLabels);
void dumpCanonicalLabels(CanonicalLabel *labels, int numOfLabels);
// atomLabelsとHyperLinkLabelsを同時にダンプするための関数
void dumpTwoCanonicalLabels(CanonicalLabel *atomLabels,
                   CanonicalLabel *hyperLinkLabels,
                   int numOfAtoms,
                   int numOfHyperLinks);

// Labelsを扱う為の関数
Labels *makeLabels(int numOfAtoms, int numOfHyperLinks);
void freeLabels(Labels *labels);
void dumpLabels(Labels *labels);

// inheritedVertexが1対1で対応するatomかhyperLinkのIDの場所にcanonicalLabelを格納する関数
void collectLabelFromIVertexToLabels(InheritedVertex *iVertex, Labels *labels, int gapOfGlobalRootMemID);

//トライ木とConvertedGraphから状態全体の周辺情報を集める関数
AdjacentInfoOfGraph *collectAdjacentInfoOfGraph(Trie *trie, ConvertedGraph *cgraph, int gapOfGlobalRootMemID);

//ConvertedGraphとLabelsから状態全体の周辺情報を集める関数
AdjacentInfoOfGraph *collectAdjacentInfoOfGraphFromLabels(ConvertedGraph *cgraph, Labels *labels, int gapOfGlobalRootMemID);

//・書換え前のLabels & AdjacentInfoOfGraph
//・書換え時に消去された頂点のStack:deletedVertices
//・書換え後のLabels & ConvertedGraph
//・(gapOfGlobalRootMemID)
// から
//・書換え前後の差分(対称差)
// を生成して
//・書換え後のAdjacentInfoOfGraph
// を返す関数
AdjacentInfoOfGraph *makeAfterAndDiffAdjacentInfoOfGraph(Labels *beforeLabels,
                                                         AdjacentInfoOfGraph *beforeAdjacentInfoOfGraph,
                                                         Stack *deletedVertices,
                                                         Labels *afterLabels,
                                                         ConvertedGraph *afterGraph,
                                                         int gapOfGlobalRootMemID,
                                                         AdjacentInfoOfGraph *diffAdjacentInfoOfGraph);

void collectLabelFromTrie(Trie *trie,
                          CanonicalLabel *atomLabels,
                          CanonicalLabel *hyperLinkLabels,
                          int gapOfGlobalRootMemID);

void collectLabelFromIVertex(InheritedVertex *iVertex, 
                             CanonicalLabel *atomLabels,
                             CanonicalLabel *hyperLinkLabels,
                             int serial,
                             int gapOfGlobalRootMemID);

int *searchDifferentLabels(CanonicalLabel *beforeLabels,
                           CanonicalLabel *afterLabels,
                           int numOfBeforeLabel,
                           int numOfAfterLabel);

// canonicalDiscreteRefinement(McKayでは判別しきれない頂点のリスト)
// の正規ラベルに通し番号を振り、その情報をLabelsの対応するラベルに入れる関数
void addSerialNumberFromCanonicalDiscreteRefinement(Labels *labels,
                                                    List *canonicalDiscreteRefinement,
                                                    int gapOfGlobalRootMemID,
                                                    Bool measure);

#endif
