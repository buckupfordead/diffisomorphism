#ifndef _MCKAY_H
#define _MCKAY_H

#include"hash.h"
#include"collection.h"
#include"trie.h"

typedef struct _CanonicalAdjacencyInformation{
  Hash hash;
  CanonicalLabel myLabel;
  Stack *adjacentLabels;
} CanonicalAdjacencyInformation;

typedef struct _CanonicalAdjacencyList{
  Hash hashSum;
  Hash hashMul;
  Stack *adjacencyInformations;
} CanonicalAdjacencyList;

//[!] もしLabelsを更新するならこの処理に追加
List *trieMcKay(Trie *trie,DiffInfo *diffInfo,ConvertedGraph *cAfterGraph,ConvertedGraph *cBeforeGraph,int gapOfGlobalRootMemID,Bool measure);
Order compareDiscretePropagationListOfInheritedVerticesWithAdjacentLabels(List *listA,List *listB);
void freePreserveDiscreteProapgationList(List *pdpList);
void freePreserveDiscreteProapgationListCaster(void *pdpList);
Bool checkIsomorphismValidity(DynamicArray *slimKeyCollection,RedBlackTree *McKayKeyCollection,List *canonicalDiscreteRefinement,int stateID);

/* ---ここまでmiyahara作成--- */

/* ---ここからsakazume作成--- */
#include"adjacent.h"
//[#] Labelsを追加した処理
//[%%] Stackを追加した処理
List *trieMcKayWithLabelsAndStack(Trie *trie,
								  DiffInfo *diffInfo,
								  ConvertedGraph *cAfterGraph,
								  ConvertedGraph *cBeforeGraph,
								  int gapOfGlobalRootMemID,
								  Bool measure,
								  Labels *labels,
								  Stack *deletedStack);

//[#] Labelsを追加した処理
List *trieMcKayWithLabels(Trie *trie,DiffInfo *diffInfo,ConvertedGraph *cAfterGraph,ConvertedGraph *cBeforeGraph,int gapOfGlobalRootMemID,Bool measure,Labels *labels);
#endif
