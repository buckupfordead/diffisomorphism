#include<stdio.h>
#include<string.h>
#include"diff.h"
#include"jsonValueDump.h"
#include"collection.h"
#include"convertedGraph.h"
#include"trie.h"
#include"McKay.h"
#include"hash.h"
#include"experiment.h"
#include"adjacent.h"

#define FULLMODE(CODE) 
#define SINGLEMODE(CODE) CODE
#define DEBUG_SWITCH 1

//to do:rules
int main(void){
  SINGLEMODE(int isFirstLoop = TRUE;)

  startTime = get_dtime();

  //*
  Diff *diff = makeDiff();
  Trie *trie = makeTrie();
  GraphInfo *emptyGraphInfo = makeEmptyGrpahInfo();
  List *canonicalDiscreteRefinement;
  DynamicArray *slimKeyCollection = makeDynamicArray();
  RedBlackTree *McKayKeyCollection = makeRedBlackTree();

  int succ;
  int i;

  /* Succ number Informationと次の行を読み込み */
  /* 先頭行がSucc number Informationでない場合はループを抜ける */
  while(succRead(&succ)){
    countOfStates++;

    //printf("succ is %d\n",succ);

    /* グラフ情報を読み込み 
         ・diff->parentGraphInfo 
         ・diff->childrenGraphInfo
         ・diff->parentGraphInfo->stateID
         ・diff->stateID
    */
    // printf("%lu : %d\n",__LINE__, isFirstLoop);
    FULLMODE( scanSuccessorGraphInfos(diff,succ,TRUE);)
    SINGLEMODE( scanSuccessorGraphInfos(diff,succ,isFirstLoop);)

    //diffDump(diff);

    if(isFirstLoop){
      SINGLEMODE(isFirstLoop = FALSE;)
      continue;
    }
    //#######################from empty##################################

    int gapOfGlobalRootMemID = diff->parentGraphInfo->globalRootMemID - emptyGraphInfo->globalRootMemID;
    // printf(" --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
    DiffInfo *diffInfo = compareGraphInfo(emptyGraphInfo,diff->parentGraphInfo,gapOfGlobalRootMemID);

    /*$* */
    Labels *parentGraphLabels = makeLabels(diff->parentGraphInfo->convertedGraph->atoms->cap,diff->parentGraphInfo->convertedGraph->hyperLinks->cap);
    // fprintf(stdout,"\n\n --- parent graph info ---\n\n");
    // convertedGraphDump(diff->parentGraphInfo->convertedGraph);
    // fprintf(stdout,"\n --- parent graph info end ---\n");
    /* *$*/

    //diffInfoDump(diffInfo);

    //CHECKER("###FROM EMPTY###\n");

    // printf(" --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
    //[!] もしLabelsを更新するならこの処理に追加
    // canonicalDiscreteRefinement = trieMcKay(trie,diffInfo,diff->parentGraphInfo->convertedGraph,emptyGraphInfo->convertedGraph,gapOfGlobalRootMemID,FALSE);
    //[#] Labelsを追加した処理
    canonicalDiscreteRefinement = trieMcKayWithLabels(trie,diffInfo,diff->parentGraphInfo->convertedGraph,emptyGraphInfo->convertedGraph,gapOfGlobalRootMemID,FALSE,parentGraphLabels);
    // collectLabelFromTrie(trie,parentGraphLabels->atomLabels,parentGraphLabels->hyperLinkLabels,gapOfGlobalRootMemID);
    // fprintf(stdout,"\n\n --- canonicalDiscreteRefinement ---\n\n");
    // listDump(canonicalDiscreteRefinement,inheritedVertexDumpCaster);
    // fprintf(stdout,"\n");
    addSerialNumberFromCanonicalDiscreteRefinement(parentGraphLabels,canonicalDiscreteRefinement,gapOfGlobalRootMemID,FALSE);
    freePreserveDiscreteProapgationList(canonicalDiscreteRefinement);
    // dumpLabels(parentGraphLabels);
    // collectAdjacentInfoOfGraphFromLabels(diff->parentGraphInfo->convertedGraph,parentGraphLabels,gapOfGlobalRootMemID);
    /*$*/
    // fprintf(stdout, "\n");
    // printf(" --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
    // fprintf(stdout,"\n --- trie ---\n\n");
    // trieDump(trie);
    // fprintf(stdout,"\n --- trie end ---\n\n");
    /*$*/

    freeDiffInfo(diffInfo);
    //Bool isValidParent = checkIsomorphismValidity(slimKeyCollection,McKayKeyCollection,canonicalDiscreteRefinement,diff->parentGraphInfo->stateID);

    FULLMODE(for(i=0;i<succ;i++){)
    SINGLEMODE(for(i=0;i<1;i++){)
      if(!isFirstLoop) terminationConditionInfoDumpExperimentFromTrie(trie);

      GraphInfo *graphInfo = readDynamicArray(diff->childrenGraphInfo,i);

      /*$*/
      // fprintf(stdout,"\n --- child graph info ---\n\n");
      // convertedGraphDump(graphInfo->convertedGraph);
      // fprintf(stdout,"\n --- child graph info end ---\n");
      /*$*/

      gapOfGlobalRootMemID = graphInfo->globalRootMemID - diff->parentGraphInfo->globalRootMemID;

      //#######################forward##################################

      /* グラフの差分を計算してdiffinfoに格納 */
      /*$*/
      // fprintf(stdout,"\n\n --- forword ---\n\n");
      // printf(" --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
      /*$*/
      diffInfo = compareGraphInfo(diff->parentGraphInfo,graphInfo,gapOfGlobalRootMemID);

      /*$* */
      Labels *childGraphLabels = makeLabels(graphInfo->convertedGraph->atoms->cap,graphInfo->convertedGraph->hyperLinks->cap);
      // diffInfoDump(diffInfo);
      /* *$*/
      //CHECKER("###FORWARD###\n");
      // printf(" --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
      //[!] もしLabelsを更新するならこの処理に追加
      // FULLMODE(canonicalDiscreteRefinement = trieMcKay(trie,diffInfo,graphInfo->convertedGraph,diff->parentGraphInfo->convertedGraph,gapOfGlobalRootMemID,TRUE);)
      // SINGLEMODE(canonicalDiscreteRefinement = trieMcKay(trie,diffInfo,graphInfo->convertedGraph,diff->parentGraphInfo->convertedGraph,gapOfGlobalRootMemID,!isFirstLoop);)
      //[#] Labelsを追加した処理
      Stack *deletedVertices = makeStack();
      FULLMODE(canonicalDiscreteRefinement = trieMcKayWithLabelsAndStack(trie,diffInfo,graphInfo->convertedGraph,diff->parentGraphInfo->convertedGraph,gapOfGlobalRootMemID,TRUE,childGraphLabels,deletedVertices);)
      SINGLEMODE(canonicalDiscreteRefinement = trieMcKayWithLabelsAndStack(trie,diffInfo,graphInfo->convertedGraph,diff->parentGraphInfo->convertedGraph,gapOfGlobalRootMemID,!isFirstLoop,childGraphLabels,deletedVertices);)
      /*$*/
      // fprintf(stdout,"\n\n --- canonicalDiscreteRefinement ---\n\n");
      // listDump(canonicalDiscreteRefinement,inheritedVertexDumpCaster);
      // collectLabelFromTrie(trie,childGraphLabels->atomLabels,childGraphLabels->hyperLinkLabels,gapOfGlobalRootMemID);
      addSerialNumberFromCanonicalDiscreteRefinement(childGraphLabels,canonicalDiscreteRefinement,gapOfGlobalRootMemID,TRUE);
      AdjacentInfoOfGraph *parentAdjacentInfoOfGraph = collectAdjacentInfoOfGraphFromLabels(diff->parentGraphInfo->convertedGraph,parentGraphLabels,gapOfGlobalRootMemID);
      // dumpAdjacentInfoOfGraph(parentAdjacentInfoOfGraph);
      printf("before Rewrite ... ");
      printAdjacentGraphHash(parentAdjacentInfoOfGraph);
      AdjacentInfoOfGraph *childAdjacentInfoOfGraph = makeAdjacentInfoOfGraph();
      AdjacentInfoOfGraph *diffAdjacentInfoOfGraph = makeAdjacentInfoOfGraph();
      // printf("\n --- file:%s...func:%s...line:%d ---\n\n",__FILE__,__func__,__LINE__);
      // diffInfo = compareGraphInfo(diff->parentGraphInfo,graphInfo,gapOfGlobalRootMemID);
      childAdjacentInfoOfGraph = makeAfterAndDiffAdjacentInfoOfGraph(parentGraphLabels,parentAdjacentInfoOfGraph,deletedVertices,childGraphLabels,graphInfo->convertedGraph,gapOfGlobalRootMemID,diffAdjacentInfoOfGraph);
      // dumpAdjacentInfoOfGraph(childAdjacentInfoOfGraph);
      printf(" after Rewrite ... ");
      printAdjacentGraphHash(childAdjacentInfoOfGraph);
      // printf("\n --- file:%s...func:%s...line:%d ---\n\n",__FILE__,__func__,__LINE__);
      /*$*/
      // fprintf(stdout,"\n\n --- trie ---\n\n");
      // trieDump(trie);
      // fprintf(stdout,"\n --- trie end ---\n\n");
      // fprintf(stdout,"\n\n --- PARENT GRAPH EXPRESSION ---\n");
      // collectAdjacentInfoOfGraphFromLabels(diff->parentGraphInfo->convertedGraph,parentGraphLabels,gapOfGlobalRootMemID);
      // dumpAdjacentInfoOfGraph(parentAdjacentInfoOfGraph);
      // fprintf(stdout,"\n --- CHILD GRAPH EXPRESSION (only diff) ---\n");
      // collectAdjacentInfoOfGraphFromLabels(graphInfo->convertedGraph,childGraphLabels,gapOfGlobalRootMemID);
      // printf("\n");

      /* デバッグ用書き換え後グラフ表示 */
      collectLabelFromTrie(trie,childGraphLabels->atomLabels,childGraphLabels->hyperLinkLabels,gapOfGlobalRootMemID);
      addSerialNumberFromCanonicalDiscreteRefinement(childGraphLabels,canonicalDiscreteRefinement,gapOfGlobalRootMemID,FALSE);
      // fprintf(stdout,"\n --- CHILD GRAPH EXPRESSION (full) ---\n\n");
      collectAdjacentInfoOfGraphFromLabels(graphInfo->convertedGraph,childGraphLabels,gapOfGlobalRootMemID);
      // dumpAdjacentInfoOfGraph(childAdjacentInfoOfGraph);
      printf(" after  debug  ... ");
      printAdjacentGraphHash(childAdjacentInfoOfGraph);
      /* デバッグ用書き換え後グラフ表示ここまで */
      /*$*/
      // printf("\n --- file:%s...func:%s...line:%d ---\n\n",__FILE__,__func__,__LINE__);
      // dumpLabels(parentGraphLabels);
      // dumpLabels(childGraphLabels);
      // searchDifferentLabels(parentGraphLabels->atomLabels,childGraphLabels->atomLabels,parentGraphLabels->numOfAtoms,childGraphLabels->numOfAtoms);
      printf("\n");
      // printf("\n --- file:%s...func:%s...line:%d ---\n\n",__FILE__,__func__,__LINE__);
      /*$*/
      freePreserveDiscreteProapgationList(canonicalDiscreteRefinement);

      if(!isFirstLoop) terminationConditionInfoDumpExperimentFromTrie(trie);
    //Bool isValid = checkIsomorphismValidity(slimKeyCollection,McKayKeyCollection,canonicalDiscreteRefinement,graphInfo->stateID);
    /*  
if( commentout || 1 ){
    if(graphInfo->stateID == 4){
      trieDump(trie);
      printf("stateID:%d\n",graphInfo->stateID);
      exit(EXIT_FAILURE);
    }
    //*/
    /*
    if(!isValid){
      CHECKER("########### INVALID ISOMORPHISM #########\n");
      trieDump(trie);
      printf("stateID:%d\n",graphInfo->stateID);
      printf("child number:%d\n",i);
      exit(EXIT_FAILURE);
    }else{
      //CHECKER("########### VALID ISOMORPHISM #########\n");
    }
}
    //*/

      freeDiffInfo(diffInfo);

      //########################backward#################################

      /* グラフの差分を計算してdiffinfoに格納 */
      /*$* */
      // fprintf(stdout,"\n\n --- backword ---\n\n");
      // printf(" --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
      /* *$*/
//      diffInfo = compareGraphInfo(graphInfo,diff->parentGraphInfo,-gapOfGlobalRootMemID);
      /*$* */
      // diffInfoDump(diffInfo);
      /* *$*/

      //CHECKER("###BACKWARD###\n");

      // printf(" --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
//      FULLMODE(canonicalDiscreteRefinement = trieMcKay(trie,diffInfo,diff->parentGraphInfo->convertedGraph,graphInfo->convertedGraph,-gapOfGlobalRootMemID,IS_DIFFERENCE_APPLICATION_MODE);)
//      SINGLEMODE(canonicalDiscreteRefinement = trieMcKay(trie,diffInfo,diff->parentGraphInfo->convertedGraph,graphInfo->convertedGraph,-gapOfGlobalRootMemID,FALSE);)
      /*$* */
      // fprintf(stdout,"\n\n --- canonicalDiscreteRefinement ---\n\n");
      // listDump(canonicalDiscreteRefinement,inheritedVertexDumpCaster);
      // fprintf(stdout,"\n\n --- trie ---\n\n");
      // trieDump(trie);
      // fprintf(stdout,"\n --- trie end ---\n\n");
      /* *$*/
//      freePreserveDiscreteProapgationList(canonicalDiscreteRefinement);

//      freeDiffInfo(diffInfo);

      //#########################################################
    }

    //###########################to empty##############################
//    gapOfGlobalRootMemID = emptyGraphInfo->globalRootMemID - diff->parentGraphInfo->globalRootMemID;
//    diffInfo = compareGraphInfo(diff->parentGraphInfo,emptyGraphInfo,gapOfGlobalRootMemID);

    //diffInfoDump(diffInfo);

    //CHECKER("###TO EMPTY###\n");
//    canonicalDiscreteRefinement = trieMcKay(trie,diffInfo,emptyGraphInfo->convertedGraph,diff->parentGraphInfo->convertedGraph,gapOfGlobalRootMemID,FALSE);
//    freePreserveDiscreteProapgationList(canonicalDiscreteRefinement);

//    freeDiffInfo(diffInfo);
    //#########################################################

    freeGraphInfo(diff->parentGraphInfo);
    freeValuesOfDynamicArray(diff->childrenGraphInfo,freeGraphInfoCaster);

    SINGLEMODE(if(!isFirstLoop) break;)
    SINGLEMODE(isFirstLoop = FALSE;)
  }

  freeDynamicArrayAndValues(slimKeyCollection,freePreserveDiscreteProapgationListCaster);
  freeRedBlackTree(McKayKeyCollection);
  freeGraphInfo(emptyGraphInfo);
  freeTrie(trie);
  free(diff);
  //*/
  endTime = get_dtime();

if( 1 ){
  CHECKER("#################### SUCCESS #######################\n");
  int trueTransition;
  if(IS_DIFFERENCE_APPLICATION_MODE){
    printf("################### DIFF MODE ###################\n");
    printf("STATES:%d\n",countOfStates);
    trueTransition = countOfTransition/2;
    printf("TRANSITION:%d\n",trueTransition);
  }else{
    printf("################### NOT DIFF MODE ###################\n");
    printf("STATES:%d\n",countOfStates);
    trueTransition = countOfTransition;
    printf("TRANSITION:%d\n",countOfTransition);
  }

  printf("sorted     only in trie:%8d...%10.6f%%\n",countOfSortedInTrie,100*((double)countOfSortedInTrie)/((double)countOfTransition));
  printf("sorted not only in trie:%8d...%10.6f%%\n",countOfNotSortedInTrie,100*((double)countOfNotSortedInTrie)/((double)countOfTransition));
  // printf("    sum of   canonical labeling time:%11.6f[sec]\n",triePropagateTime+listMcKayTime);
  // printf("average of canonical labeling time:%11.6f[sec]\n",(triePropagateTime+listMcKayTime)/((double)trueTransition));
  printf("    sum of      triePropagation time:%11.6f[sec]...%10.6f%%\n",triePropagateTime,100*triePropagateTime/(triePropagateTime+listMcKayTime));
  // printf("average of    triePropagation time:%11.6f[sec]\n",triePropagateTime/((double)trueTransition));
  printf("    sum of            listMcKay time:%11.6f[sec]...%10.6f%%\n",listMcKayTime,100*listMcKayTime/(triePropagateTime+listMcKayTime));
  // printf("average of          listMcKay time:%11.6f[sec]\n",listMcKayTime/((double)trueTransition));

  printf("    sum of  add serial of label time:%11.6f[sec]\n",addSerialTime);
  printf("    -------------------------------------------------\n");
  printf("    sum of   canonical labeling time:%11.6f[sec]\n",triePropagateTime+listMcKayTime+addSerialTime);
  printf("    sum of rewrite adjacentinfo time:%11.6f[sec]\n",rewriteAdjacentTime);  


  double experimentTime = endTime - startTime;
  int second = (int)experimentTime;
  printf("\nexperiment time:%f[sec] = %d:%02d:%02d\n\n",experimentTime,second/3600,(second/60)%60,second%60);
  // fprintf(stderr,"\nexperiment time:%f[sec] = %d:%02d:%02d\n\n",experimentTime,second/3600,(second/60)%60,second%60);
}
  //*/

  return 0;
}





