#include"trie.h"
#include"convertedGraph.h"
#include<string.h>
#include"experiment.h"
#include"diff.h"

int compareTrieLeaves(TrieBody *a,TrieBody *b){
  if(a == b){
    return 0;
  }else if(a->key.u.ui32 < b->key.u.ui32){
    return -1;
  }else if(a->key.u.ui32 > b->key.u.ui32){
    return 1;
  }else{
    int depthA = a->depth;
    int depthB = b->depth;
    HashString *hStringA = ((InheritedVertex *)peekList(a->inheritedVertices))->hashString;
    HashString *hStringB = ((InheritedVertex *)peekList(b->inheritedVertices))->hashString;

    int i;
    for(i=0;;i++){
      if(i >= depthA || i >= depthB){
        CHECKER("unexpected case\n");
        exit(EXIT_FAILURE);
      }

      Hash hashA = ((KeyContainer *)readDynamicArray(hStringA->body,i))->u.ui32;
      Hash hashB = ((KeyContainer *)readDynamicArray(hStringB->body,i))->u.ui32;

      if(hashA < hashB){
        return -1;
      }else if(hashA > hashB){
        return 1;
      }else{
        CHECKER("unexpected case\n");
        exit(EXIT_FAILURE);
      }
    }
  }
}

void freeInheritedVertex(InheritedVertex *iVertex){
  freeHashString(iVertex->hashString);
  freeIntStack(iVertex->conventionalPropagationMemo);
  freeDisjointSetForest(iVertex->equivalenceClassOfIsomorphism);
  free(iVertex);

  return;
}

TrieBody *makeTrieBody(){
  TrieBody *ret = (TrieBody *)malloc(sizeof(TrieBody));
  ret->key.type = key_null;
  ret->inheritedVertices = makeList();
  ret->parent = NULL;
  ret->children = makeRedBlackTree();
  ret->depth = -1;
  ret->isInfinitedDepth = FALSE;
  ret->isPushedIntoGoAheadStack = FALSE;

  return ret;
}

TerminationConditionInfo *makeTerminationConditionInfo(){
  TerminationConditionInfo *ret = (TerminationConditionInfo *)malloc(sizeof(TerminationConditionInfo));
  ret->distribution = makeOmegaArray();
  ret->increase = makeOmegaArray();

  return ret;
}

Trie *makeTrie(){
  Trie *ret = (Trie *)malloc(sizeof(Trie));
  ret->body = makeTrieBody();
  ret->body->depth = -1;
  ret->info = makeTerminationConditionInfo();

  return ret;
}

void freeTrieInnerCaster(void *body);

void freeTrieInner(TrieBody *body){
  freeList(body->inheritedVertices);
  freeRedBlackTreeWithValue(body->children,freeTrieInnerCaster);
  free(body);

  return;
}

void freeTrieInnerCaster(void *body){
  freeTrieInner((TrieBody *)body);

  return;
}

void freeTrieBody(TrieBody *body){
  freeTrieInner(body);

  return;
}

void freeTerminationConditionInfo(TerminationConditionInfo *info){
  freeOmegaArray(info->distribution);
  freeOmegaArray(info->increase);
  free(info);

  return;
}

void freeTrie(Trie *trie){
  freeTrieInner(trie->body);
  freeTerminationConditionInfo(trie->info);
  free(trie);

  return;
}

void deleteTrieBody(TrieBody *body){
  deleteRedBlackTree(body->parent->children,body->key);
  freeTrieBody(body);

  return;
}

void deleteTrieDescendantsAndItself(TrieBody *body);

void deleteTrieDescendantsAndItselfCaster(void *body){
  deleteTrieDescendantsAndItself((TrieBody *)body);

  return;
}

void deleteTrieDescendantsAndItself(TrieBody *body){
  if(body != NULL){
    freeRedBlackTreeWithValue(body->children,deleteTrieDescendantsAndItselfCaster);
    free(body);
  }

  return;
}

void deleteTrieDescendants(TrieBody *body){
  freeRedBlackTreeWithValueInner(body->children->body,deleteTrieDescendantsAndItselfCaster);
  body->children->body = NULL;

  return;
}

void pushTrieBodyIntoGoAheadStackWithoutOverlap(Stack *stack,TrieBody *body){
  if(body != NULL){
    if(!body->isPushedIntoGoAheadStack){
      pushStack(stack,body);
      // printf("pushTrieBodyIntoGoAheadStackWithoutOverlap!! KEY:%08X\n",body->key.u);
      body->isPushedIntoGoAheadStack = TRUE;
    }
  }
  return;
}

TrieBody *popTrieBodyFromGoAheadStackWithoutOverlap(Stack *stack){
  TrieBody *ret = popStack(stack);

  ret->isPushedIntoGoAheadStack = FALSE;

  return ret;
}

void goBackProcessInnerManyCommonPrefixVertices(ListBody *targetCell,TrieBody *currentNode,Stack *goAheadStack,TerminationConditionInfo *tInfo,int targetDepth){
  if(targetDepth == currentNode->depth){
    // printf("targetDepth == currentNode->depth!! KEY:%08X\n",currentNode->key.u);
    pushCell(currentNode->inheritedVertices,targetCell);
    ((InheritedVertex *)targetCell->value)->ownerNode = currentNode;
    ((InheritedVertex *)targetCell->value)->hashString->creditIndex = currentNode->depth;
    pushTrieBodyIntoGoAheadStackWithoutOverlap(goAheadStack,currentNode);
  }else{
    TrieBody *parent = currentNode->parent;

    goBackProcessInnerManyCommonPrefixVertices(targetCell,parent,goAheadStack,tInfo,targetDepth);
  }
}

void goBackProcessInnerDoubleCommonPrefixVertices(ListBody *targetCell,ListBody *brotherCell,TrieBody *currentNode,TrieBody *prevNode,Stack *goAheadStack,TerminationConditionInfo *tInfo,int targetDepth){
  if(targetDepth == currentNode->depth){
    pushCell(currentNode->inheritedVertices,targetCell);
    ((InheritedVertex *)targetCell->value)->ownerNode = currentNode;
    ((InheritedVertex *)targetCell->value)->hashString->creditIndex = currentNode->depth;
    pushCell(prevNode->inheritedVertices,brotherCell);
    ((InheritedVertex *)brotherCell->value)->ownerNode = prevNode;
    ((InheritedVertex *)brotherCell->value)->hashString->creditIndex = prevNode->depth;
    incrementOmegaArray(tInfo->distribution,prevNode->depth);
    //[$] ラベル更新
    printf("Vertex move in goBack !! ID : %d ",((InheritedVertex *)brotherCell->value)->beforeID);
    fprintf(stdout,"LABEL:(%08X,%d) -> ",((InheritedVertex *)brotherCell->value)->canonicalLabel.first,((InheritedVertex *)brotherCell->value)->canonicalLabel.second);
    ((InheritedVertex *)brotherCell->value)->canonicalLabel.first = prevNode->key.u.ui32;
    fprintf(stdout,"LABEL:(%08X,%d)\n",((InheritedVertex *)brotherCell->value)->canonicalLabel.first,((InheritedVertex *)brotherCell->value)->canonicalLabel.second);

    pushTrieBodyIntoGoAheadStackWithoutOverlap(goAheadStack,currentNode);
  }else if(isSingletonRedBlackTree(currentNode->children)){
    TrieBody *parent = currentNode->parent;

    deleteTrieBody(prevNode);
    goBackProcessInnerDoubleCommonPrefixVertices(targetCell,brotherCell,parent,currentNode,goAheadStack,tInfo,targetDepth);
  }else{
    TrieBody *parent = currentNode->parent;

    pushCell(prevNode->inheritedVertices,brotherCell);
    ((InheritedVertex *)brotherCell->value)->ownerNode = prevNode;
    ((InheritedVertex *)brotherCell->value)->hashString->creditIndex = prevNode->depth;
    incrementOmegaArray(tInfo->distribution,prevNode->depth);
    //[$] ラベル更新
    // printf("Vertex move in goBack !! ID : %d ",((InheritedVertex *)brotherCell->value)->beforeID);
    // fprintf(stdout,"LABEL:(%08X,%d) -> ",((InheritedVertex *)brotherCell->value)->canonicalLabel.first,((InheritedVertex *)brotherCell->value)->canonicalLabel.second);
    ((InheritedVertex *)brotherCell->value)->canonicalLabel.first = prevNode->key.u.ui32;
    // fprintf(stdout,"LABEL:(%08X,%d)\n",((InheritedVertex *)brotherCell->value)->canonicalLabel.first,((InheritedVertex *)brotherCell->value)->canonicalLabel.second);

    goBackProcessInnerManyCommonPrefixVertices(targetCell,parent,goAheadStack,tInfo,targetDepth);
  }
}

void goBackProcessInnerSingleCommonPrefixVertex(ListBody *targetCell,TrieBody *currentNode,Stack *goAheadStack,TerminationConditionInfo *tInfo,int targetDepth){
  if(targetDepth == currentNode->depth){
    // printf("targetDepth == currentNode->depth !! ID : %d \n",((InheritedVertex *)targetCell->value)->beforeID);
    pushCell(currentNode->inheritedVertices,targetCell);
    ((InheritedVertex *)targetCell->value)->ownerNode = currentNode;
    ((InheritedVertex *)targetCell->value)->hashString->creditIndex = currentNode->depth;
    pushTrieBodyIntoGoAheadStackWithoutOverlap(goAheadStack,currentNode);
  }else if(isSingletonRedBlackTree(currentNode->children) && isSingletonList(((TrieBody *)(currentNode->children->body->value))->inheritedVertices)){
    // printf("targetDepth != currentNode->depth but isSingletonRedBlackTree and isSingletonList !! ID : %d \n",((InheritedVertex *)targetCell->value)->beforeID);
    TrieBody *childNode = (TrieBody *)currentNode->children->body->value;
    ListBody *brother = popCell(childNode->inheritedVertices);

    decrementOmegaArray(tInfo->distribution,childNode->depth);

    goBackProcessInnerDoubleCommonPrefixVertices(targetCell,brother,currentNode,childNode,goAheadStack,tInfo,targetDepth);
  }else{
    // printf("targetDepth != currentNode->depth and !isSingletonRedBlackTree or !isSingletonList !! ID : %d \n",((InheritedVertex *)targetCell->value)->beforeID);
    TrieBody *parent = currentNode->parent;

    goBackProcessInnerManyCommonPrefixVertices(targetCell,parent,goAheadStack,tInfo,targetDepth);
  }

  return;
}

//trie is minimal for uniqueness!!
void goBackProcess(ListBody *targetCell,TrieBody *currentNode,Stack *goAheadStack,TerminationConditionInfo *tInfo,int targetDepth){
  if(targetDepth < currentNode->depth){
    if(isEmptyList(currentNode->inheritedVertices)){
      // printf("isEmptyList is true !! ID : %d \n",((InheritedVertex *)targetCell->value)->beforeID);
      TrieBody *parent = currentNode->parent;

      decrementOmegaArray(tInfo->distribution,currentNode->depth);
      if(parent->depth >= 0 && !isSingletonRedBlackTree(parent->children)){
        decrementOmegaArray(tInfo->increase,parent->depth);
      }

      deleteTrieBody(currentNode);

      goBackProcessInnerSingleCommonPrefixVertex(targetCell,parent,goAheadStack,tInfo,targetDepth);
    }else if(isSingletonList(currentNode->inheritedVertices)){
      printf("isEmptyList is false but isSingletonList is true !! ID : %d \n",((InheritedVertex *)targetCell->value)->beforeID);
      ListBody *brother = popCell(currentNode->inheritedVertices);
      TrieBody *parent = currentNode->parent;

      decrementOmegaArray(tInfo->distribution,OMEGA);
      decrementOmegaArray(tInfo->distribution,OMEGA);

      goBackProcessInnerDoubleCommonPrefixVertices(targetCell,brother,parent,currentNode,goAheadStack,tInfo,targetDepth);
    }else{
      printf("isEmptyList is false and isSingletonList is false !! ID : %d \n",((InheritedVertex *)targetCell->value)->beforeID);
      TrieBody *parent = currentNode->parent;

      decrementOmegaArray(tInfo->distribution,OMEGA);

      // 57,64,66,68,73は全部ここでtargetCellになっている！
      goBackProcessInnerManyCommonPrefixVertices(targetCell,parent,goAheadStack,tInfo,targetDepth);
    }
  }else{
    //[$] ラベル更新っぽそうな怪しさmaxなので出力テスト
    printf("targetDepth == currentNode->depth !! ID : %d ",((InheritedVertex *)targetCell->value)->beforeID);
    fprintf(stdout,"LABEL:(%08X,%d) \n",((InheritedVertex *)targetCell->value)->canonicalLabel.first,((InheritedVertex *)targetCell->value)->canonicalLabel.second);

    pushCell(currentNode->inheritedVertices,targetCell);
  }
}

void goBackProcessOfCurrentConvertedVertices(Stack *BFSStack,Stack *goAheadStack,TerminationConditionInfo *tInfo,int targetDepth){
  int i;

  for(i=0;i<numStack(BFSStack);i++){
    ConvertedGraphVertex *cVertex = readStack(BFSStack,i);
    InheritedVertex *iVertex = cVertex->correspondingVertexInTrie;
    TrieBody *currentNode = iVertex->ownerNode;
    ListBody *targetCell = iVertex->ownerCell;
    cutCell(targetCell);

    //[$] ラベル更新…っぽそうな怪しさmaxなので出力テスト
    // 57,64,66,68,73は全部ここでtargetCellになっている！
    //ここも必要
    printf("Vertex dump in goBackProcessOfCurrentConvertedVertices !! ID : %d ",((InheritedVertex *)targetCell->value)->beforeID);
    fprintf(stdout,"LABEL:(%08X,%d) \n",((InheritedVertex *)targetCell->value)->canonicalLabel.first,((InheritedVertex *)targetCell->value)->canonicalLabel.second);
    goBackProcess(targetCell,currentNode,goAheadStack,tInfo,targetDepth);
  }

  return;
}

void goAheadProcess(TrieBody *targetNode,Stack *goAheadStack,Stack *fixCreditIndexStack,TerminationConditionInfo *tInfo,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID){
  List *inheritedVerticesList = targetNode->inheritedVertices;
  RedBlackTree *children = targetNode->children;

  if(isSingletonList(inheritedVerticesList) && isEmptyRedBlackTree(children) && targetNode->depth != -1){
    incrementOmegaArray(tInfo->distribution,targetNode->depth);
    //[$] ラベル更新
    printf("Vertex move in goAhead !! ID : %d ",((InheritedVertex *)peekCell(inheritedVerticesList)->value)->beforeID);
    fprintf(stdout,"LABEL:(%08X,%d) -> ",((InheritedVertex *)peekCell(inheritedVerticesList)->value)->canonicalLabel.first,((InheritedVertex *)peekCell(inheritedVerticesList)->value)->canonicalLabel.second);
    ((InheritedVertex *)peekCell(inheritedVerticesList)->value)->canonicalLabel.first = targetNode->key.u.ui32;
    ((InheritedVertex *)peekCell(inheritedVerticesList)->value)->canonicalLabel.second = 0;
    fprintf(stdout,"LABEL:(%08X,%d)\n",((InheritedVertex *)peekCell(inheritedVerticesList)->value)->canonicalLabel.first,((InheritedVertex *)peekCell(inheritedVerticesList)->value)->canonicalLabel.second);
  }else{
    while(!isEmptyList(inheritedVerticesList)){
      ListBody *tmpCell = popCell(inheritedVerticesList);
      // 57,64,66,68,73は全部ここでtargetCellになっている？
      /**/printf("Vertex move in goAhead ?! ID : %d ",((InheritedVertex *)tmpCell->value)->beforeID);
      /**/fprintf(stdout,"LABEL:(%08X,%d) -> ",((InheritedVertex *)tmpCell->value)->canonicalLabel.first,((InheritedVertex *)tmpCell->value)->canonicalLabel.second);
      KeyContainer key = makeUInt32Key(callHashValue(((InheritedVertex *)tmpCell->value),targetNode->depth,cAfterGraph,gapOfGlobalRootMemID,fixCreditIndexStack));
      /**/fprintf(stdout,"LABEL:(");
      /**/keyDump(key);
      /**/fprintf(stdout,",?)\n");

      TrieBody *nextNode = searchRedBlackTree(children,key);
      if(nextNode == NULL){
        if(!isEmptyRedBlackTree(children)){
          incrementOmegaArray(tInfo->increase,targetNode->depth);
        }

        nextNode = makeTrieBody();
        insertRedBlackTree(children,key,nextNode);
        nextNode->key = key;
        nextNode->parent = targetNode;
        nextNode->depth = targetNode->depth + 1;
      }

      if(!nextNode->isPushedIntoGoAheadStack && !isEmptyList(nextNode->inheritedVertices)){
        if(isSingletonList(nextNode->inheritedVertices)){
          decrementOmegaArray(tInfo->distribution,nextNode->depth);
        }else{
          ListBody *iterator;
          ListBody *sentinel = nextNode->inheritedVertices->sentinel;
          for(iterator=sentinel->next;iterator!=sentinel;iterator=iterator->next){
            decrementOmegaArray(tInfo->distribution,OMEGA);
          }
        }
      }

      pushCell(nextNode->inheritedVertices,tmpCell);
      ((InheritedVertex *)tmpCell->value)->ownerNode = nextNode;
      pushTrieBodyIntoGoAheadStackWithoutOverlap(goAheadStack,nextNode);
    }
  }
}

void goAheadProcessOfCurrentTrieNodes(Stack *goAheadStack,Stack *fixCreditIndexStack,TerminationConditionInfo *tInfo,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID){
  Stack *nextGoAheadStack = makeStack();

  while(!isEmptyStack(goAheadStack)){
    TrieBody *targetNode = popTrieBodyFromGoAheadStackWithoutOverlap(goAheadStack);

    goAheadProcess(targetNode,nextGoAheadStack,fixCreditIndexStack,tInfo,cAfterGraph,gapOfGlobalRootMemID);
  }

  swapStack(nextGoAheadStack,goAheadStack);
  freeStack(nextGoAheadStack);

  return;
}

//[%] stackを追加すべき処理
void deleteInheritedVerticesFromTrie(Trie *trie,Stack *deletedVertices,Stack *goAheadStack){
  while(!isEmptyStack(deletedVertices)){
    ConvertedGraphVertex *targetCVertex = popConvertedVertexFromDiffInfoStackWithoutOverlap(deletedVertices);
    printf(" - Vertex delete !! - \n");
    // convertedGraphVertexDump(targetCVertex);
    InheritedVertex *targetIVertex = targetCVertex->correspondingVertexInTrie;
    inheritedVertexDump(targetIVertex);
    printf("\n\n");    

    ListBody *targetCell = targetIVertex->ownerCell;
    cutCell(targetCell);
    TrieBody *currentNode = targetIVertex->ownerNode;

    goBackProcess(targetCell,currentNode,goAheadStack,trie->info,-1);

    cutCell(targetCell);
    free(targetCell);
    freeInheritedVertex(targetIVertex);
  }
}

InheritedVertex *wrapAfterConvertedVertexInInheritedVertex(ConvertedGraphVertex *cVertex,int gapOfGlobalRootMemID){
  InheritedVertex *iVertex = (InheritedVertex *)malloc(sizeof(InheritedVertex));
  iVertex->type = cVertex->type;
  strcpy(iVertex->name,cVertex->name);
  iVertex->canonicalLabel.first = 0;
  iVertex->canonicalLabel.second = 0;
  iVertex->hashString = makeHashString();
  iVertex->isPushedIntoFixCreditIndex = FALSE;
  iVertex->beforeID = cVertex->ID - gapOfGlobalRootMemID;
  /*$*/
  // printf(" --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
  // printf("%d -> %d (%d)\n", cVertex->ID, iVertex->beforeID, gapOfGlobalRootMemID);
  /*$*/
  cVertex->correspondingVertexInTrie = iVertex;
  iVertex->ownerNode = NULL;
  iVertex->ownerCell = NULL;
  iVertex->conventionalPropagationMemo = makeIntStack();
  iVertex->equivalenceClassOfIsomorphism = makeDisjointSetForest();

  return iVertex;
}

void addInheritedVerticesToTrie(Trie *trie,Stack *addedVertices,Stack *initializeConvertedVerticesStack,Stack *goAheadStack,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID){
  if(!isEmptyStack(addedVertices)){
    pushTrieBodyIntoGoAheadStackWithoutOverlap(goAheadStack,trie->body);
  }

  while(!isEmptyStack(addedVertices)){
    ConvertedGraphVertex *targetCVertex = popConvertedVertexFromDiffInfoStackWithoutOverlap(addedVertices);
    InheritedVertex *targetIVertex = wrapAfterConvertedVertexInInheritedVertex(targetCVertex,gapOfGlobalRootMemID);

    pushList(trie->body->inheritedVertices,targetIVertex);
    targetIVertex->ownerCell = trie->body->inheritedVertices->sentinel->next;
    targetCVertex->isVisitedInBFS = TRUE;
    pushStack(initializeConvertedVerticesStack,targetCVertex);
  }

  return;
}

void moveInheritedRelinkedVerticesToBFSStack(Stack *relinkedVertices,Stack *initializeConvertedVerticesStack,Stack *BFSStack){
  while(!isEmptyStack(relinkedVertices)){
    ConvertedGraphVertex *cVertex = popConvertedVertexFromDiffInfoStackWithoutOverlap(relinkedVertices);
    pushStack(BFSStack,cVertex);
    cVertex->isVisitedInBFS = TRUE;
  }

  return;
}

void initializeConvertedVertices(Stack *initializeConvertedVerticesStack){
  while(!isEmptyStack(initializeConvertedVerticesStack)){
    ConvertedGraphVertex *cVertex = popStack(initializeConvertedVerticesStack);
    cVertex->isVisitedInBFS = FALSE;
  }

  return;
}

Bool isEmptyTrie(Trie *trie){
  return isEmptyRedBlackTree(trie->body->children);
}

Bool isDescreteTrie(Stack *goAheadStack,TerminationConditionInfo *tInfo,int depth){
  return maxIndex(tInfo->distribution) == depth && isEmptyStack(goAheadStack);
}

Bool isRefinedTrie(TerminationConditionInfo *tInfo,int step){
  return readOmegaArray(tInfo->increase,step) != 0;
}

Bool triePropagationIsContinued(Stack *goAheadStack,TerminationConditionInfo *tInfo,int step){
  return isRefinedTrie(tInfo,step) && !isDescreteTrie(goAheadStack,tInfo,step);
}

void pushInftyDepthTrieNodesIntoGoAheadStackInner(TrieBody *body,Stack *goAheadStack,TerminationConditionInfo *tInfo,int depth);

void pushInftyDepthTrieNodesIntoGoAheadStackInnerInner(RedBlackTreeBody *trieChildrenBody,Stack *goAheadStack,TerminationConditionInfo *tInfo,int depth){
  if(trieChildrenBody != NULL){
    pushInftyDepthTrieNodesIntoGoAheadStackInnerInner(trieChildrenBody->children[LEFT],goAheadStack,tInfo,depth);
    pushInftyDepthTrieNodesIntoGoAheadStackInner(trieChildrenBody->value,goAheadStack,tInfo,depth);
    pushInftyDepthTrieNodesIntoGoAheadStackInnerInner(trieChildrenBody->children[RIGHT],goAheadStack,tInfo,depth);
  }

  return;
}

void collectDescendantConvertedVerticesInner(TrieBody *ancestorBody,RedBlackTreeBody *rbtb);

void pushInftyDepthTrieNodesIntoGoAheadStackInner(TrieBody *body,Stack *goAheadStack,TerminationConditionInfo *tInfo,int targetDepth){
  if(body->depth == targetDepth){
    if(!body->isPushedIntoGoAheadStack){
      collectDescendantConvertedVerticesInner(body,body->children->body);
      deleteTrieDescendants(body);
      body->isInfinitedDepth = FALSE;

      if(!isSingletonList(body->inheritedVertices)){
        pushTrieBodyIntoGoAheadStackWithoutOverlap(goAheadStack,body);

        ListBody *sentinel = body->inheritedVertices->sentinel;
        ListBody *iterator;
        for(iterator=sentinel->next;iterator!=sentinel;iterator=iterator->next){
          decrementOmegaArray(tInfo->distribution,OMEGA);
        }
      }
    }
  }else{
    pushInftyDepthTrieNodesIntoGoAheadStackInnerInner(body->children->body,goAheadStack,tInfo,targetDepth);
  }

  return;
}

void pushInftyDepthTrieNodesIntoGoAheadStack(Trie *trie,Stack *goAheadStack,int targetDepth){
  pushInftyDepthTrieNodesIntoGoAheadStackInner(trie->body,goAheadStack,trie->info,targetDepth);

  return;
}

void triePropagateInner(Trie *trie,Stack *BFSStack,Stack *initializeConvertedVerticesStack,Stack *goAheadStack,Stack *fixCreditIndexStack,TerminationConditionInfo *tInfo,int stepOfPropagation,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID){
  if(maxIndex(tInfo->distribution) == OMEGA && maxIndex(tInfo->increase) == stepOfPropagation - 1){
    pushInftyDepthTrieNodesIntoGoAheadStack(trie,goAheadStack,stepOfPropagation);
  }

  goBackProcessOfCurrentConvertedVertices(BFSStack,goAheadStack,tInfo,stepOfPropagation);
  goAheadProcessOfCurrentTrieNodes(goAheadStack,fixCreditIndexStack,tInfo,cAfterGraph,gapOfGlobalRootMemID);

  getNextDistanceConvertedVertices(BFSStack,initializeConvertedVerticesStack,cAfterGraph);

  return;
}

void collectDescendantConvertedVertices(TrieBody *ancestorBody,TrieBody *descendantBody);

void collectDescendantConvertedVerticesInner(TrieBody *ancestorBody,RedBlackTreeBody *rbtb){
  if(rbtb != NULL){
    collectDescendantConvertedVerticesInner(ancestorBody,rbtb->children[LEFT]);
    collectDescendantConvertedVertices(ancestorBody,rbtb->value);
    collectDescendantConvertedVerticesInner(ancestorBody,rbtb->children[RIGHT]);
  }

  return;
}

void collectDescendantConvertedVertices(TrieBody *ancestorBody,TrieBody *descendantBody){
  if(isEmptyRedBlackTree(descendantBody->children)){
    while(!isEmptyList(descendantBody->inheritedVertices)){
      ListBody *targetCell = popCell(descendantBody->inheritedVertices);
      pushCell(ancestorBody->inheritedVertices,targetCell);
      ((InheritedVertex *)targetCell->value)->ownerNode = ancestorBody;
      ((InheritedVertex *)targetCell->value)->hashString->creditIndex = ancestorBody->depth;

      //[$] ラベル更新?
      printf("Vertex move in collectDescendant !! ID : %d ",((InheritedVertex *)targetCell->value)->beforeID);
      fprintf(stdout,"LABEL:(%08X,%d) -> ",((InheritedVertex *)targetCell->value)->canonicalLabel.first,((InheritedVertex *)targetCell->value)->canonicalLabel.second);
      ((InheritedVertex *)targetCell->value)->canonicalLabel.first = ancestorBody->key.u.ui32;
      fprintf(stdout,"LABEL:(%08X,%d)\n",((InheritedVertex *)targetCell->value)->canonicalLabel.first,((InheritedVertex *)targetCell->value)->canonicalLabel.second);
    }
  }else{
    collectDescendantConvertedVerticesInner(ancestorBody,descendantBody->children->body);
  }

  return;
}

void makeTrieMinimumInner(TrieBody *body,TerminationConditionInfo *tInfo,int stepOfPropagation);

void makeTrieMinimumInnerInner(RedBlackTreeBody *rbtb,TerminationConditionInfo *tInfo,int stepOfPropagation){
  if(rbtb != NULL){
    makeTrieMinimumInnerInner(rbtb->children[LEFT],tInfo,stepOfPropagation);
    makeTrieMinimumInner(rbtb->value,tInfo,stepOfPropagation);
    makeTrieMinimumInnerInner(rbtb->children[RIGHT],tInfo,stepOfPropagation);
  }

  return;
}

void makeTrieMinimumInner(TrieBody *body,TerminationConditionInfo *tInfo,int stepOfPropagation){
  printf("body->depth : %d , stepOfPropagation : %d\n",body->depth,stepOfPropagation);
  if(body->depth == stepOfPropagation + 1){
    if(body->isPushedIntoGoAheadStack){
      ListBody *iterator;
      ListBody *sentinel = body->inheritedVertices->sentinel;

      for(iterator=sentinel->next;iterator!=sentinel;iterator=iterator->next){
        incrementOmegaArray(tInfo->distribution,OMEGA);
        //[$] ラベル更新
        printf("Vertex move in makeTrieMinimumInner !! ID : %d ",((InheritedVertex *)iterator->value)->beforeID);
        fprintf(stdout,"LABEL:(%08X,%d) -> ",((InheritedVertex *)iterator->value)->canonicalLabel.first,((InheritedVertex *)iterator->value)->canonicalLabel.second);
        ((InheritedVertex *)iterator->value)->canonicalLabel.first = body->key.u.ui32;
        fprintf(stdout,"LABEL:(%08X,%d)\n",((InheritedVertex *)iterator->value)->canonicalLabel.first,((InheritedVertex *)iterator->value)->canonicalLabel.second);

      }
    }

    if(!isEmptyRedBlackTree(body->children)){
      collectDescendantConvertedVerticesInner(body,body->children->body);
      deleteTrieDescendants(body);
    }

    body->isInfinitedDepth = TRUE;
  }else{
    makeTrieMinimumInnerInner(body->children->body,tInfo,stepOfPropagation);
  }

  return;
}

void makeTrieMinimum(Trie *trie,int stepOfPropagation){
  TerminationConditionInfo *tInfo = trie->info;

  makeTrieMinimumInner(trie->body,tInfo,stepOfPropagation);

  while(tInfo->distribution->maxFiniteIndex > stepOfPropagation){
    decrementOmegaArray(tInfo->distribution,tInfo->distribution->maxFiniteIndex);
    incrementOmegaArray(tInfo->distribution,OMEGA);
  }

  while(tInfo->increase->maxFiniteIndex > stepOfPropagation){
    decrementOmegaArray(tInfo->increase,tInfo->increase->maxFiniteIndex);
  }

  return;
}

void makeConventionalPropagationListInner(TrieBody *body,List *list,int stepOfPropagation);

void makeConventionalPropagationListInnerInner(RedBlackTreeBody *body,List *list,int stepOfPropagation){
  if(body != NULL){
    makeConventionalPropagationListInnerInner(body->children[LEFT],list,stepOfPropagation);
    makeConventionalPropagationListInner(body->value,list,stepOfPropagation);
    makeConventionalPropagationListInnerInner(body->children[RIGHT],list,stepOfPropagation);
  }

  return;
}

void makeConventionalPropagationListInner(TrieBody *body,List *list,int stepOfPropagation){
  if(isEmptyRedBlackTree(body->children)){
    if(!isEmptyList(list)){
      pushList(list,CLASS_SENTINEL);
    }

    ListBody *sentinel = body->inheritedVertices->sentinel;
    ListBody *iterator;

    for(iterator=sentinel->next;iterator!=sentinel;iterator=iterator->next){
      // inheritedVertexDump(iterator->value);
      // printf("\n");
      pushList(list,iterator->value);
    }
  }else{
    makeConventionalPropagationListInnerInner(body->children->body,list,stepOfPropagation);
  }

  return;
}

List *makeConventionalPropagationList(Trie *trie,int stepOfPropagation){
  List *ret = makeList();

  makeConventionalPropagationListInner(trie->body,ret,stepOfPropagation);

  return ret;
}

ListBody *getNextSentinel(ListBody *beginSentinel){
  ListBody *endSentinel;

  for(endSentinel=beginSentinel->next;endSentinel->value!=CLASS_SENTINEL;endSentinel=endSentinel->next){
  }

  return endSentinel;
}

Bool putClassesWithPriority(ListBody *beginSentinel,ListBody *endSentinel,PriorityQueue *cellPQueue){
  Bool isRefined = FALSE;
  ValueWithPriority prevWrapper = peekPriorityQueue(cellPQueue);
  ValueWithPriority tmpWrapper;

  while(!isEmptyPriorityQueue(cellPQueue)){
    tmpWrapper = popPriorityQueue(cellPQueue);

    if(tmpWrapper.priority < prevWrapper.priority){
      ListBody *classSentinel = makeCell(CLASS_SENTINEL);
      insertNextCell(beginSentinel,classSentinel);

      isRefined = TRUE;
    }

    insertNextCell(beginSentinel,tmpWrapper.value);

    prevWrapper = tmpWrapper;
  }

  return isRefined;
}

Bool classifyConventionalPropagationList(List *pList,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID,Bool classifyConventionalPropagationListInner(ListBody *,ListBody *,ConvertedGraph *,int,PriorityQueue *)){
  if(isEmptyList(pList)){
    return FALSE;
  }else{
    Bool isRefined = FALSE;
    PriorityQueue *cellPQueue = makePriorityQueue();

    ListBody *beginSentinel;
    ListBody *endSentinel;

    endSentinel = pList->sentinel;
    beginSentinel = endSentinel;

    do{
      endSentinel = getNextSentinel(beginSentinel);

      if(classifyConventionalPropagationListInner(beginSentinel,endSentinel,cAfterGraph,gapOfGlobalRootMemID,cellPQueue)){
        isRefined = TRUE;
      }
      beginSentinel = endSentinel;
    }while(endSentinel != pList->sentinel);

    freePriorityQueue(cellPQueue);

    return isRefined;
  }
}

Bool classifyConventionalPropagationListWithTypeInner(ListBody *beginSentinel,ListBody *endSentinel,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID,PriorityQueue *cellPQueue){
  while(beginSentinel->next != endSentinel){
    ListBody *tmpCell = beginSentinel->next;
    cutCell(tmpCell);

    int tmpPriority = ((InheritedVertex *)(tmpCell->value))->type;
    pushPriorityQueue(cellPQueue,tmpCell,tmpPriority);
  }

  Bool isRefined = putClassesWithPriority(beginSentinel,endSentinel,cellPQueue);

  return isRefined;
}

Bool classifyConventionalPropagationListWithDegreeInner(ListBody *beginSentinel,ListBody *endSentinel,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID,PriorityQueue *cellPQueue){
  while(beginSentinel->next != endSentinel){
    ListBody *tmpCell = beginSentinel->next;
    cutCell(tmpCell);

    int tmpPriority = numStack(correspondingVertexInConvertedGraph(((InheritedVertex *)(tmpCell->value)),cAfterGraph,gapOfGlobalRootMemID)->links);
    pushPriorityQueue(cellPQueue,tmpCell,tmpPriority);
  }

  Bool isRefined = putClassesWithPriority(beginSentinel,endSentinel,cellPQueue);

  return isRefined;
}

Bool classifyConventionalPropagationListWithNameLengthInner(ListBody *beginSentinel,ListBody *endSentinel,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID,PriorityQueue *cellPQueue){
  while(beginSentinel->next != endSentinel){
    ListBody *tmpCell = beginSentinel->next;
    cutCell(tmpCell);

    int tmpPriority = strlen(correspondingVertexInConvertedGraph(((InheritedVertex *)(tmpCell->value)),cAfterGraph,gapOfGlobalRootMemID)->name);
    pushPriorityQueue(cellPQueue,tmpCell,tmpPriority);
  }

  Bool isRefined = putClassesWithPriority(beginSentinel,endSentinel,cellPQueue);

  return isRefined;
}

Bool classifyConventionalPropagationListWithNameCharactersInnerInner(ListBody *beginSentinel,ListBody *endSentinel,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID,int index,PriorityQueue *cellPQueue){
  while(beginSentinel->next != endSentinel){
    ListBody *tmpCell = beginSentinel->next;
    cutCell(tmpCell);

    int tmpPriority = (correspondingVertexInConvertedGraph(((InheritedVertex *)(tmpCell->value)),cAfterGraph,gapOfGlobalRootMemID)->name)[index];
    pushPriorityQueue(cellPQueue,tmpCell,tmpPriority);
  }

  Bool isRefined = putClassesWithPriority(beginSentinel,endSentinel,cellPQueue);

  return isRefined;
}

Bool classifyConventionalPropagationListWithNameCharactersInner(ListBody *beginSentinel,ListBody *endSentinel,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID,PriorityQueue *cellPQueue){
  Bool isRefined = FALSE;

  ListBody *innerBeginSentinel;
  ListBody *innerEndSentinel;

  int nameLength = strlen(correspondingVertexInConvertedGraph(((InheritedVertex *)(beginSentinel->next->value)),cAfterGraph,gapOfGlobalRootMemID)->name);

  int i;
  for(i=0;i<nameLength;i++){
    innerEndSentinel = beginSentinel;
    innerBeginSentinel = beginSentinel;

    do{
      innerEndSentinel = getNextSentinel(innerBeginSentinel);

      isRefined = classifyConventionalPropagationListWithNameCharactersInnerInner(innerBeginSentinel,innerEndSentinel,cAfterGraph,gapOfGlobalRootMemID,i,cellPQueue) || isRefined;

      innerBeginSentinel = innerEndSentinel;
    }while(innerEndSentinel != endSentinel);
  }

  return isRefined;
}

Bool classifyConventionalPropagationListWithType(List *pList,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID){
  return classifyConventionalPropagationList(pList,cAfterGraph,gapOfGlobalRootMemID,classifyConventionalPropagationListWithTypeInner);
}

Bool classifyConventionalPropagationListWithDegree(List *pList,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID){
  return classifyConventionalPropagationList(pList,cAfterGraph,gapOfGlobalRootMemID,classifyConventionalPropagationListWithDegreeInner);
}

Bool classifyConventionalPropagationListWithName(List *pList,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID){
  Bool isRefined = FALSE;
  isRefined = classifyConventionalPropagationList(pList,cAfterGraph,gapOfGlobalRootMemID,classifyConventionalPropagationListWithNameLengthInner) || isRefined;
  isRefined = classifyConventionalPropagationList(pList,cAfterGraph,gapOfGlobalRootMemID,classifyConventionalPropagationListWithNameCharactersInner) || isRefined;

  return isRefined;
}

Bool classifyConventionalPropagationListWithAttribute(List *pList,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID){
  Bool isRefined = FALSE;
  isRefined = classifyConventionalPropagationListWithType(pList,cAfterGraph,gapOfGlobalRootMemID) || isRefined;
  isRefined = classifyConventionalPropagationListWithDegree(pList,cAfterGraph,gapOfGlobalRootMemID) || isRefined;
  isRefined = classifyConventionalPropagationListWithName(pList,cAfterGraph,gapOfGlobalRootMemID) || isRefined;

  return isRefined;
}

void putLabelsToAdjacentVertices(List *pList,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID){
  if(isEmptyList(pList)){
    return;
  }

  int tmpLabel = 0;

  ListBody *beginSentinel = pList->sentinel;
  ListBody *endSentinel = beginSentinel;

  do{
    endSentinel = getNextSentinel(beginSentinel);

    int tmpDegree = numStack(correspondingVertexInConvertedGraph(((InheritedVertex *)(beginSentinel->next->value)),cAfterGraph,gapOfGlobalRootMemID)->links);
    ConvertedGraphVertexType tmpType = correspondingVertexInConvertedGraph(((InheritedVertex *)(beginSentinel->next->value)),cAfterGraph,gapOfGlobalRootMemID)->type;

    int i;
    for(i=0;i<tmpDegree;i++){
      ListBody *iteratorCell;
      for(iteratorCell=beginSentinel->next;iteratorCell!=endSentinel;iteratorCell=iteratorCell->next){
        LMNtalLink *tmpLink = readStack(correspondingVertexInConvertedGraph(((InheritedVertex *)(iteratorCell->value)),cAfterGraph,gapOfGlobalRootMemID)->links,i);
        ConvertedGraphVertex *adjacentVertex;

        switch(tmpLink->attr){
          case INTEGER_ATTR:
            writeIntStack(((InheritedVertex *)(iteratorCell->value))->conventionalPropagationMemo,i,tmpLink->data.integer*256+INTEGER_ATTR);
            break;
          //case DOUBLE_ATTR:
            //break;
          //case STRING_ATTR:
            //break;
          case HYPER_LINK_ATTR:
            adjacentVertex = getConvertedVertexFromGraphAndIDAndType(cAfterGraph,tmpLink->data.ID,convertedHyperLink);
            pushIntStack(adjacentVertex->correspondingVertexInTrie->conventionalPropagationMemo,tmpLabel*256+i);
            break;
          case GLOBAL_ROOT_MEM_ATTR:
            break;
          default:
            if(tmpLink->attr < 128){
              adjacentVertex = getConvertedVertexFromGraphAndIDAndType(cAfterGraph,tmpLink->data.ID,convertedAtom);
              switch(tmpType){
                case convertedAtom:
                  writeIntStack(adjacentVertex->correspondingVertexInTrie->conventionalPropagationMemo,tmpLink->attr,tmpLabel*256+i);
                  break;
                case convertedHyperLink:
                  writeIntStack(adjacentVertex->correspondingVertexInTrie->conventionalPropagationMemo,tmpLink->attr,tmpLabel*256+HYPER_LINK_ATTR);
                  break;
                default:
                  CHECKER("unexpected vertex type\n");
                  exit(EXIT_FAILURE);
                  break;
              }
            }else{
              CHECKER("unexpected vertex type\n");
              exit(EXIT_FAILURE);
            }
            break;
        }
      }
    }

    tmpLabel++;

    beginSentinel = endSentinel;
  }while(endSentinel != pList->sentinel);

  return;
}

Bool classifyConventionalPropagationListWithAdjacentLabelsInnerInner(ListBody *beginSentinel,ListBody *endSentinel,PriorityQueue *cellPQueue){
  while(beginSentinel->next != endSentinel){
    ListBody *tmpCell = beginSentinel->next;
    cutCell(tmpCell);

    int tmpPriority = popIntStack(((InheritedVertex *)(tmpCell->value))->conventionalPropagationMemo);
    pushPriorityQueue(cellPQueue,tmpCell,tmpPriority);
  }

  Bool isRefined = putClassesWithPriority(beginSentinel,endSentinel,cellPQueue);

  return isRefined;
}

Bool classifyConventionalPropagationListWithAdjacentLabelsInner(ListBody *beginSentinel,ListBody *endSentinel,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID,PriorityQueue *cellPQueue){
  Bool isRefined = FALSE;

  ListBody *innerBeginSentinel;
  ListBody *innerEndSentinel;

  int degree = numStack(correspondingVertexInConvertedGraph(((InheritedVertex *)(beginSentinel->next->value)),cAfterGraph,gapOfGlobalRootMemID)->links);

  int i;
  for(i=0;i<degree;i++){
    innerEndSentinel = beginSentinel;
    innerBeginSentinel = beginSentinel;

    do{
      innerEndSentinel = getNextSentinel(innerBeginSentinel);

      isRefined = classifyConventionalPropagationListWithAdjacentLabelsInnerInner(innerBeginSentinel,innerEndSentinel,cellPQueue) || isRefined;

      innerBeginSentinel = innerEndSentinel;
    }while(innerEndSentinel != endSentinel);
  }

  return isRefined;
}

Bool classifyConventionalPropagationListWithAdjacentLabels(List *pList,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID){
  return classifyConventionalPropagationList(pList,cAfterGraph,gapOfGlobalRootMemID,classifyConventionalPropagationListWithAdjacentLabelsInner);
}

Bool refineConventionalPropagationListByPropagation(List *pList,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID){
  Bool isRefined = FALSE;

  putLabelsToAdjacentVertices(pList,cAfterGraph,gapOfGlobalRootMemID);

  isRefined = classifyConventionalPropagationListWithAdjacentLabels(pList,cAfterGraph,gapOfGlobalRootMemID) || isRefined;

  return isRefined;
}

Bool getStableRefinementOfConventionalPropagationList(List *pList,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID){
  Bool isRefined = FALSE;

  while(refineConventionalPropagationListByPropagation(pList,cAfterGraph,gapOfGlobalRootMemID)){
    isRefined = TRUE;
  }

  return isRefined;
}

InheritedVertex *copyInheritedVertex(InheritedVertex *iVertex){
  if(iVertex == CLASS_SENTINEL){
    return CLASS_SENTINEL;
  }else{
    InheritedVertex *ret = (InheritedVertex *)malloc(sizeof(InheritedVertex));

    ret->type = iVertex->type;
    strcpy(ret->name,iVertex->name);
    ret->canonicalLabel = iVertex->canonicalLabel;
    ret->hashString = iVertex->hashString;
    ret->isPushedIntoFixCreditIndex = iVertex->isPushedIntoFixCreditIndex;
    ret->beforeID = iVertex->beforeID;
    ret->ownerNode = iVertex->ownerNode;
    ret->ownerCell = iVertex->ownerCell;
    ret->conventionalPropagationMemo = makeIntStack();
    int i;
    for(i=0;i<numIntStack(iVertex->conventionalPropagationMemo);i++){
      int tmp = readIntStack(iVertex->conventionalPropagationMemo,i);
      writeIntStack(ret->conventionalPropagationMemo,i,tmp);
    }
    ret->equivalenceClassOfIsomorphism = iVertex->equivalenceClassOfIsomorphism;

    return ret;
  }
}

void *copyInheritedVertexCaster(void *iVertex){
  return copyInheritedVertex(iVertex);
}

void assureReferenceFromConvertedVerticesToInheritedVertices(ConvertedGraph *cAfterGraph,ConvertedGraph *cBeforeGraph,int gapOfGlobalRootMemID){
  int i;
  for(i=0;i<cAfterGraph->atoms->cap;i++){
    ConvertedGraphVertex *cAfterVertex = readDynamicArray(cAfterGraph->atoms,i);
    if(cAfterVertex != NULL){
      if(cAfterVertex->correspondingVertexInTrie == NULL){
        ConvertedGraphVertex *cBeforeVertex = getConvertedVertexFromGraphAndIDAndType(cBeforeGraph,cAfterVertex->ID - gapOfGlobalRootMemID,cAfterVertex->type);
        cAfterVertex->correspondingVertexInTrie = cBeforeVertex->correspondingVertexInTrie;
        cAfterVertex->correspondingVertexInTrie->beforeID = cBeforeVertex->ID;
      }
    }
  }

  for(i=0;i<cAfterGraph->hyperLinks->cap;i++){
    ConvertedGraphVertex *cAfterVertex = readDynamicArray(cAfterGraph->hyperLinks,i);
    if(cAfterVertex != NULL){
      if(cAfterVertex->correspondingVertexInTrie == NULL){
        ConvertedGraphVertex *cBeforeVertex = getConvertedVertexFromGraphAndIDAndType(cBeforeGraph,cAfterVertex->ID - gapOfGlobalRootMemID,cAfterVertex->type);
        cAfterVertex->correspondingVertexInTrie = cBeforeVertex->correspondingVertexInTrie;
        cAfterVertex->correspondingVertexInTrie->beforeID = cBeforeVertex->ID;
      }
    }
  }

  return;
}

void initializeReferencesFromConvertedVerticesToInheritedVertices(ConvertedGraph *cBeforeGraph){
  int i;
  for(i=0;i<cBeforeGraph->atoms->cap;i++){
    ConvertedGraphVertex *cBeforeVertex = readDynamicArray(cBeforeGraph->atoms,i);
    if(cBeforeVertex != NULL){
      cBeforeVertex->correspondingVertexInTrie = NULL;
    }
  }

  for(i=0;i<cBeforeGraph->hyperLinks->cap;i++){
    ConvertedGraphVertex *cBeforeVertex = readDynamicArray(cBeforeGraph->hyperLinks,i);
    if(cBeforeVertex != NULL){
      cBeforeVertex->correspondingVertexInTrie = NULL;
    }
  }

  return;
}

Bool triePropagate(Trie *trie,DiffInfo *diffInfo,ConvertedGraph *cAfterGraph,ConvertedGraph *cBeforeGraph,int gapOfGlobalRootMemID,int *stepOfPropagationPtr,Bool measure){
  beforeTime = get_dtime();

  Stack *goAheadStack = makeStack();
  Stack *BFSStack = makeStack();
  Stack *initializeConvertedVerticesStack = makeStack();
  Stack *fixCreditIndexStack = makeStack();
  TerminationConditionInfo *tInfo = trie->info;

  deleteInheritedVerticesFromTrie(trie,diffInfo->deletedVertices,goAheadStack);
  addInheritedVerticesToTrie(trie,diffInfo->addedVertices,initializeConvertedVerticesStack,goAheadStack,cAfterGraph,gapOfGlobalRootMemID);

  afterTime = get_dtime();
  if(measure){
    triePropagateTime += afterTime - beforeTime;
  }

  //実際のSLIMでは起きない操作
  assureReferenceFromConvertedVerticesToInheritedVertices(cAfterGraph,cBeforeGraph,gapOfGlobalRootMemID);

  beforeTime = get_dtime();

  moveInheritedRelinkedVerticesToBFSStack(diffInfo->relinkedVertices,initializeConvertedVerticesStack,BFSStack);

  int stepOfPropagation = -1;
  goAheadProcessOfCurrentTrieNodes(goAheadStack,fixCreditIndexStack,tInfo,cAfterGraph,gapOfGlobalRootMemID);

  stepOfPropagation = 0;
  goAheadProcessOfCurrentTrieNodes(goAheadStack,fixCreditIndexStack,tInfo,cAfterGraph,gapOfGlobalRootMemID);

  while(triePropagationIsContinued(goAheadStack,tInfo,stepOfPropagation)){
    stepOfPropagation++;
    triePropagateInner(trie,BFSStack,initializeConvertedVerticesStack,goAheadStack,fixCreditIndexStack,tInfo,stepOfPropagation,cAfterGraph,gapOfGlobalRootMemID);
  }

  Bool verticesAreCompletelySorted = isDescreteTrie(goAheadStack,tInfo,stepOfPropagation) || isEmptyTrie(trie);
  if(!verticesAreCompletelySorted){
    makeTrieMinimum(trie,stepOfPropagation);

    while(!isEmptyStack(goAheadStack)){
      popTrieBodyFromGoAheadStackWithoutOverlap(goAheadStack);
    }

  }

  initializeConvertedVertices(initializeConvertedVerticesStack);
  initializeConvertedVertices(BFSStack);
  fixCreditIndex(fixCreditIndexStack,cAfterGraph,gapOfGlobalRootMemID);

  afterTime = get_dtime();
  if(measure){
    triePropagateTime += afterTime - beforeTime;
  }

  //実際のSLIMでは起きない操作
  initializeReferencesFromConvertedVerticesToInheritedVertices(cBeforeGraph);

  beforeTime = get_dtime();

  freeStack(goAheadStack);
  freeStack(BFSStack);
  freeStack(initializeConvertedVerticesStack);
  freeStack(fixCreditIndexStack);

  *stepOfPropagationPtr = stepOfPropagation;

  afterTime = get_dtime();
  if(measure){
    triePropagateTime += afterTime - beforeTime;
  }

  return verticesAreCompletelySorted;
}

void spacePrinter(int length){
  int i;
  for(i=0;i<length;i++){
    printf("    ");
  }

  return;
}

void inheritedVertexDump(InheritedVertex *iVertex){
  fprintf(stdout,"<");

  switch(iVertex->type){
    case convertedAtom:
      fprintf(stdout,"SYMBOLATOM,");
      break;
    case convertedHyperLink:
      fprintf(stdout," HYPERLINK,");
      break;
    default:
      fprintf(stderr,"This is unexpected vertex type\n");
      exit(EXIT_FAILURE);
      break;
  }

  fprintf(stdout,"BEFORE_ID=%d,",iVertex->beforeID);
  fprintf(stdout,"LABEL=(%08X,%d),",iVertex->canonicalLabel.first,iVertex->canonicalLabel.second);
  // fprintf(stdout,"CREDIT=%d,",iVertex->hashString->creditIndex);
  // fprintf(stdout,"NAME:\"%s\"",iVertex->name);

  fprintf(stdout,">");

  // fprintf(stdout,"\n$propMemo\n");
  // intStackDump(iVertex->conventionalPropagationMemo);
  return;
}

void inheritedVertexDumpCaster(void *iVertex){
  if(iVertex == CLASS_SENTINEL){
    fprintf(stdout,"CLASS_SENTINEL\n");
  }else{
    inheritedVertexDump((InheritedVertex *)iVertex);
  }

  return;
}

void trieDumpInner(TrieBody *body);

void trieDumpInnerCaster(void *body){
  trieDumpInner((TrieBody *)body);

  return;
}

void trieDumpInner(TrieBody *body){
  if(body->isPushedIntoGoAheadStack){
    fprintf(stdout,"\x1b[33m");
  }

  spacePrinter(body->depth);
  fprintf(stdout,"KEY:");
  keyDump(body->key);
  fprintf(stdout,"\n");

  spacePrinter(body->depth);
  fprintf(stdout,"VERTICES:");
  listDump(body->inheritedVertices,inheritedVertexDumpCaster);
  fprintf(stdout,"\n");

  if(body->isPushedIntoGoAheadStack){
    fprintf(stdout,"\x1b[39m");
  }

  redBlackTreeValueDump(body->children,trieDumpInnerCaster);

  return;
}

void terminationConditionInfoDump(TerminationConditionInfo *tInfo){
  fprintf(stdout,"DISTRIBUTION:"),omegaArrayDump(tInfo->distribution),fprintf(stdout,"\n");
  fprintf(stdout,"INCREASE    :"),omegaArrayDump(tInfo->increase),fprintf(stdout,"\n");

  return;
}

void makeTerminationConditionMemoInner(TrieBody *tBody,OmegaArray *distributionMemo,OmegaArray *increaseMemo);

void makeTerminationConditionMemoInnerInner(RedBlackTreeBody *rBody,OmegaArray *distributionMemo,OmegaArray *increaseMemo){
  if(rBody != NULL){
    makeTerminationConditionMemoInnerInner(rBody->children[LEFT],distributionMemo,increaseMemo);
    makeTerminationConditionMemoInner(rBody->value,distributionMemo,increaseMemo);
    makeTerminationConditionMemoInnerInner(rBody->children[RIGHT],distributionMemo,increaseMemo);
  }

  return;
}

void makeTerminationConditionMemoInner(TrieBody *tBody,OmegaArray *distributionMemo,OmegaArray *increaseMemo){
  if(!tBody->isPushedIntoGoAheadStack){
    if(isSingletonList(tBody->inheritedVertices)){
      incrementOmegaArray(distributionMemo,tBody->depth);
    }else{
      ListBody *iterator;
      ListBody *sentinel = tBody->inheritedVertices->sentinel;
      for(iterator=sentinel->next;iterator!=sentinel;iterator=iterator->next){
        incrementOmegaArray(distributionMemo,OMEGA);
      }
    }
  }

  if(tBody->depth != 0){
    incrementOmegaArray(increaseMemo,tBody->depth - 1);
  }

  makeTerminationConditionMemoInnerInner(tBody->children->body,distributionMemo,increaseMemo);

  if(!isEmptyRedBlackTree(tBody->children)){
    decrementOmegaArray(increaseMemo,tBody->depth);
  }

  return;
}

void makeTerminationConditionMemo(Trie *trie,OmegaArray *distributionMemo,OmegaArray * increaseMemo){
  makeTerminationConditionMemoInnerInner(trie->body->children->body,distributionMemo,increaseMemo);

  return;
}

void trieDump(Trie *trie){
  OmegaArray *distributionMemo = makeOmegaArray();
  OmegaArray *increaseMemo = makeOmegaArray();

  setvbuf( stdout, NULL, _IONBF, BUFSIZ );
  terminationConditionInfoDump(trie->info);
  redBlackTreeValueDump(trie->body->children,trieDumpInnerCaster);

  makeTerminationConditionMemo(trie,distributionMemo,increaseMemo);

  if(!isEqualOmegaArray(distributionMemo,trie->info->distribution) || !isEqualOmegaArray(increaseMemo,trie->info->increase)){
    fprintf(stderr,"WRONG TerminationConditionInfo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    fprintf(stderr,"CORRECT DISTRIBUTION:"),omegaArrayDump(distributionMemo),fprintf(stderr,"\n");
    fprintf(stderr,"CORRECT INCREASE    :"),omegaArrayDump(increaseMemo),fprintf(stderr,"\n");
    exit(EXIT_FAILURE);
  }

  freeOmegaArray(distributionMemo);
  freeOmegaArray(increaseMemo);

  return;
}

void omegaArrayDumpExperiment(OmegaArray *oArray){
  fprintf(stdout,"[");

  int i;
  for(i=0;i<=oArray->maxFiniteIndex;i++){
    fprintf(stdout,"%2d,",readOmegaArray(oArray,i));
  }

  fprintf(stdout," 0, 0, 0,...,%2d]",readOmegaArray(oArray,OMEGA));

  return;
}

void terminationConditionInfoDumpExperiment(TerminationConditionInfo *tInfo){
  fprintf(stdout,"DISTRIBUTION:\n"),omegaArrayDumpExperiment(tInfo->distribution),fprintf(stdout,"\n");
  fprintf(stdout,"INCREASE    :\n"),omegaArrayDumpExperiment(tInfo->increase),fprintf(stdout,"\n");

  return;
}

void terminationConditionInfoDumpExperimentFromTrie(Trie *trie){
  setvbuf( stdout, NULL, _IONBF, BUFSIZ );
  terminationConditionInfoDumpExperiment(trie->info);

  return;
}



/* ---ここまでmiyahara作成--- */

/* ---ここからsakazume作成--- */
#include"adjacent.h"


//[#] Labelsを追加した処理
void goBackProcessInnerDoubleCommonPrefixVerticesWithLabels(ListBody *targetCell,ListBody *brotherCell,TrieBody *currentNode,TrieBody *prevNode,Stack *goAheadStack,TerminationConditionInfo *tInfo,int targetDepth,Labels *labels,int gapOfGlobalRootMemID){
  if(targetDepth == currentNode->depth){
    pushCell(currentNode->inheritedVertices,targetCell);
    ((InheritedVertex *)targetCell->value)->ownerNode = currentNode;
    ((InheritedVertex *)targetCell->value)->hashString->creditIndex = currentNode->depth;
    pushCell(prevNode->inheritedVertices,brotherCell);
    ((InheritedVertex *)brotherCell->value)->ownerNode = prevNode;
    ((InheritedVertex *)brotherCell->value)->hashString->creditIndex = prevNode->depth;
    incrementOmegaArray(tInfo->distribution,prevNode->depth);
    //[$] ラベル更新
    // printf("Vertex move in goBack !! ID : %d ",((InheritedVertex *)brotherCell->value)->beforeID);
    // fprintf(stdout,"LABEL:(%08X,%d) -> ",((InheritedVertex *)brotherCell->value)->canonicalLabel.first,((InheritedVertex *)brotherCell->value)->canonicalLabel.second);
    ((InheritedVertex *)brotherCell->value)->canonicalLabel.first = prevNode->key.u.ui32;
    // fprintf(stdout,"LABEL:(%08X,%d)\n",((InheritedVertex *)brotherCell->value)->canonicalLabel.first,((InheritedVertex *)brotherCell->value)->canonicalLabel.second);
    collectLabelFromIVertexToLabels((InheritedVertex *)brotherCell->value,labels,gapOfGlobalRootMemID);


    pushTrieBodyIntoGoAheadStackWithoutOverlap(goAheadStack,currentNode);
  }else if(isSingletonRedBlackTree(currentNode->children)){
    TrieBody *parent = currentNode->parent;

    deleteTrieBody(prevNode);
    //[#] Labelsを追加した処理
    goBackProcessInnerDoubleCommonPrefixVerticesWithLabels(targetCell,brotherCell,parent,currentNode,goAheadStack,tInfo,targetDepth,labels,gapOfGlobalRootMemID);
  }else{
    TrieBody *parent = currentNode->parent;

    pushCell(prevNode->inheritedVertices,brotherCell);
    ((InheritedVertex *)brotherCell->value)->ownerNode = prevNode;
    ((InheritedVertex *)brotherCell->value)->hashString->creditIndex = prevNode->depth;
    incrementOmegaArray(tInfo->distribution,prevNode->depth);
    //[$] ラベル更新
    //ただしtargetDepthが-1(0も?)のときは削除されるアトムなので記録しない
    if(targetDepth>=0){
      // printf("Vertex move in goBack !! ID : %d ",((InheritedVertex *)brotherCell->value)->beforeID);
      // fprintf(stdout,"LABEL:(%08X,%d) -> ",((InheritedVertex *)brotherCell->value)->canonicalLabel.first,((InheritedVertex *)brotherCell->value)->canonicalLabel.second);
      ((InheritedVertex *)brotherCell->value)->canonicalLabel.first = prevNode->key.u.ui32;
      // fprintf(stdout,"LABEL:(%08X,%d)\n",((InheritedVertex *)brotherCell->value)->canonicalLabel.first,((InheritedVertex *)brotherCell->value)->canonicalLabel.second);
      collectLabelFromIVertexToLabels((InheritedVertex *)brotherCell->value,labels,gapOfGlobalRootMemID);
    }else{
      // printf("Vertex delete !! ID : %d \n",((InheritedVertex *)brotherCell->value)->beforeID);      
    }

    goBackProcessInnerManyCommonPrefixVertices(targetCell,parent,goAheadStack,tInfo,targetDepth);
  }
}

//[#] Labelsを追加した処理
void goBackProcessInnerSingleCommonPrefixVertexWithLabels(ListBody *targetCell,TrieBody *currentNode,Stack *goAheadStack,TerminationConditionInfo *tInfo,int targetDepth,Labels *labels,int gapOfGlobalRootMemID){
  if(targetDepth == currentNode->depth){
    // printf("targetDepth == currentNode->depth !! ID : %d \n",((InheritedVertex *)targetCell->value)->beforeID);
    pushCell(currentNode->inheritedVertices,targetCell);
    ((InheritedVertex *)targetCell->value)->ownerNode = currentNode;
    ((InheritedVertex *)targetCell->value)->hashString->creditIndex = currentNode->depth;
    pushTrieBodyIntoGoAheadStackWithoutOverlap(goAheadStack,currentNode);
  }else if(isSingletonRedBlackTree(currentNode->children) && isSingletonList(((TrieBody *)(currentNode->children->body->value))->inheritedVertices)){
    // printf("targetDepth != currentNode->depth but isSingletonRedBlackTree and isSingletonList !! ID : %d \n",((InheritedVertex *)targetCell->value)->beforeID);
    TrieBody *childNode = (TrieBody *)currentNode->children->body->value;
    ListBody *brother = popCell(childNode->inheritedVertices);
    // printf("brother correntred!? id:%d\n",((InheritedVertex *)brother->value)->beforeID);

    decrementOmegaArray(tInfo->distribution,childNode->depth);

    //[#] Labelsを追加した処理
    goBackProcessInnerDoubleCommonPrefixVerticesWithLabels(targetCell,brother,currentNode,childNode,goAheadStack,tInfo,targetDepth,labels,gapOfGlobalRootMemID);
  }else{
    // printf("targetDepth != currentNode->depth and !isSingletonRedBlackTree or !isSingletonList !! ID : %d \n",((InheritedVertex *)targetCell->value)->beforeID);
    TrieBody *parent = currentNode->parent;

    goBackProcessInnerManyCommonPrefixVertices(targetCell,parent,goAheadStack,tInfo,targetDepth);
  }

  return;
}

//trie is minimal for uniqueness!!
//[#] Labelsを追加した処理
void goBackProcessWithLabels(ListBody *targetCell,TrieBody *currentNode,Stack *goAheadStack,TerminationConditionInfo *tInfo,int targetDepth,Labels *labels,int gapOfGlobalRootMemID){
  // fprintf(stdout,"LABEL:(%08X,%d)\n",((InheritedVertex *)targetCell->value)->canonicalLabel.first,((InheritedVertex *)targetCell->value)->canonicalLabel.second);
  if(targetDepth < currentNode->depth){
    if(isEmptyList(currentNode->inheritedVertices)){
      // printf("isEmptyList is true !! ID : %d \n",((InheritedVertex *)targetCell->value)->beforeID);
      TrieBody *parent = currentNode->parent;

      decrementOmegaArray(tInfo->distribution,currentNode->depth);
      if(parent->depth >= 0 && !isSingletonRedBlackTree(parent->children)){
        decrementOmegaArray(tInfo->increase,parent->depth);
      }

      // printf("delete Node. KEY:%08X\n",currentNode->key.u);
      deleteTrieBody(currentNode);

      //[#] Labelsを追加した処理
      goBackProcessInnerSingleCommonPrefixVertexWithLabels(targetCell,parent,goAheadStack,tInfo,targetDepth,labels,gapOfGlobalRootMemID);
    }else if(isSingletonList(currentNode->inheritedVertices)){
      // printf("isEmptyList is false but isSingletonList is true !! ID : %d \n",((InheritedVertex *)targetCell->value)->beforeID);
      ListBody *brother = popCell(currentNode->inheritedVertices);
      TrieBody *parent = currentNode->parent;

      decrementOmegaArray(tInfo->distribution,OMEGA);
      decrementOmegaArray(tInfo->distribution,OMEGA);

      //[#] Labelsを追加した処理
      goBackProcessInnerDoubleCommonPrefixVerticesWithLabels(targetCell,brother,parent,currentNode,goAheadStack,tInfo,targetDepth,labels,gapOfGlobalRootMemID);
    }else{
      // printf("isEmptyList is false and isSingletonList is false !! ID : %d \n",((InheritedVertex *)targetCell->value)->beforeID);
      TrieBody *parent = currentNode->parent;

      decrementOmegaArray(tInfo->distribution,OMEGA);

      // 57,64,66,68,73は全部ここでtargetCellになっている！
      goBackProcessInnerManyCommonPrefixVertices(targetCell,parent,goAheadStack,tInfo,targetDepth);
    }
  }else{
    //[$] ラベル更新っぽそうな怪しさmaxなので出力テスト
    // printf("targetDepth == currentNode->depth !! ID : %d ",((InheritedVertex *)targetCell->value)->beforeID);
    //[#] 強引にラベル追加処理
    // fprintf(stdout,"LABEL:(%08X,0) \n",currentNode->key.u);
    // collectLabelFromIVertexToLabels((InheritedVertex *)targetCell->value,labels,gapOfGlobalRootMemID);  
    // …本当にここは不要なの？  
    pushCell(currentNode->inheritedVertices,targetCell);
  }
}

//[#] Labelsを追加した処理
void goBackProcessOfCurrentConvertedVerticesWithLabels(Stack *BFSStack,Stack *goAheadStack,TerminationConditionInfo *tInfo,int targetDepth,Labels *labels,int gapOfGlobalRootMemID){
  int i;

  for(i=0;i<numStack(BFSStack);i++){
    ConvertedGraphVertex *cVertex = readStack(BFSStack,i);
    InheritedVertex *iVertex = cVertex->correspondingVertexInTrie;
    // inheritedVertexDump(iVertex);
    // printf("\n");
    TrieBody *currentNode = iVertex->ownerNode;
    ListBody *targetCell = iVertex->ownerCell;
    cutCell(targetCell);

    //[$] ラベル更新…っぽそうな怪しさmaxなので出力テスト
    // 57,64,66,68,73は全部ここでtargetCellになっている！
    //ここも必要
    // printf("Vertex dump in goBackProcessOfCurrentConvertedVertices !! ID : %d ",((InheritedVertex *)targetCell->value)->beforeID);
    // fprintf(stdout,"LABEL:(%08X,%d) \n",((InheritedVertex *)targetCell->value)->canonicalLabel.first,((InheritedVertex *)targetCell->value)->canonicalLabel.second);
    //[#] Labelsを追加した処理
    goBackProcessWithLabels(targetCell,currentNode,goAheadStack,tInfo,targetDepth,labels,gapOfGlobalRootMemID);
  }

  return;
}

//[#] Labelsを追加した処理
void goAheadProcessWithLabels(TrieBody *targetNode,Stack *goAheadStack,Stack *fixCreditIndexStack,TerminationConditionInfo *tInfo,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID,Labels *labels){
  List *inheritedVerticesList = targetNode->inheritedVertices;
  RedBlackTree *children = targetNode->children;

  if(isSingletonList(inheritedVerticesList) && isEmptyRedBlackTree(children) && targetNode->depth != -1){
    incrementOmegaArray(tInfo->distribution,targetNode->depth);
    //[$] ラベル更新
    // printf("Vertex move in goAhead !! ID : %d ",((InheritedVertex *)peekCell(inheritedVerticesList)->value)->beforeID);
    // fprintf(stdout,"LABEL:(%08X,%d) -> ",((InheritedVertex *)peekCell(inheritedVerticesList)->value)->canonicalLabel.first,((InheritedVertex *)peekCell(inheritedVerticesList)->value)->canonicalLabel.second);
    ((InheritedVertex *)peekCell(inheritedVerticesList)->value)->canonicalLabel.first = targetNode->key.u.ui32;
    // printf("Vertex move in      goAhead      : ");
    collectLabelFromIVertexToLabels((InheritedVertex *)peekCell(inheritedVerticesList)->value,labels,gapOfGlobalRootMemID);
    // fprintf(stdout,"LABEL:(%08X,%d)\n",((InheritedVertex *)peekCell(inheritedVerticesList)->value)->canonicalLabel.first,((InheritedVertex *)peekCell(inheritedVerticesList)->value)->canonicalLabel.second);
  }else{
    while(!isEmptyList(inheritedVerticesList)){
      ListBody *tmpCell = popCell(inheritedVerticesList);
      // 57,64,66,68,73は全部ここでtargetCellになっている？
      // /**/printf("Vertex move in goAhead ?! ID : %d ",((InheritedVertex *)tmpCell->value)->beforeID);
      // /**/fprintf(stdout,"LABEL:(%08X,%d) -> ",((InheritedVertex *)tmpCell->value)->canonicalLabel.first,((InheritedVertex *)tmpCell->value)->canonicalLabel.second);
      KeyContainer key = makeUInt32Key(callHashValue(((InheritedVertex *)tmpCell->value),targetNode->depth,cAfterGraph,gapOfGlobalRootMemID,fixCreditIndexStack));
      // /**/fprintf(stdout,"LABEL:(");
      // /**/keyDump(key);
      // /**/fprintf(stdout,",?)\n");
      // こっちを通ったものはmakeTrieMinimumでラベルを書き換えるかが決まるのでラベル更新はなし
      // collectLabelFromIVertexToLabels((InheritedVertex *)tmpCell->value,labels,gapOfGlobalRootMemID);

      TrieBody *nextNode = searchRedBlackTree(children,key);
      if(nextNode == NULL){
        if(!isEmptyRedBlackTree(children)){
          incrementOmegaArray(tInfo->increase,targetNode->depth);
        }

        nextNode = makeTrieBody();
        insertRedBlackTree(children,key,nextNode);
        nextNode->key = key;
        nextNode->parent = targetNode;
        nextNode->depth = targetNode->depth + 1;
      }

      if(!nextNode->isPushedIntoGoAheadStack && !isEmptyList(nextNode->inheritedVertices)){
        if(isSingletonList(nextNode->inheritedVertices)){
          decrementOmegaArray(tInfo->distribution,nextNode->depth);
        }else{
          ListBody *iterator;
          ListBody *sentinel = nextNode->inheritedVertices->sentinel;
          for(iterator=sentinel->next;iterator!=sentinel;iterator=iterator->next){
            decrementOmegaArray(tInfo->distribution,OMEGA);
          }
        }
      }

      pushCell(nextNode->inheritedVertices,tmpCell);
      ((InheritedVertex *)tmpCell->value)->ownerNode = nextNode;
      pushTrieBodyIntoGoAheadStackWithoutOverlap(goAheadStack,nextNode);
    }
  }
}

//[#] Labelsを追加した処理
void goAheadProcessOfCurrentTrieNodesWithLabels(Stack *goAheadStack,Stack *fixCreditIndexStack,TerminationConditionInfo *tInfo,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID,Labels *labels){
  Stack *nextGoAheadStack = makeStack();

  while(!isEmptyStack(goAheadStack)){
    TrieBody *targetNode = popTrieBodyFromGoAheadStackWithoutOverlap(goAheadStack);

    //[#] Labelsを追加した処理
    goAheadProcessWithLabels(targetNode,nextGoAheadStack,fixCreditIndexStack,tInfo,cAfterGraph,gapOfGlobalRootMemID,labels);
  }

  swapStack(nextGoAheadStack,goAheadStack);
  freeStack(nextGoAheadStack);

  return;
}


//[%%] stackを追加した処理
ConvertedGraphVertex *popConvertedVertexFromDiffInfoStackWithoutOverlapWithStack(Stack *stack,Stack *stack2){
  ConvertedGraphVertex *ret = popStack(stack);
  ret->isPushedIntoDiffInfoStack = FALSE;
  pushStack(stack2,ret);

  return ret;
}

//[%%] stackを追加した処理
//[#] Labelsを追加した処理
void deleteInheritedVerticesFromTrieWithStackAndLabels(Trie *trie,Stack *deletedVertices,Stack *goAheadStack,Stack *deletedStack,Labels *labels,int gapOfGlobalRootMemID){
  while(!isEmptyStack(deletedVertices)){
    ConvertedGraphVertex *targetCVertex = popConvertedVertexFromDiffInfoStackWithoutOverlapWithStack(deletedVertices,deletedStack);
    // printf(" - Vertex delete !! - \n");
    // convertedGraphVertexDump(targetCVertex);
    InheritedVertex *targetIVertex = targetCVertex->correspondingVertexInTrie;
    // inheritedVertexDump(targetIVertex);
    // printf("\n\n");    

    ListBody *targetCell = targetIVertex->ownerCell;
    cutCell(targetCell);
    TrieBody *currentNode = targetIVertex->ownerNode;

    //[#] Labelsを追加した処理
    // fprintf(stdout,"LABEL:(%08X,%d)\n",((InheritedVertex *)targetCell->value)->canonicalLabel.first,((InheritedVertex *)targetCell->value)->canonicalLabel.second);
    goBackProcessWithLabels(targetCell,currentNode,goAheadStack,trie->info,-1,labels,gapOfGlobalRootMemID);

    cutCell(targetCell);
    free(targetCell);
    freeInheritedVertex(targetIVertex);
  }
}



//[#] Labelsを追加した処理
void pushInftyDepthTrieNodesIntoGoAheadStackWithLabelsInner(TrieBody *body,Stack *goAheadStack,TerminationConditionInfo *tInfo,int depth,Labels *labels,int gapOfGlobalRootMemID);

//[#] Labelsを追加した処理
void pushInftyDepthTrieNodesIntoGoAheadStackWithLabelsInnerInner(RedBlackTreeBody *trieChildrenBody,Stack *goAheadStack,TerminationConditionInfo *tInfo,int depth,Labels *labels,int gapOfGlobalRootMemID){
  if(trieChildrenBody != NULL){
    //[#] Labelsを追加した処理
    pushInftyDepthTrieNodesIntoGoAheadStackWithLabelsInnerInner(trieChildrenBody->children[LEFT],goAheadStack,tInfo,depth,labels,gapOfGlobalRootMemID);
    //[#] Labelsを追加した処理
    pushInftyDepthTrieNodesIntoGoAheadStackWithLabelsInner(trieChildrenBody->value,goAheadStack,tInfo,depth,labels,gapOfGlobalRootMemID);
    //[#] Labelsを追加した処理
    pushInftyDepthTrieNodesIntoGoAheadStackWithLabelsInnerInner(trieChildrenBody->children[RIGHT],goAheadStack,tInfo,depth,labels,gapOfGlobalRootMemID);
  }

  return;
}

//[#] Labelsを追加した処理
void collectDescendantConvertedVerticesWithLabelsInner(TrieBody *ancestorBody,RedBlackTreeBody *rbtb,Labels *labels,int gapOfGlobalRootMemID);

//[#] Labelsを追加した処理
void pushInftyDepthTrieNodesIntoGoAheadStackWithLabelsInner(TrieBody *body,Stack *goAheadStack,TerminationConditionInfo *tInfo,int targetDepth,Labels *labels,int gapOfGlobalRootMemID){
  if(body->depth == targetDepth){
    if(!body->isPushedIntoGoAheadStack){
      //[#] Labelsを追加した処理
      collectDescendantConvertedVerticesWithLabelsInner(body,body->children->body,labels,gapOfGlobalRootMemID);
      deleteTrieDescendants(body);
      body->isInfinitedDepth = FALSE;

      if(!isSingletonList(body->inheritedVertices)){
        pushTrieBodyIntoGoAheadStackWithoutOverlap(goAheadStack,body);

        ListBody *sentinel = body->inheritedVertices->sentinel;
        ListBody *iterator;
        for(iterator=sentinel->next;iterator!=sentinel;iterator=iterator->next){
          decrementOmegaArray(tInfo->distribution,OMEGA);
        }
      }
    }
  }else{
    //[#] Labelsを追加した処理
    pushInftyDepthTrieNodesIntoGoAheadStackWithLabelsInnerInner(body->children->body,goAheadStack,tInfo,targetDepth,labels,gapOfGlobalRootMemID);
  }

  return;
}

//[#] Labelsを追加した処理
void pushInftyDepthTrieNodesIntoGoAheadStackWithLabels(Trie *trie,Stack *goAheadStack,int targetDepth,Labels *labels,int gapOfGlobalRootMemID){
  //[#] Labelsを追加した処理
  pushInftyDepthTrieNodesIntoGoAheadStackWithLabelsInner(trie->body,goAheadStack,trie->info,targetDepth,labels,gapOfGlobalRootMemID);

  return;
}

//[#] Labelsを追加した処理
void triePropagateInnerWithLabels(Trie *trie,Stack *BFSStack,Stack *initializeConvertedVerticesStack,Stack *goAheadStack,Stack *fixCreditIndexStack,TerminationConditionInfo *tInfo,int stepOfPropagation,ConvertedGraph *cAfterGraph,int gapOfGlobalRootMemID,Labels *labels){
  if(maxIndex(tInfo->distribution) == OMEGA && maxIndex(tInfo->increase) == stepOfPropagation - 1){
    pushInftyDepthTrieNodesIntoGoAheadStackWithLabels(trie,goAheadStack,stepOfPropagation,labels,gapOfGlobalRootMemID);
  }

  //[#] Labelsを追加した処理
  goBackProcessOfCurrentConvertedVerticesWithLabels(BFSStack,goAheadStack,tInfo,stepOfPropagation,labels,gapOfGlobalRootMemID);
  //[#] Labelsを追加した処理
  goAheadProcessOfCurrentTrieNodesWithLabels(goAheadStack,fixCreditIndexStack,tInfo,cAfterGraph,gapOfGlobalRootMemID,labels);

  getNextDistanceConvertedVertices(BFSStack,initializeConvertedVerticesStack,cAfterGraph);

  return;
}

//[#] Labelsを追加した処理
void collectDescendantConvertedVerticesWithLabels(TrieBody *ancestorBody,TrieBody *descendantBody,Labels *labels,int gapOfGlobalRootMemID);

//[#] Labelsを追加した処理
void collectDescendantConvertedVerticesWithLabelsInner(TrieBody *ancestorBody,RedBlackTreeBody *rbtb,Labels *labels,int gapOfGlobalRootMemID){
  if(rbtb != NULL){
    //[#] Labelsを追加した処理
    collectDescendantConvertedVerticesWithLabelsInner(ancestorBody,rbtb->children[LEFT],labels,gapOfGlobalRootMemID);
    //[#] Labelsを追加した処理
    collectDescendantConvertedVerticesWithLabels(ancestorBody,rbtb->value,labels,gapOfGlobalRootMemID);
    //[#] Labelsを追加した処理
    collectDescendantConvertedVerticesWithLabelsInner(ancestorBody,rbtb->children[RIGHT],labels,gapOfGlobalRootMemID);
  }

  return;
}

//[#] Labelsを追加した処理
void collectDescendantConvertedVerticesWithLabels(TrieBody *ancestorBody,TrieBody *descendantBody,Labels *labels,int gapOfGlobalRootMemID){
  if(isEmptyRedBlackTree(descendantBody->children)){
    while(!isEmptyList(descendantBody->inheritedVertices)){
      ListBody *targetCell = popCell(descendantBody->inheritedVertices);
      pushCell(ancestorBody->inheritedVertices,targetCell);
      ((InheritedVertex *)targetCell->value)->ownerNode = ancestorBody;
      ((InheritedVertex *)targetCell->value)->hashString->creditIndex = ancestorBody->depth;

      //[$] ラベル更新?
      // printf("Vertex move in collectDescendant !! ID : %d ",((InheritedVertex *)targetCell->value)->beforeID);
      // fprintf(stdout,"LABEL:(%08X,%d) -> ",((InheritedVertex *)targetCell->value)->canonicalLabel.first,((InheritedVertex *)targetCell->value)->canonicalLabel.second);
      ((InheritedVertex *)targetCell->value)->canonicalLabel.first = ancestorBody->key.u.ui32;
      // printf("Vertex move in collectDescendant : ");
      collectLabelFromIVertexToLabels((InheritedVertex *)targetCell->value,labels,gapOfGlobalRootMemID);
      // fprintf(stdout,"LABEL:(%08X,%d)\n",((InheritedVertex *)targetCell->value)->canonicalLabel.first,((InheritedVertex *)targetCell->value)->canonicalLabel.second);

    }
  }else{
    collectDescendantConvertedVerticesWithLabelsInner(ancestorBody,descendantBody->children->body,labels,gapOfGlobalRootMemID);
  }

  return;
}

//[#] Labelsを追加した処理
void makeTrieMinimumWithLabelsInner(TrieBody *body,TerminationConditionInfo *tInfo,int stepOfPropagation,Labels *labels,int gapOfGlobalRootMemID);
 
//[#] Labelsを追加した処理
void makeTrieMinimumWithLabelsInnerInner(RedBlackTreeBody *rbtb,TerminationConditionInfo *tInfo,int stepOfPropagation,Labels *labels,int gapOfGlobalRootMemID){
  if(rbtb != NULL){
    //[#] Labelsを追加した処理
    makeTrieMinimumWithLabelsInnerInner(rbtb->children[LEFT],tInfo,stepOfPropagation,labels,gapOfGlobalRootMemID);
    //[#] Labelsを追加した処理
    makeTrieMinimumWithLabelsInner(rbtb->value,tInfo,stepOfPropagation,labels,gapOfGlobalRootMemID);
    //[#] Labelsを追加した処理
    makeTrieMinimumWithLabelsInnerInner(rbtb->children[RIGHT],tInfo,stepOfPropagation,labels,gapOfGlobalRootMemID);
  }

  return;
}

//[#] Labelsを追加した処理
void makeTrieMinimumWithLabelsInner(TrieBody *body,TerminationConditionInfo *tInfo,int stepOfPropagation,Labels *labels,int gapOfGlobalRootMemID){
  if(body->depth == stepOfPropagation + 1){
    if(body->isPushedIntoGoAheadStack){
      ListBody *iterator;
      ListBody *sentinel = body->inheritedVertices->sentinel;

      for(iterator=sentinel->next;iterator!=sentinel;iterator=iterator->next){
        incrementOmegaArray(tInfo->distribution,OMEGA);
        //[$] ラベル更新
        ((InheritedVertex *)iterator->value)->canonicalLabel.first = body->key.u.ui32;
        // printf("Vertex move in  makeTrieMinimum  : ");
        collectLabelFromIVertexToLabels(iterator->value,labels,gapOfGlobalRootMemID);
      }
    }

    if(!isEmptyRedBlackTree(body->children)){
      //[#] Labelsを追加した処理
      // CHECKER("###collectDescendantConvertedVerticesWithLabelsInner###\n");
      collectDescendantConvertedVerticesWithLabelsInner(body,body->children->body,labels,gapOfGlobalRootMemID);
      deleteTrieDescendants(body);
    }

    body->isInfinitedDepth = TRUE;
  }else{
    //[#] Labelsを追加した処理
    makeTrieMinimumWithLabelsInnerInner(body->children->body,tInfo,stepOfPropagation,labels,gapOfGlobalRootMemID);
  }

  return;
}

//[#] Labelsを追加した処理
void makeTrieMinimumWithLabels(Trie *trie,int stepOfPropagation,Labels *labels,int gapOfGlobalRootMemID){
  TerminationConditionInfo *tInfo = trie->info;

  //[#] Labelsを追加した処理
  makeTrieMinimumWithLabelsInner(trie->body,tInfo,stepOfPropagation,labels,gapOfGlobalRootMemID);

  while(tInfo->distribution->maxFiniteIndex > stepOfPropagation){
    decrementOmegaArray(tInfo->distribution,tInfo->distribution->maxFiniteIndex);
    incrementOmegaArray(tInfo->distribution,OMEGA);
  }

  while(tInfo->increase->maxFiniteIndex > stepOfPropagation){
    decrementOmegaArray(tInfo->increase,tInfo->increase->maxFiniteIndex);
  }

  return;
}

//[#] Labelsを追加した処理
//[%%] stackを追加した処理
Bool triePropagateWithLabelsAndStack(Trie *trie,DiffInfo *diffInfo,ConvertedGraph *cAfterGraph,ConvertedGraph *cBeforeGraph,int gapOfGlobalRootMemID,int *stepOfPropagationPtr,Bool measure,Labels *labels,Stack *deletedStack){
  beforeTime = get_dtime();

  Stack *goAheadStack = makeStack();
  Stack *BFSStack = makeStack();
  Stack *initializeConvertedVerticesStack = makeStack();
  Stack *fixCreditIndexStack = makeStack();
  TerminationConditionInfo *tInfo = trie->info;

  //[%] stackを追加すべき処理
  // deleteInheritedVerticesFromTrie(trie,diffInfo->deletedVertices,goAheadStack);
  //[%%] stackを追加した処理
  //[#] Labelsを追加した処理
  deleteInheritedVerticesFromTrieWithStackAndLabels(trie,diffInfo->deletedVertices,goAheadStack,deletedStack,labels,gapOfGlobalRootMemID);
  addInheritedVerticesToTrie(trie,diffInfo->addedVertices,initializeConvertedVerticesStack,goAheadStack,cAfterGraph,gapOfGlobalRootMemID);

  afterTime = get_dtime();
  if(measure){
    triePropagateTime += afterTime - beforeTime;
  }

  //実際のSLIMでは起きない操作
  assureReferenceFromConvertedVerticesToInheritedVertices(cAfterGraph,cBeforeGraph,gapOfGlobalRootMemID);

  beforeTime = get_dtime();

  moveInheritedRelinkedVerticesToBFSStack(diffInfo->relinkedVertices,initializeConvertedVerticesStack,BFSStack);

  int stepOfPropagation = -1;
  // printf("stepOfPropagation : %d\n",stepOfPropagation);
  //[#] Labelsを追加した処理
  goAheadProcessOfCurrentTrieNodesWithLabels(goAheadStack,fixCreditIndexStack,tInfo,cAfterGraph,gapOfGlobalRootMemID,labels);

  stepOfPropagation = 0;
  // printf("stepOfPropagation : %d\n",stepOfPropagation);
  //[#] Labelsを追加した処理
  goAheadProcessOfCurrentTrieNodesWithLabels(goAheadStack,fixCreditIndexStack,tInfo,cAfterGraph,gapOfGlobalRootMemID,labels);

  while(triePropagationIsContinued(goAheadStack,tInfo,stepOfPropagation)){
    stepOfPropagation++;
    // printf("stepOfPropagation : %d\n",stepOfPropagation);
    //[#] Labelsを追加した処理
    triePropagateInnerWithLabels(trie,BFSStack,initializeConvertedVerticesStack,goAheadStack,fixCreditIndexStack,tInfo,stepOfPropagation,cAfterGraph,gapOfGlobalRootMemID,labels);
  }

  Bool verticesAreCompletelySorted = isDescreteTrie(goAheadStack,tInfo,stepOfPropagation) || isEmptyTrie(trie);
  // printf("!verticesAreCompletelySorted : %d\n",!verticesAreCompletelySorted);
  if(!verticesAreCompletelySorted){
    //[#] Labelsを追加した処理
    // makeTrieMinimum(trie,stepOfPropagation);
    makeTrieMinimumWithLabels(trie,stepOfPropagation,labels,gapOfGlobalRootMemID);

    while(!isEmptyStack(goAheadStack)){
      popTrieBodyFromGoAheadStackWithoutOverlap(goAheadStack);
    }

  }

  initializeConvertedVertices(initializeConvertedVerticesStack);
  initializeConvertedVertices(BFSStack);
  fixCreditIndex(fixCreditIndexStack,cAfterGraph,gapOfGlobalRootMemID);

  afterTime = get_dtime();
  if(measure){
    triePropagateTime += afterTime - beforeTime;
  }

  //実際のSLIMでは起きない操作
  initializeReferencesFromConvertedVerticesToInheritedVertices(cBeforeGraph);

  beforeTime = get_dtime();

  freeStack(goAheadStack);
  freeStack(BFSStack);
  freeStack(initializeConvertedVerticesStack);
  freeStack(fixCreditIndexStack);

  *stepOfPropagationPtr = stepOfPropagation;

  afterTime = get_dtime();
  if(measure){
    triePropagateTime += afterTime - beforeTime;
  }

  return verticesAreCompletelySorted;
}


