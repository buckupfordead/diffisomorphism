#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include"collection.h"
#include"hash.h"
#include"diff.h"
#include"convertedGraph.h"
#include"adjacent.h"
#include"trie.h"
#include"hashString.h"
#include"experiment.h"

AdjacentInfo *makeAdjacentInfo(){
  AdjacentInfo *adjacentInfo = (AdjacentInfo *)malloc(sizeof(AdjacentInfo));
  adjacentInfo->myself.first = 0;
  adjacentInfo->myself.second = -1;  
  adjacentInfo->adjacent = makeList();
  adjacentInfo->hash_myself=0;
  adjacentInfo->hash_info=0;
  return adjacentInfo;
}


void freeAdjacentInfo(AdjacentInfo *adjacentInfo){
  freeList(adjacentInfo->adjacent);
  free(adjacentInfo);
}

// CanonicalLabelどうしを比較してどちらが大きいかを返す関数
// GTはlabel1の方が大きい
Order compareCanonicalLabel(CanonicalLabel *label1, CanonicalLabel *label2){
  if(((unsigned int)label1->first) > ((unsigned int)label2->first)){
    return GT;
  }else if(((unsigned int)label1->first) == ((unsigned int)label2->first)){
    if(label1->second > label2->second){
      return GT;
    }else if(label1->second == label2->second){
      return EQ;
    }else{
      return LT;        
    }
  }else{
    return LT;
  }
}

// adjacentInfoのadjacentにCanonicalLabelを追加する関数
// adjacentは昇順に入れる
void addAdjacent(AdjacentInfo *adjacentInfo,CanonicalLabel *canonicalLabel){
  ListBody *cell = (ListBody *)malloc(sizeof(ListBody));
  cell->value = canonicalLabel;
  Order tmpOrder;

  ListBody *sentinel = adjacentInfo->adjacent->sentinel;
  ListBody *iterator;

  for(iterator=sentinel->next;iterator!=sentinel;){
    ListBody *iteratorNext = iterator->next;
    // tmpOrder=compareCanonicalLabel(addLabel,iterator->value);
    tmpOrder=compareCanonicalLabel(canonicalLabel,iterator->value);
    if(tmpOrder!=GT){
      connectCell(iterator->prev,cell);
      connectCell(cell,iterator);
      break;
    }
    iterator = iteratorNext;
  }if(tmpOrder==GT){
      connectCell(iterator->prev,cell);
      connectCell(cell,iterator);    
  }
}

// adjacentInfoをdumpするための関数
void dumpAdjacentInfo(AdjacentInfo *adjacentInfo){
  fprintf(stdout,"((%08X,%d),",adjacentInfo->myself.first,adjacentInfo->myself.second);
  fprintf(stdout,"{|");

  ListBody *sentinel = adjacentInfo->adjacent->sentinel;
  ListBody *iterator;

  for(iterator=sentinel->next;iterator!=sentinel;){
    ListBody *iteratorNext = iterator->next;

    CanonicalLabel *label =iterator->value;
    fprintf(stdout,"(%08X,%d)",label->first,label->second);

    if(iterator->next != sentinel){
      fprintf(stdout,",");
    }

    iterator = iteratorNext;
  }

  fprintf(stdout,"|})");
  return;
}

AdjacentInfoOfGraph *makeAdjacentInfoOfGraph(){
  AdjacentInfoOfGraph *adjacentInfoOfGraph = (AdjacentInfoOfGraph *)malloc(sizeof(AdjacentInfoOfGraph));
  adjacentInfoOfGraph->adjacents = makeDynamicArray();
  adjacentInfoOfGraph->adjacents = assureSizeOfDynamicArray(adjacentInfoOfGraph->adjacents,0XFFFF);
  adjacentInfoOfGraph->sum = 0;
  adjacentInfoOfGraph->mul = 0;

  return adjacentInfoOfGraph;
}

void freeAdjacentInfoOfGraph(AdjacentInfoOfGraph *adjacentInfoOfGraph){
  freeDynamicArrayAndValues(adjacentInfoOfGraph->adjacents,(void *)freeStack);
  free(adjacentInfoOfGraph);
}

// adjacentInfoどうしを比較して同じかどうかを返す関数
Bool compareAdjacentInfo(AdjacentInfo *adjacentInfo1,AdjacentInfo *adjacentInfo2){
  // AdjacentInfoどうしの全体or頂点自身のhashが異なればfalse
  if(adjacentInfo1->hash_info != adjacentInfo2->hash_info
     || adjacentInfo1->hash_myself != adjacentInfo2->hash_myself){
    return FALSE; 
  }else{
    // adjacentの要素どうしを比較
    ListBody *sentinel1 = adjacentInfo1->adjacent->sentinel;
    ListBody *iterator1;
    ListBody *sentinel2 = adjacentInfo2->adjacent->sentinel;
    ListBody *iterator2;
    for(iterator1=sentinel1->next,iterator2=sentinel2->next;
        iterator1!=sentinel1 && iterator2!=sentinel2;){
      ListBody *iteratorNext1 = iterator1->next;
      ListBody *iteratorNext2 = iterator2->next;
      if(compareCanonicalLabel(iterator1->value,iterator2->value)!=EQ){
        return FALSE;
      }
      iterator1 = iteratorNext1;
      iterator2 = iteratorNext2;
    }
    if(iterator1==sentinel1 && iterator2==sentinel2){
      return TRUE;
    }else{
      return FALSE;
    }
  }
}

// adjacentInfoOfGraphにadjacentInfoを追加する関数
Bool addAdjacentInfo(AdjacentInfoOfGraph *adjacentInfoOfGraph,AdjacentInfo *adjacentInfo){
  int index = (int)adjacentInfo->hash_myself & 0xFFFF;
    Stack *stack = readDynamicArray(adjacentInfoOfGraph->adjacents,index);
    if(stack==NULL){
      // stackがなかった場合は新しくadjacentInfoを格納するstackを宣言
      stack = makeStack();
    }else{
      // stackがあった場合は全く同じ情報のadjacentInfoがないかチェック
      int i;
      for(i=0;i<numStack(stack);i++){
        if(compareAdjacentInfo(adjacentInfo,readStack(stack,i))){
          // 同じ情報のadjacentInfoがあれば追加せずにFALSEを返す
          return FALSE;
        }
      }
    }
  pushStack(stack,adjacentInfo);
  writeDynamicArray(adjacentInfoOfGraph->adjacents,index,stack);
  //hash値更新
  adjacentInfoOfGraph->sum += adjacentInfo->hash_info;
  adjacentInfoOfGraph->mul *= (adjacentInfo->hash_info*2)+1;
  // printf("in add %08X %08X\n",adjacentInfo->hash_myself,adjacentInfoOfGraph->sum^adjacentInfoOfGraph->mul);
  return TRUE;
}

// adjacentInfoOfGraphからadjacentInfoを削除する関数
Bool deleteAdjacentInfo(AdjacentInfoOfGraph *adjacentInfoOfGraph,AdjacentInfo *adjacentInfo){
  int index = (int)adjacentInfo->hash_myself & 0xFFFF;
  Stack *stack = readDynamicArray(adjacentInfoOfGraph->adjacents,index);
  if(stack==NULL){
    // // stackがなかった場合はエラーを出力して終了
    // fprintf(stderr," --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
    // fprintf(stderr,"stack not found\n");
    // exit(EXIT_FAILURE);
    // // stackがなかった場合はFALSEを返す
    return FALSE;
  }else{
    int i;
    int numOfStack = numStack(stack); //for文の中で一時的に増減するので先に宣言
    for(i=0;i<numOfStack;i++){
      // stackから一時的に値を取り出し、もし違うなら最後に戻す
      // もし同じなら既に取り出しているので追加で削除する必要はない
      AdjacentInfo *tmpInfo = popStack(stack);
      if(!compareAdjacentInfo(adjacentInfo,tmpInfo)){
        pushStack(stack,tmpInfo);
      }else{
        //見つかったらhash値を更新してTRUEをreturn
        adjacentInfoOfGraph->sum -= tmpInfo->hash_info;
        adjacentInfoOfGraph->mul *= inverse((tmpInfo->hash_info*2)+1);
        if(numStack(stack)==0){
          //もし要素が0ならstackとbody[index]自体を開放
          // freeStack(stack);
          // free(adjacentInfoOfGraph->adjacents->body[index]);
        }
        // printf("in del %08X %08X\n",adjacentInfo->hash_myself,adjacentInfoOfGraph->sum^adjacentInfoOfGraph->mul);
        return TRUE;
      }
    }
    // // stackの中になかった場合もエラーを出力して終了
    // fprintf(stderr," --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
    // fprintf(stderr,"adjacentInfo is not found in the stack\n");
    // exit(EXIT_FAILURE);
    // // stack中になかった場合もFALSEを返す
    return FALSE;
  }
}

// adjacentInfoOfGraphの中から該当するLabelが頂点になっているadjacentInfoのポインタを返す関数
// どのadajacentInfoの中にもエラーがない限り特定のLabelが頂点になっているinfoは1つなので、
// 特定のadjacentInfoが返せるはず
AdjacentInfo *findAdjacentInfoFromLabel(AdjacentInfoOfGraph *adjacentInfoOfGraph,CanonicalLabel canonicalLabel,Bool delete){
  int index = (int)labelHashValue(canonicalLabel) & 0xFFFF;
  Stack *stack = readDynamicArray(adjacentInfoOfGraph->adjacents,index);
  if(stack==NULL){
    // stackがなかった場合はNULLを返す
    return NULL;
  }else{
    int i;
    // deleteがTRUEの時は削除して情報を返す
    int numOfStack = numStack(stack); //for文の中で一時的に増減するので先に宣言
    for(i=0;i<numOfStack;i++){
      // stackから一時的に値を取り出し、もし違うなら最後に戻す
      // もし同じなら既に取り出しているので追加で削除する必要はない
      AdjacentInfo *tmpInfo = popStack(stack);
      if(!compareCanonicalLabel(&canonicalLabel,&(tmpInfo->myself))){
        pushStack(stack,tmpInfo);
      }else{
        if(!delete){
          // deleteがFALSEの時は削除せずに情報を返す
          pushStack(stack,tmpInfo);
          return tmpInfo;
        }
        //見つかったらhash値を更新してreturn
        adjacentInfoOfGraph->sum -= tmpInfo->hash_info;
        adjacentInfoOfGraph->mul *= inverse((tmpInfo->hash_info*2)+1);
        // printf("in fin %08X %08X\n",labelHashValue(canonicalLabel),adjacentInfoOfGraph->sum^adjacentInfoOfGraph->mul);
        if(numStack(stack)==0){
          //もし要素が0ならstackとbody[index]自体を開放
          // freeStack(stack);
          // free(adjacentInfoOfGraph->adjacents->body[index]);
        }
        return tmpInfo;
      }
    }
    // stackの中になかった場合もNULLを返す
    return NULL;
  }
}

// hash値を出力するためだけの関数
void printAdjacentGraphHash(AdjacentInfoOfGraph *adjacentInfoOfGraph){
  fprintf(stdout,"hash of the graph : %08X\n",(adjacentInfoOfGraph->sum)^(adjacentInfoOfGraph->mul));
  return;
}

// adjacentInfoOfGraphを出力するための関数
void dumpAdjacentInfoOfGraph(AdjacentInfoOfGraph *adjacentInfoOfGraph){
  int i,j;

  fprintf(stdout,"{\n");
  for(i=0;i<adjacentInfoOfGraph->adjacents->cap;i++){
    Stack *stack = readDynamicArray(adjacentInfoOfGraph->adjacents,i);
    if(stack != NULL && numStack(stack)!=0){
      fprintf(stdout,"  DA[%04X(%d)]->",i,numStack(stack));
      for(j=0;j<numStack(stack);j++){
        AdjacentInfo *adjacentInfo = readStack(stack,j);
        if(adjacentInfo != NULL){
          if(j != 0){
            fprintf(stdout,"               ");  
          }
          // calcAdjacentInfoHashValue(adjacentInfo);
          fprintf(stdout,"S[%d] [%08X:%08X] <- ",j,adjacentInfo->hash_myself,adjacentInfo->hash_info);
          dumpAdjacentInfo(adjacentInfo);
          fprintf(stdout,",\n");
        }
      }
    }
  }

  fprintf(stdout,"}\n");
  // fprintf(stdout,"hash of the graph : %08X\n",(adjacentInfoOfGraph->sum)^(adjacentInfoOfGraph->mul));
  // printAdjacentGraphHash(adjacentInfoOfGraph);
  return;
}


// CanonicalLableの配列を宣言・初期化するための関数
CanonicalLabel *initializeCanonicalLabels(int numOfLabels){
  CanonicalLabel *labels = (CanonicalLabel *)malloc(sizeof(CanonicalLabel)*numOfLabels);
  int i;
  for(i=0; i<numOfLabels; i++){
    labels[i].first = 0;
    labels[i].second = -1;
  }

  return labels;
}

// CanonicalLabelの配列をダンプするための関数
void dumpCanonicalLabels(CanonicalLabel *labels, int numOfLabels){
  int ID;
  for(ID=0;ID<numOfLabels;ID++){
    if(labels[ID].second == -1){
      // fprintf(stdout,"%2d:not yet\n",ID);
    }else{
      fprintf(stdout,"%2d:(%08X,%d)\n",
        ID,labels[ID].first,labels[ID].second);
    }
  }
}

// atomLabelsとHyperLinkLabelsを同時にダンプするための関数
void dumpTwoCanonicalLabels(CanonicalLabel *atomLabels,
                   CanonicalLabel *hyperLinkLabels,
                   int numOfAtoms,
                   int numOfHyperLinks){
  fprintf(stdout,"\nATOM LABELS:\n");
  dumpCanonicalLabels(atomLabels,numOfAtoms);  
  fprintf(stdout,"\nHYPERLINK LABELS:\n");
  dumpCanonicalLabels(hyperLinkLabels,numOfHyperLinks);  
}

Labels *makeLabels(int numOfAtoms, int numOfHyperLinks){
  Labels *labels = (Labels *)malloc(sizeof(Labels));
  labels->numOfAtoms = numOfAtoms;
  labels->numOfHyperLinks = numOfHyperLinks;
  labels->atomLabels = initializeCanonicalLabels(numOfAtoms);
  labels->hyperLinkLabels = initializeCanonicalLabels(numOfHyperLinks);

  return labels;
}

void freeLabels(Labels *labels){
  free(labels->atomLabels);
  free(labels->hyperLinkLabels);
  free(labels);

  return; 
}

void dumpLabels(Labels *labels){
  dumpTwoCanonicalLabels(labels->atomLabels,labels->hyperLinkLabels,labels->numOfAtoms,labels->numOfHyperLinks);

  return;
}


// 以下、トライ木から全てのatomおよびHyperLinkのCanonicalLabelを収集する関数群の宣言
/*
   詳しい説明は関数の処理の記述の先頭にまわして、ここではトライ木の内部構造について述べる
   この実装におけるトライ木の構造は
   トライ木：Trie
     └トライ木のbody：TrieBody(深さ:-1)
       └赤黒木：children
         └赤黒木のbody：body
           ├赤黒木のbody：children[LEFT]
           ├赤黒木のbody：children[RIGHT]
           └トライ木のbody：value(深さ:0)
             ├頂点のリスト：inheritedVertices
             └赤黒木：children
               └赤黒木のbody：body
                 ├赤黒木のbody：children[LEFT]
                 ├赤黒木のbody：children[RIGHT]
                 └トライ木のbody：value(深さ:1)
   …？わからない
*/
void collectLabelFromTrie(Trie *trie,
                          CanonicalLabel *atomLabels,
                          CanonicalLabel *hyperLinkLabels,
                          int gapOfGlobalRootMemID);

void collectLabelFromRedBlackTreeBody(RedBlackTreeBody *rbtb,
                                      CanonicalLabel *atomLabels,
                                      CanonicalLabel *hyperLinkLabels,
                                      int gapOfGlobalRootMemID);

void collectLabelFromTrieBody(TrieBody *body,
                              CanonicalLabel *atomLabels,
                              CanonicalLabel *hyperLinkLabels,
                              int gapOfGlobalRootMemID);

void collectLabelFromIVertices(List *inheritedVertices,
                               CanonicalLabel *atomLabels,
                               CanonicalLabel *hyperLinkLabels,
                               int gapOfGlobalRootMemID);

void collectLabelFromIVertex(InheritedVertex *iVertex,
                             CanonicalLabel *atomLabels,
                             CanonicalLabel *hyperLinkLabels,
                             int serial,
                             int gapOfGlobalRootMemID);


// トライ木から全てのatomおよびHyperLinkのCanonicalLabelを収集する関数
// ただし、CanonicalLabelの格納先はatomおよびHyperLinkのIDに等しい
void collectLabelFromTrie(Trie *trie,
                          CanonicalLabel *atomLabels,
                          CanonicalLabel *hyperLinkLabels,
                          int gapOfGlobalRootMemID){

  // printf(" --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
  collectLabelFromRedBlackTreeBody(trie->body->children->body,atomLabels,hyperLinkLabels,gapOfGlobalRootMemID);

  return;
}

// トライ木から全てのatomおよびHyperLinkのCanonicalLabelを収集する関数
// CanonicalLabelの格納先がlabelsの中になっただけ
Labels *collectLabelFromTrie_toLabels(Trie *trie,
                                      int numOfAtoms,
                                      int numOfHyperLinks,
                                      int gapOfGlobalRootMemID){

  Labels *labels = makeLabels(numOfAtoms,numOfHyperLinks);
  collectLabelFromRedBlackTreeBody(trie->body->children->body,labels->atomLabels,labels->hyperLinkLabels,gapOfGlobalRootMemID);

  return labels;
}

// トライ木の中で木構造をなしているのは赤黒木
// 赤黒木のchildrenの内部を探索しながら赤黒木が持つとトライ木の内部を探索し、
// 赤黒木内の全てのatomおよびHyperLinkのCanonicalLabelを収集する関数
void collectLabelFromRedBlackTreeBody(RedBlackTreeBody *rbtb,
                                      CanonicalLabel *atomLabels,
                                      CanonicalLabel *hyperLinkLabels,
                                      int gapOfGlobalRootMemID){

  if(rbtb == NULL){
    // printf(" --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
    // fprintf(stdout, "this TrieBody is null \n");
    return;
  }else{
    // printf(" --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
    collectLabelFromRedBlackTreeBody(rbtb->children[LEFT],atomLabels,hyperLinkLabels,gapOfGlobalRootMemID);
    // trieDumpInner(rbtb->value);
    collectLabelFromTrieBody(rbtb->value,atomLabels,hyperLinkLabels,gapOfGlobalRootMemID);
    collectLabelFromRedBlackTreeBody(rbtb->children[RIGHT],atomLabels,hyperLinkLabels,gapOfGlobalRootMemID);
    return;
  }
}

// トライ木がもつTrieBodyの中にあるinheritedVerticesから
// inheitedVertices内の全てのatomおよびHyperLinkのCanonicalLabelを収集し、
// 同時にTrieBodyがもつ赤黒木の内部も探索する関数
void collectLabelFromTrieBody(TrieBody *body,
                              CanonicalLabel *atomLabels,
                              CanonicalLabel *hyperLinkLabels,
                              int gapOfGlobalRootMemID){

  // printf(" --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
  // fprintf(stdout,"depth : %d\n",body->depth);
  collectLabelFromIVertices(body->inheritedVertices,atomLabels,hyperLinkLabels,gapOfGlobalRootMemID);
  collectLabelFromRedBlackTreeBody(body->children->body,atomLabels,hyperLinkLabels,gapOfGlobalRootMemID);

  return;
}

// トライ木の中のinheritedVerticesからinheritedVertexを取り出し、
// 各inheritedVertexの中に格納されているcanonicalLabelを格納する関数
// canonicalLabelを格納すると同時に正規ラベルの通し番号の更新も行なっている
// void listDump(List *list,void valueDump(void *)){
void collectLabelFromIVertices(List *inheritedVertices,
                               CanonicalLabel *atomLabels,
                               CanonicalLabel *hyperLinkLabels,
                               int gapOfGlobalRootMemID){

  // printf(" --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
  ListBody *sentinel = inheritedVertices->sentinel;
  ListBody *iterator;

  // 疑問：atomとhyperLinkに同一のHashが割り振られることはあるのか？
  // あるなら/*#*/が先頭についた部分の処理を見なおさないといけない
  // →trie.c 内 makeTrieMinimumInner
  //    CanonicalLabel.firstでTrieBodyのkeyを格納してる直後に
  //    countしたserialを格納するだけで完了
  /*#*/ int serial; //通し番号

  /*#*/ serial=0;
  for(iterator=sentinel->next;iterator!=sentinel;){
    ListBody *iteratorNext = iterator->next;
    collectLabelFromIVertex(iterator->value,atomLabels,hyperLinkLabels,serial,gapOfGlobalRootMemID);
    iterator = iteratorNext;
    // /*#*/ serial++;
  }

  return;
}

// トライ木の中のinheritedVerticesはinheritedVertexのリスト
// リストから取り出したこのinheritedVertexが1対1で対応するatomかhyperLinkのIDの場所に、
// このVertexの中に格納されているcanonicalLabelを格納する関数
void collectLabelFromIVertex(InheritedVertex *iVertex, 
                             CanonicalLabel *atomLabels,
                             CanonicalLabel *hyperLinkLabels,
                             int serial,
                             int gapOfGlobalRootMemID){

  int ID = iVertex->beforeID + gapOfGlobalRootMemID;
  // /*#*/ //通し番号の更新
  // /*#*/ iVertex->canonicalLabel.second = serial;
  // fprintf(stdout," --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
  switch(iVertex->type){
    case convertedAtom:
      // fprintf(stdout,"LABEL:(%08X,%d) -> atomLabels[%d]\n",iVertex->canonicalLabel.first,iVertex->canonicalLabel.second,ID);
      atomLabels[ID] = iVertex->canonicalLabel;
      break;
    case convertedHyperLink:
      // fprintf(stdout,"LABEL:(%08X,%d) -> hyperLinkLabels[%d]\n",iVertex->canonicalLabel.first,iVertex->canonicalLabel.second,ID);
      hyperLinkLabels[ID] = iVertex->canonicalLabel;
      break;
    default:
      fprintf(stderr," --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
      fprintf(stderr,"This is unexpected vertex type\n");
      exit(EXIT_FAILURE);
      break;
  }

  return;
}

// collectLabelFromIvertexの引数をLabelsにした関数
// serialは使っていないので引数から削除
void collectLabelFromIVertexToLabels(InheritedVertex *iVertex, Labels *labels, int gapOfGlobalRootMemID){

  collectLabelFromIVertex(iVertex,labels->atomLabels,labels->hyperLinkLabels,0,gapOfGlobalRootMemID);

  return;
}


//トライ木とConvertedGraphから状態全体の周辺情報を集める関数
AdjacentInfoOfGraph *collectAdjacentInfoOfGraph(Trie *trie, ConvertedGraph *cgraph, int gapOfGlobalRootMemID){
  AdjacentInfoOfGraph *adjacentInfoOfGraph = makeAdjacentInfoOfGraph();

  // トライ木の中を毎回走査するのは効率が悪いのでatom及びHyperLinkのIDと正規化ラベルを紐付け
  // DynamicArrayの中全てに定義されているわけではないが、
  // cgraph->atoms->cap にcgraph->atoms(DynamicArray)のArray数
  // cgraph->hyperLinks->cap にcgraph->hyperLinks(DynamicArray)のArray数
  // がそれぞれ格納されているのでその分だけCanonicalLabelの配列を作って対応

  int numOfAtoms = cgraph->atoms->cap;
  int numOfHyperLinks = cgraph->hyperLinks->cap;
  int i,j;
  CanonicalLabel *atomLabels = (CanonicalLabel *)malloc(sizeof(CanonicalLabel)*numOfAtoms);
  CanonicalLabel *hyperLinkLabels = (CanonicalLabel *)malloc(sizeof(CanonicalLabel)*numOfHyperLinks);
  // initialize
  for(i=0; i<numOfAtoms; i++){
    atomLabels[i].first = 0;
    atomLabels[i].second = -1;
  }
  for(i=0; i<numOfHyperLinks; i++){
    hyperLinkLabels[i].first = 0;
    hyperLinkLabels[i].second = -1;
  }

  // トライ木のKEYを対応するatom及びHyperLinkのIDの場所へ格納
  // IDはトライ木の中のiVertex->ID"+gapOfGlobalRootMemID"で取得可能
  collectLabelFromTrie(trie,atomLabels,hyperLinkLabels,gapOfGlobalRootMemID);
  // dumpTwoCanonicalLabels(atomLabels,hyperLinkLabels,numOfAtoms,numOfHyperLinks);

  // cgraph->atomsのIDとLinksからアトム毎のadjacentInfoを作成し、adjacentInfoOfGraphに追加
  for(i=0; i<numOfAtoms; i++){
    // 正規ラベルのsecondが-1以外に書き換わっていた時、
    // そのIDのアトムはgraph中に存在している
    if(atomLabels[i].second != -1){
      AdjacentInfo *adjacentInfo = makeAdjacentInfo();
      // そのアトム自身のラベルはatomLabels[i]に格納済
      adjacentInfo->myself = atomLabels[i];
      // cgraph->atoms->body[i]->links にlinkの内訳が入っている
      // link->attrの値で何が接続されているかわかるが、
      // (link->attr)<128:普通のアトム、(link->attr)=138:HyperLink以外はそのままスルー
      ConvertedGraphVertex *cVertex = readDynamicArray(cgraph->atoms,i);
      // convertedGraphVertexDump(cVertex);
      for(j=0;j<numStack(cVertex->links);j++){
        // LMNtalLinkDump((LMNtalLink *)readStack(cVertex->links,j));
        LMNtalLink *link = readStack(cVertex->links,j);
        if(link->attr < 128){
          // fprintf(stdout,"  ");
          // LMNtalLinkDump(link);
          // 接続先はatomなのでatomLabelsからラベルの情報を格納
          // fprintf(stdout," LABEL:(%08X,%d)\n",atomLabels[link->data.ID].first,atomLabels[link->data.ID].second);
          pushList(adjacentInfo->adjacent,&atomLabels[link->data.ID]);
        }else if(link->attr == 138){
          pushList(adjacentInfo->adjacent,&hyperLinkLabels[link->data.ID]);
        }
      }
      calcAdjacentInfoHashValue(adjacentInfo);
      addAdjacentInfo(adjacentInfoOfGraph,adjacentInfo);
    }
  }

  dumpAdjacentInfoOfGraph(adjacentInfoOfGraph);

  free(atomLabels);
  free(hyperLinkLabels);

  return adjacentInfoOfGraph;
}

//ConvertedGraphとLabelsから状態全体の周辺情報を集める関数
AdjacentInfoOfGraph *collectAdjacentInfoOfGraphFromLabels(ConvertedGraph *cgraph, Labels *labels, int gapOfGlobalRootMemID){
  AdjacentInfoOfGraph *adjacentInfoOfGraph = makeAdjacentInfoOfGraph();

  // dumpTwoCanonicalLabels(labels->atomLabels,labels->hyperLinkLabels,labels->numOfAtoms,labels->numOfHyperLinks);
  int i,j;

  // cgraph->atomsのIDとLinksからアトム毎のadjacentInfoを作成し、adjacentInfoOfGraphに追加
  for(i=0; i<labels->numOfAtoms; i++){
    // 正規ラベルのsecondが-1以外に書き換わっていた時、
    // そのIDのアトムはgraph中に存在している
    if(labels->atomLabels[i].second != -1){
      AdjacentInfo *adjacentInfo = makeAdjacentInfo();
      // そのアトム自身のラベルはatomLabels[i]に格納済
      adjacentInfo->myself = labels->atomLabels[i];
      // cgraph->atoms->body[i]->links にlinkの内訳が入っている
      // link->attrの値で何が接続されているかわかるが、
      // (link->attr)<128:普通のアトム、(link->attr)=138:HyperLink以外はそのままスルー
      ConvertedGraphVertex *cVertex = readDynamicArray(cgraph->atoms,i);
      // convertedGraphVertexDump(cVertex);
      for(j=0;j<numStack(cVertex->links);j++){
        // LMNtalLinkDump((LMNtalLink *)readStack(cVertex->links,j));
        LMNtalLink *link = readStack(cVertex->links,j);
        if(link->attr < 128){
          // fprintf(stdout,"  ");
          // LMNtalLinkDump(link);
          // 接続先はatomなのでatomLabelsからラベルの情報を格納
          // fprintf(stdout," LABEL:(%08X,%d)\n",atomLabels[link->data.ID].first,atomLabels[link->data.ID].second);
          // pushList(adjacentInfo->adjacent,&labels->atomLabels[link->data.ID]);
          addAdjacent(adjacentInfo,&labels->atomLabels[link->data.ID]);
        }else if(link->attr == 138){
          // pushList(adjacentInfo->adjacent,&labels->hyperLinkLabels[link->data.ID]);
          addAdjacent(adjacentInfo,&labels->hyperLinkLabels[link->data.ID]);
        }
      }
      calcAdjacentInfoHashValue(adjacentInfo);
      addAdjacentInfo(adjacentInfoOfGraph,adjacentInfo);
    }
  }

  // dumpAdjacentInfoOfGraph(adjacentInfoOfGraph);

  return adjacentInfoOfGraph;
}

//・書換え前のLabels & AdjacentInfoOfGraph
//・書換え時に消去された頂点のStack:deletedVertices
//・書換え後のLabels & ConvertedGraph
//・(gapOfGlobalRootMemID)
// から
//・書換え前後の差分(対称差)
// を生成して
//・書換え後のAdjacentInfoOfGraph
// を返す関数
AdjacentInfoOfGraph *makeAfterAndDiffAdjacentInfoOfGraph(Labels *beforeLabels,
                                                         AdjacentInfoOfGraph *beforeAdjacentInfoOfGraph,
                                                         Stack *deletedVertices,
                                                         Labels *afterLabels,
                                                         ConvertedGraph *afterGraph,
                                                         int gapOfGlobalRootMemID,
                                                         AdjacentInfoOfGraph *diffAdjacentInfoOfGraph){

  int i,j;

  beforeTime = get_dtime();

  /*[*1/22*]
  // 頂点(アトム)ごとにどういう状態なのかを把握するためstatusの配列を作成、格納
  int numOfStatus = MAX(beforeLabels->numOfAtoms,afterLabels->numOfAtoms);
  Status status[numOfStatus];

  for(i=0;i<numOfStatus;i++){
    status[i]=NONE;
  }
  // もし書き換え後にラベルが更新されている隣接頂点があれば、
  // たとえその頂点自身が更新されていなくても更新しないといけない
  // そのために更新すべき頂点のIDのStackを作成
  IntStack *needUpdate = makeIntStack();
  [*1/22*]*/

  // AdjacentInfoOfGprahに同一情報の重複は許されない
  // ただし、同一の情報が追加に失敗してから消去される可能性があるため、重複した情報を別に持っておく
  AdjacentInfoOfGraph *overlapInfoOfGraph = makeAdjacentInfoOfGraph();


  // 書換え前から削除されたadjacentInfoを抜き出して削除
  int tmpNum=numStack(deletedVertices);
  for(i=0;i<tmpNum;i++){
    ConvertedGraphVertex *tmpVertex = readStack(deletedVertices,i);
    // printf("deleted? ID : %d %d\n",tmpVertex->ID,gapOfGlobalRootMemID);
    // AdjacentInfo *tmpInfo = findAdjacentInfoFromLabel(beforeAdjacentInfoOfGraph,beforeLabels->atomLabels[tmpVertex->ID-gapOfGlobalRootMemID],FALSE);
    AdjacentInfo *tmpInfo = findAdjacentInfoFromLabel(beforeAdjacentInfoOfGraph,beforeLabels->atomLabels[tmpVertex->ID-gapOfGlobalRootMemID],TRUE);
    // addAdjacentInfo(deleteInfoOfGraph,tmpInfo);
    addAdjacentInfo(diffAdjacentInfoOfGraph,tmpInfo);
  /*[*1/22*]
    status[tmpVertex->ID-gapOfGlobalRootMemID]=UPDATED;
  [*1/22*]*/
  }

  // printf("\n --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
  // fprintf(stdout,"\n --- diffAdjacentInfoOfGraph ---\n");
  // dumpAdjacentInfoOfGraph(diffAdjacentInfoOfGraph);
  // printf("\n --- file:%s...func:%s...line:%d ---\n\n",__FILE__,__func__,__LINE__);
  // dumpAdjacentInfoOfGraph(beforeAdjacentInfoOfGraph);

  // afterGraph->atomsのIDとLinksからアトム毎のadjacentInfoを作成し、adjacentInfoOfGraphに追加
  for(i=0; i<afterLabels->numOfAtoms; i++){
    // afterLabels->atomLabelsのsecondが-1以外に書き換わっていた時、
    // そのIDのアトムは書換え前後で少なくとも1回はラベルの書き換え処理を行なっている
    if(afterLabels->atomLabels[i].second != -1){
      // printf("ID : %d\n",i);
      // printf("\n --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
      AdjacentInfo *adjacentInfo = makeAdjacentInfo();
      // そのアトム自身のラベルはatomLabels[i]に格納済
      // fprintf(stdout,"aAt[%d]:(%08X,%d),",i,afterLabels->atomLabels[i].first,afterLabels->atomLabels[i].second);
      adjacentInfo->myself = afterLabels->atomLabels[i];
      // cgraph->atoms->body[i]->links にlinkの内訳が入っている
      // (link->attr)<128:普通のアトム、(link->attr)=138:HyperLink以外はそのままスルー
      ConvertedGraphVertex *cVertex = readDynamicArray(afterGraph->atoms,i);
      // convertedGraphVertexDump(cVertex);
      /*[*1/22*]
      Bool bool = (Order)compareCanonicalLabel(&beforeLabels->atomLabels[i],&afterLabels->atomLabels[i])-EQ; 
      [*1/22*]*/
      for(j=0;j<numStack(cVertex->links);j++){
        LMNtalLink *link = readStack(cVertex->links,j);
        if(link->attr < 128){
          // 接続先はatomなのでatomLabelsからラベルの情報を格納
          // afterLabelsに格納されていない場合はbeforeLabelsから変化していないので、
          // beforeLabelから格納する
          if(afterLabels->atomLabels[link->data.ID].second==-1){
            addAdjacent(adjacentInfo,&beforeLabels->atomLabels[link->data.ID]);
          }else{
            addAdjacent(adjacentInfo,&afterLabels->atomLabels[link->data.ID]);
          }
          /*[*1/22*]
          if(bool && status[link->data.ID]<ADDED){
            pushIntStack(needUpdate,link->data.ID);
            status[link->data.ID]=ADDED;
            // printf("(%d ADDED)",link->data.ID);
          }
          [*1/22*]*/
        }else if(link->attr == 138){
          if(afterLabels->hyperLinkLabels[link->data.ID].second==-1){
            addAdjacent(adjacentInfo,&beforeLabels->hyperLinkLabels[link->data.ID]);
          }else{
            addAdjacent(adjacentInfo,&afterLabels->hyperLinkLabels[link->data.ID]);
          }
        }
      }
      calcAdjacentInfoHashValue(adjacentInfo);
      // printf("\n --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
      // fprintf(stdout,"\n");
      // dumpAdjacentInfo(adjacentInfo);
      AdjacentInfo *tmpInfo;
      tmpInfo = findAdjacentInfoFromLabel(beforeAdjacentInfoOfGraph,beforeLabels->atomLabels[i],FALSE);
      if(tmpInfo!=NULL 
        && !compareAdjacentInfo(adjacentInfo,tmpInfo) 
        && addAdjacentInfo(diffAdjacentInfoOfGraph,tmpInfo) 
        && !findAdjacentInfoFromLabel(overlapInfoOfGraph,tmpInfo->myself,TRUE)){
        // fprintf(stdout,"\n --- delete to before ---\n");
        // dumpAdjacentInfo(adjacentInfo);
        findAdjacentInfoFromLabel(beforeAdjacentInfoOfGraph,beforeLabels->atomLabels[i],TRUE);
      }
      /*[*1/22]
      status[i]=UPDATED;
      [*1/22*]*/
      // fprintf(stdout,"\n");
      if(addAdjacentInfo(beforeAdjacentInfoOfGraph,adjacentInfo)==FALSE){
        // fprintf(stdout,"\n --- add failed to before ---\n");
        // dumpAdjacentInfo(adjacentInfo);
        addAdjacentInfo(overlapInfoOfGraph,adjacentInfo);
      }else{
        if(!deleteAdjacentInfo(diffAdjacentInfoOfGraph,adjacentInfo)){
          addAdjacentInfo(diffAdjacentInfoOfGraph,adjacentInfo);        
        }
      }
    }
  }

  // printf("\n --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
  // fprintf(stdout,"\n --- beforeAdjacentInfoOfGraph ---\n");
  // dumpAdjacentInfoOfGraph(beforeAdjacentInfoOfGraph);  
  // fprintf(stdout,"\n --- diffAdjacentInfoOfGraph ---\n");
  // dumpAdjacentInfoOfGraph(diffAdjacentInfoOfGraph);

  /*
  while(numIntStack(needUpdate)>0){
    if(status[i=popIntStack(needUpdate)]<UPDATED){
      AdjacentInfo *adjacentInfo = makeAdjacentInfo();
      // そのアトム自身のラベルはatomLabels[i]に格納済…とは限らない
      if(afterLabels->atomLabels[i].second==-1){
        fprintf(stdout,"bAt[%d]:(%08X,%d),",i,beforeLabels->atomLabels[i].first,beforeLabels->atomLabels[i].second);
        adjacentInfo->myself = beforeLabels->atomLabels[i];
      }else{
        fprintf(stdout,"aAt[%d]:(%08X,%d),",i,afterLabels->atomLabels[i].first,afterLabels->atomLabels[i].second);
        adjacentInfo->myself = afterLabels->atomLabels[i];
      }
      // cgraph->atoms->body[i]->links にlinkの内訳が入っている
      // (link->attr)<128:普通のアトム、(link->attr)=138:HyperLink以外はそのままスルー
      ConvertedGraphVertex *cVertex = readDynamicArray(afterGraph->atoms,i);
      for(j=0;j<numStack(cVertex->links);j++){
        LMNtalLink *link = readStack(cVertex->links,j);
        if(link->attr < 128){
          // 接続先はatomなのでatomLabelsからラベルの情報を格納
          // afterLabelsに書いてない場合はbeforeLabelsから変化していないので、
          // beforeLabelから格納する
          if(afterLabels->atomLabels[link->data.ID].second==-1){
            // fprintf(stdout,"bAt[%d]:(%08X,%d), ",link->data.ID,beforeLabels->atomLabels[link->data.ID].first,beforeLabels->atomLabels[link->data.ID].second);
            addAdjacent(adjacentInfo,&beforeLabels->atomLabels[link->data.ID]);
          }else{
            // fprintf(stdout,"aAt[%d]:(%08X,%d), ",link->data.ID,afterLabels->atomLabels[link->data.ID].first,afterLabels->atomLabels[link->data.ID].second);
            addAdjacent(adjacentInfo,&afterLabels->atomLabels[link->data.ID]);
          }
        }else if(link->attr == 138){
          if(afterLabels->hyperLinkLabels[link->data.ID].second==-1){
            // fprintf(stdout,"bHL[%d]:(%08X,%d), ",i,beforeLabels->hyperLinkLabels[link->data.ID].first,beforeLabels->hyperLinkLabels[link->data.ID].second);
            addAdjacent(adjacentInfo,&beforeLabels->hyperLinkLabels[link->data.ID]);
          }else{
            // fprintf(stdout,"aHL[%d]:(%08X,%d), ",i,afterLabels->hyperLinkLabels[link->data.ID].first,afterLabels->hyperLinkLabels[link->data.ID].second);
            addAdjacent(adjacentInfo,&afterLabels->hyperLinkLabels[link->data.ID]);
          }
        }
      }
      // fprintf(stdout,"\n");
      calcAdjacentInfoHashValue(adjacentInfo);
      // dumpAdjacentInfo(adjacentInfo);
      AdjacentInfo *tmpInfo;
      tmpInfo = findAdjacentInfoFromLabel(beforeAdjacentInfoOfGraph,beforeLabels->atomLabels[i],FALSE);
      if(tmpInfo!=NULL && !compareAdjacentInfo(adjacentInfo,tmpInfo) && addAdjacentInfo(diffAdjacentInfoOfGraph,tmpInfo) && !findAdjacentInfoFromLabel(overlapInfoOfGraph,tmpInfo->myself,TRUE)){
        // fprintf(stdout,"\n --- add failed to before ---\n");
        // dumpAdjacentInfo(adjacentInfo);
        findAdjacentInfoFromLabel(beforeAdjacentInfoOfGraph,beforeLabels->atomLabels[i],TRUE);
      }
      status[i]=UPDATED;
      // fprintf(stdout,"\n");
      if(addAdjacentInfo(beforeAdjacentInfoOfGraph,adjacentInfo)==FALSE){
        // fprintf(stdout,"\n --- add failed to before ---\n");
        // dumpAdjacentInfo(adjacentInfo);
        addAdjacentInfo(overlapInfoOfGraph,adjacentInfo);
      }else{
        addAdjacentInfo(diffAdjacentInfoOfGraph,adjacentInfo);        
      }
    }
  }
  */

  // printf("\n --- file:%s...func:%s...line:%d ---\n",__FILE__,__func__,__LINE__);
  // fprintf(stdout,"\n --- diffAdjacentInfoOfGraph ---\n");
  // dumpAdjacentInfoOfGraph(diffAdjacentInfoOfGraph);

  // printf("\n --- file:%s...func:%s...line:%d ---\n\n",__FILE__,__func__,__LINE__);

  afterTime = get_dtime();
  rewriteAdjacentTime += afterTime - beforeTime;

  return beforeAdjacentInfoOfGraph;
}

void showDiffOfLabels(CanonicalLabel *beforeLabels,
                      CanonicalLabel *afterLabels,
                      int *diffLabels,
                      int numOfBeforeLabel,
                      int numOfAfterLabel);

int *searchDifferentLabels(CanonicalLabel *beforeLabels,
                           CanonicalLabel *afterLabels,
                           int numOfBeforeLabel,
                           int numOfAfterLabel){

  int bigger;
  int smaller;
  int i;
  if(numOfBeforeLabel >= numOfAfterLabel){
    bigger = numOfBeforeLabel;
    smaller = numOfAfterLabel;
  }else{
    bigger = numOfAfterLabel;
    smaller = numOfBeforeLabel;    
  }
  int *diffLabels = (int *)malloc(sizeof(int)*bigger);

  for(i=0;i<smaller;i++){
    if(beforeLabels[i].first != afterLabels[i].first || beforeLabels[i].second != afterLabels[i].second){
      diffLabels[i] = TRUE;
    }else{
      diffLabels[i] = FALSE;
    }
  }
  for(i=smaller;i<bigger;i++){
      diffLabels[i] = TRUE;
  }

  showDiffOfLabels(beforeLabels,afterLabels,diffLabels,numOfBeforeLabel,numOfAfterLabel);
  return diffLabels;
}


void showDiffOfLabels(CanonicalLabel *beforeLabels,
                      CanonicalLabel *afterLabels,
                      int *diffLabels,
                      int numOfBeforeLabel,
                      int numOfAfterLabel){

  int bigger;
  int smaller;
  int i;
  if(numOfBeforeLabel >= numOfAfterLabel){
    bigger = numOfBeforeLabel;
    smaller = numOfAfterLabel;
  }else{
    bigger = numOfAfterLabel;
    smaller = numOfBeforeLabel;
  }

  fprintf(stdout,"CHANGED LABEL:\n");
  for(i=0;i<smaller;i++){
    if(diffLabels[i] == TRUE){
      fprintf(stdout,"%3d : BEFORE:(%08X,%2d) -> AFTER:(%08X,%2d)\n",
        i,beforeLabels[i].first,beforeLabels[i].second,afterLabels[i].first,afterLabels[i].second);
    }  }
  for(i=smaller;i<bigger;i++){
      if(numOfBeforeLabel > numOfAfterLabel){
        if(beforeLabels[i].second != -1){
          fprintf(stdout,"%3d : BEFORE:(%08X,%2d) -> AFTER:(--------,--)\n",
                  i,beforeLabels[i].first,beforeLabels[i].second);
        }
      }else{
        if(afterLabels[i].second != -1){
          fprintf(stdout,"%3d : BEFORE:(--------,--) -> AFTER:(%08X,%2d)\n",
                  i,afterLabels[i].first,afterLabels[i].second);
        }
      }
  }

  return ;
}

// canonicalDiscreteRefinement(McKayでは判別しきれない頂点のリスト)
// の正規ラベルに通し番号を振り、その情報をLabelsの対応するラベルに入れる関数
void addSerialNumberFromCanonicalDiscreteRefinement(Labels *labels, List *canonicalDiscreteRefinement, int gapOfGlobalRootMemID,Bool measure){

  // 正規ラベルのhashの方の値とその個数を管理する構造体LabelNumを宣言
  typedef struct _LabelNum{
    Hash first;                 // 該当するCanonicalLabel.firstの値
    int num;                    // 該当するCanonicalLabel.firstを持つ頂点の個数
  } LabelNum;
  int i;

  beforeTime = get_dtime();

  // "LabelNumのDynamicArray"を格納するDynamicArrayを宣言
  // ラベルの下16bit(1バイト)で"LabelNumのDynamicArray"の格納先を決め、
  // "LabelNumのDynamicArray"の中には0から順番に格納していく
  // (被る可能性を考えると、実験ではほぼ0だけで済むはず)
  DynamicArray *labelNumDDArray = makeDynamicArray();
  labelNumDDArray = assureSizeOfDynamicArray(labelNumDDArray,0xFF);

  ListBody *sentinel = canonicalDiscreteRefinement->sentinel;
  ListBody *iterator;

  for(iterator=sentinel->next;iterator!=sentinel;){
    ListBody *iteratorNext = iterator->next;
    InheritedVertex *iVertex = iterator->value;
    if(iVertex == CLASS_SENTINEL){
      // iVertexがCLASS＿SENTINELのときは次の要素へ
      iterator = iteratorNext;
      continue;
    }
    // inheritedVertexDump(iVertex);
    int index = (int)iVertex->canonicalLabel.first & 0xFF;
    // printf("%d\n",index);
    if(readDynamicArray(labelNumDDArray,index)==NULL){
      // DynamicArrayがなかった場合は新しくLabelNumのDynamicArrayを宣言
      DynamicArray *labelNumArray = makeDynamicArray();
      LabelNum *labelNum = (LabelNum *)malloc(sizeof(LabelNum));
      labelNum->first = iVertex->canonicalLabel.first;
      labelNum->num = 1;
      writeDynamicArray(labelNumArray,0,labelNum);
      writeDynamicArray(labelNumDDArray,index,labelNumArray);
    }else{
      DynamicArray *labelNumArray = readDynamicArray(labelNumDDArray,index);
      // DynamicArrayがある場合にはその中を順番に調査
      for(i=0;i<labelNumArray->cap;i++){
        LabelNum *labelNum = readDynamicArray(labelNumArray,i);
        if(labelNum == NULL){
          // もし該当するfirstのLabelNumがなかったら新しく作成
          labelNum = (LabelNum *)malloc(sizeof(LabelNum));
          labelNum->first = iVertex->canonicalLabel.first;
          labelNum->num = 1;
          writeDynamicArray(labelNumDDArray,i,labelNumArray);
          // 一番最初にLabelが見つかったときの通し番号は0なので更新不要
        }else if(labelNum->first == iVertex->canonicalLabel.first){
          // もし該当するfirstのLabelNumがあったら通し番号を更新
          iVertex->canonicalLabel.second = labelNum->num;
          labelNum->num++;
          writeDynamicArray(labelNumDDArray,i,labelNumArray);
          // 対象のiVertexのラベルを更新
          collectLabelFromIVertexToLabels(iVertex,labels,gapOfGlobalRootMemID);
        }else{
          // LabelNumがあるが、自分のラベルと異なる場合は次のループへ
          continue;         
        }
      }
    }

    iterator = iteratorNext;
  }

  afterTime = get_dtime();
  if(measure){
    addSerialTime += afterTime - beforeTime;
  }
// extern double rewriteAdjacentTime;

  return;
}