#ifndef _EXPERIMENT_H
#define _EXPERIMENT_H

extern double startTime;
extern double endTime;

extern double beforeTime;
extern double afterTime;

extern int countOfStates;
extern int countOfTransition;
extern int countOfSortedInTrie;
extern int countOfNotSortedInTrie;

extern double triePropagateTime;
extern double listMcKayTime;
extern double addSerialTime;
extern double rewriteAdjacentTime;


double get_dtime(void);

#endif
